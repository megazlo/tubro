//  Created by Evgeniy Krasichkov on 16.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>
#import "TPAppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([TPAppDelegate class]));
  }
}
