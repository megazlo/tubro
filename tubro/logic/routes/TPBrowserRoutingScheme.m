//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPBrowserRoutingScheme.h"

#import "NSURL+Util.h"

@implementation TPBrowserRoutingScheme

- (BOOL)canRoteUrl:(NSURL*)url {
  return YES;
}

- (BOOL)openUrl:(NSURL*)url
    withProperties:(NSDictionary*)properties
   preserveHistory:(BOOL)preserveHistory {

  if ([self openUrlInIMDB:url]) {
    return YES;
  } else if ([self openUrlInYoutube:url]) {
    return YES;
  } else if ([self openUrlInYaBro:url]) {
    return YES;
  }
  return [[UIApplication sharedApplication] openURL:url];
}

- (BOOL)openUrlInIMDB:(NSURL*)url {
  if (![[url tp_domainWithLevel:2] isEqual:@"imdb.com"]) {
    return NO;
  }
  NSURL* appUrl = [[url tp_urlByReplacingHost:@""] tp_urlByReplacingScheme:@"imdb"];
  if (![[UIApplication sharedApplication] canOpenURL:appUrl]) {
    return NO;
  }
  return [[UIApplication sharedApplication] openURL:appUrl];
}

- (BOOL)openUrlInYoutube:(NSURL*)url {
  if (![[url tp_domainWithLevel:2] isEqual:@"youtube.com"] &&
      ![[url tp_domainWithLevel:2] isEqual:@"youtu.be"]) {
    return NO;
  }
  NSURL* appUrl =
      [NSURL URLWithString:
          [url.absoluteString stringByReplacingOccurrencesOfString:@"youtu.be/"
                                                        withString:@"youtube.com/watch?v="]];
  if (![[UIApplication sharedApplication] canOpenURL:appUrl]) {
    return NO;
  }
  return [[UIApplication sharedApplication] openURL:appUrl];
}

- (BOOL)openUrlInYaBro:(NSURL*)url {
  NSURL* appUrl =
      [NSURL URLWithString:
          [@"yandexbrowser-open-url://" stringByAppendingString:url.absoluteString]];
  if (![[UIApplication sharedApplication] canOpenURL:appUrl]) {
    return NO;
  }
  return [[UIApplication sharedApplication] openURL:appUrl];
}

@end
