//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@protocol TPRoutingSchemeProtocol <NSObject>

- (BOOL)canRoteUrl:(NSURL*)url;
- (BOOL)openUrl:(NSURL*)url
    withProperties:(NSDictionary*)properties
   preserveHistory:(BOOL)preserveHistory;

@end

@interface TPRoutes : NSObject

+ (instancetype)sharedInstance;

- (void)addRoutingSchemeWithLowestPriority:(id<TPRoutingSchemeProtocol>)scheme;
- (void)addRoutingSchemeWithDefaultPriority:(id<TPRoutingSchemeProtocol>)scheme;
- (void)addRoutingSchemeWithHighestPriority:(id<TPRoutingSchemeProtocol>)scheme;

- (void)openUrl:(NSURL*)url
    withProperties:(NSDictionary*)properties
   preserveHistory:(BOOL)preserveHistory;

@end
