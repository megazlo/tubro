//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPRoutes.h"

#import "TPSiteFactory.h"

@interface TPRoutes ()

@property (nonatomic, copy) NSArray* lowestPrioritySchemes;
@property (nonatomic, copy) NSArray* defaultPrioritySchemes;
@property (nonatomic, copy) NSArray* highestPrioritySchemes;

@end

@implementation TPRoutes

+ (NSArray*)schemesByAddingScheme:(id<TPRoutingSchemeProtocol>)scheme
                        toSchemes:(NSArray*)schemes {
  NSArray* result = schemes;
  if (!result) {
    result = [@[] mutableCopy];
  }
  return [result arrayByAddingObject:scheme];
}


+ (instancetype)sharedInstance {
  static TPRoutes* sharedInstance = nil;
  if (!sharedInstance) {
    sharedInstance = [[TPRoutes alloc] init];
  }
  return sharedInstance;
}

- (void)addRoutingSchemeWithLowestPriority:(id<TPRoutingSchemeProtocol>)scheme {
  self.lowestPrioritySchemes =
      [[self class] schemesByAddingScheme:scheme
                                toSchemes:self.lowestPrioritySchemes];
}

- (void)addRoutingSchemeWithDefaultPriority:
    (id<TPRoutingSchemeProtocol>)scheme {
  self.defaultPrioritySchemes =
      [[self class] schemesByAddingScheme:scheme
                                toSchemes:self.defaultPrioritySchemes];
}

- (void)addRoutingSchemeWithHighestPriority:
    (id<TPRoutingSchemeProtocol>)scheme {
  self.highestPrioritySchemes =
      [[self class] schemesByAddingScheme:scheme
                                toSchemes:self.highestPrioritySchemes];
}

- (void)openUrl:(NSURL*)url
    withProperties:(NSDictionary*)properties
   preserveHistory:(BOOL)preserveHistory {
  if (![self openUrl:url withProperties:properties preserveHistory:preserveHistory schemes:self.highestPrioritySchemes]) {
    if (![self openUrl:url withProperties:properties preserveHistory:preserveHistory schemes:self.defaultPrioritySchemes]) {
      if (![self openUrl:url withProperties:properties preserveHistory:preserveHistory schemes:self.lowestPrioritySchemes]) {
        NSAssert(NO, @"Can't route url.");
      }
    }
  }
}

- (BOOL)openUrl:(NSURL*)url
    withProperties:(NSDictionary*)properties
   preserveHistory:(BOOL)preserveHistory
           schemes:(NSArray*)schemes {
  for (id<TPRoutingSchemeProtocol> scheme in schemes) {
    if ([scheme canRoteUrl:url]) {
      if([scheme openUrl:url withProperties:properties preserveHistory:preserveHistory]) {
        return YES;
      }
    }
  }
  return NO;
}

@end
