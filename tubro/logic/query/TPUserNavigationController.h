//  Created by Evgeniy Krasichkov on 17.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@protocol TPUserNavigationController <NSObject>

- (void)stopLoading;
- (void)reload;

- (BOOL)canGoForward;
- (BOOL)canGoBackward;

- (void)goForward;
- (void)goBackward;

@end
