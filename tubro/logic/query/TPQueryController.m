//  Created by Evgeniy Krasichkov on 20.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPQueryController.h"

#import "TPRequestFactory.h"
#import "TPRequestLoader.h"

@implementation TPQueryController

#pragma mark TPUserQueryReceiver

- (void)userDidEnterQuery:(NSString*)query {
  NSURLRequest* request = [self.requestFactory requestForQuery:query];
  [self.requestLoader loadRequest:request];
}

@end
