//  Created by Evgeniy Krasichkov on 17.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@protocol TPRequestFactory <NSObject>

- (NSURLRequest*)requestForQuery:(NSString*)query;

@end
