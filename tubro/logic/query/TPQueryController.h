//  Created by Evgeniy Krasichkov on 20.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPUserQueryReceiver.h"

@protocol TPRequestFactory;
@protocol TPRequestLoader;

@interface TPQueryController : NSObject<TPUserQueryReceiver>

@property(nonatomic, weak) id<TPRequestFactory> requestFactory;
@property(nonatomic, weak) id<TPRequestLoader> requestLoader;

@end
