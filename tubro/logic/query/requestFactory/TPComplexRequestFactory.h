//  Created by Evgeniy Krasichkov on 24.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.//

#import "TPRequestFactory.h"

@interface TPComplexRequestFactory : NSObject<TPRequestFactory>

@property (nonatomic, copy) NSArray* factories;

@end
