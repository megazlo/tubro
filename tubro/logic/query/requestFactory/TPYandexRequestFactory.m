//  Created by Evgeniy Krasichkov on 24.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPYandexRequestFactory.h"

@implementation TPYandexRequestFactory

- (NSURLRequest*)requestForQuery:(NSString*)query {
  query =
      [query stringByTrimmingCharactersInSet:
          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
  if (query.length && [query characterAtIndex:0] == '?') {
    query = [query substringFromIndex:1];
    query =
        [query stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
  }
  return [self requestForSearchQuery:query];
}

- (NSURLRequest*)requestForSearchQuery:(NSString*)query {
  NSString* encodedQuery =
      [query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
  NSString* urlString =
      [NSString stringWithFormat:@"https://yandex.ru/search/?text=%@",
                                 encodedQuery];
  NSURL* url = [NSURL URLWithString:urlString];
  return [NSURLRequest requestWithURL:url];
}

@end
