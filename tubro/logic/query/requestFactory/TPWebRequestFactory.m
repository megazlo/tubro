//  Created by Evgeniy Krasichkov on 24.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPWebRequestFactory.h"

@implementation TPWebRequestFactory

- (NSURLRequest*)requestForQuery:(NSString*)query {
  query = [query stringByTrimmingCharactersInSet:
              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
  if (query.length && [query characterAtIndex:0] == '?') {
    return nil;
  }
  // TODO: domain recognition
  NSURL* url = [self urlFromString:query];
  if (!url) {
    url = [self urlFromString:[NSString stringWithFormat:@"http://%@", query]];
    if (!url) {
      return nil;
    }
  }
  return [NSURLRequest requestWithURL:url];
}

- (NSURL*)urlFromString:(NSString*)string {
  NSURL* url = [NSURL URLWithString:string];
  if (!url || !url.scheme || !url.host) {
    return nil;
  }
  return url;
}

@end
