//  Created by Evgeniy Krasichkov on 24.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPComplexRequestFactory.h"

@implementation TPComplexRequestFactory

- (NSURLRequest*)requestForQuery:(NSString*)query {
  for (id<TPRequestFactory> factory in self.factories) {
    NSURLRequest* request = [factory requestForQuery:query];
    if (request) {
      return request;
    }
  }
  return nil;
}

@end
