//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@class TPTabManager;

@protocol TPUserNavigationController;

@protocol TPTabManagerStatusDelegate <NSObject>

- (void)tabManager:(TPTabManager*)tabManager didUpdateTitle:(NSString*)title;
- (void)tabManager:(TPTabManager*)tabManager
    didUpdateNavigationController:(id<TPUserNavigationController>)controller;

- (void)tabManager:(TPTabManager*)tabManager
    didStartLoadingRequest:(NSURLRequest*)request;
- (void)tabManagerDidFinishLoadingRequest:(TPTabManager*)tabManager;

@end