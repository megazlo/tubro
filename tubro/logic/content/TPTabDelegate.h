//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@class TPTab;

@protocol TPUserNavigationController;

@protocol TPTabDelegate <NSObject>

- (void)tab:(TPTab*)tab didUpdateTitle:(NSString*)title;
- (void)tab:(TPTab*)tab
    didUpdateNavigationController:(id<TPUserNavigationController>)controller;

- (void)tab:(TPTab*)tab didStartLoadingRequest:(NSURLRequest*)request;
- (void)tabDidFinishLoadingRequest:(TPTab*)tab;

@end