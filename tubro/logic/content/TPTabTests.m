//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPTabPresenter.h"
#import "TPTab.h"

SPEC_BEGIN(TPTabSpec)

describe(@"TPTab", ^{
  NSURLRequest* const kTestLink =
      [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://test.ru"]];
  NSURLRequest* const kTestLink2 =
      [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://test2.ru"]];
  NSURLRequest* const kTestLink3 =
      [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://test3.ru"]];
  NSURLRequest* const kTestLink4 =
      [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://test4.ru"]];
  __block TPTab* tab = nil;
  beforeEach(^{
    tab = [[TPTab alloc] init];
  });

  context(@"content navigation", ^{
    specify(^{
      [tab loadRequest:kTestLink];
      [[tab.currentRequest should] equal:kTestLink];
      [[tab.requestStack should] equal:@[kTestLink]];
      [[@([tab canGoForward]) should] equal:@NO];
      [[@([tab canGoBackward]) should] equal:@NO];
    });
    specify(^{
      [tab loadRequest:kTestLink];
      [tab loadRequest:kTestLink2];
      [[tab.currentRequest should] equal:kTestLink2];
      [[tab.requestStack should] equal:@[kTestLink, kTestLink2]];
      [[@([tab canGoForward]) should] equal:@NO];
      [[@([tab canGoBackward]) should] equal:@YES];
    });
    specify(^{
      [tab loadRequest:kTestLink];
      [tab loadRequest:kTestLink2];
      [tab goBackward];
      [[tab.currentRequest should] equal:kTestLink];
      [[@([tab canGoForward]) should] equal:@YES];
      [[@([tab canGoBackward]) should] equal:@NO];
      [tab goForward];
      [[tab.currentRequest should] equal:kTestLink2];
    });
    specify(^{
      [tab loadRequest:kTestLink];
      [tab loadRequest:kTestLink2];
      [tab loadRequest:kTestLink3];
      [[tab.requestStack should]
          equal:@[kTestLink, kTestLink2, kTestLink3]];
      [tab goBackward];
      [tab loadRequest:kTestLink4];
      [[tab.currentRequest should] equal:kTestLink4];
      [[tab.requestStack should]
          equal:@[kTestLink, kTestLink2, kTestLink4]];
      [[@([tab canGoForward]) should] equal:@NO];
      [[@([tab canGoBackward]) should] equal:@YES];
    });
  });
  context(@"tab delegate", ^{
    __block id presenter = nil;
    beforeEach(^{
      presenter = [KWMock nullMockForProtocol:@protocol(TPTabPresenter)];
      tab.presenter = presenter;
    });
    specify(^{
      [[presenter should] receive:@selector(startLoadingRequest:withReason:)
                    withArguments:kTestLink, any()];
      [tab loadRequest:kTestLink];
    });
  });
});

SPEC_END