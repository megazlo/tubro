//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPRequestLoader.h"
#import "TPTabDelegate.h"
#import "TPTabPresenterDelegate.h"
#import "TPUserNavigationController.h"

@class TPLink;

@protocol TPTabPresenter;

@interface TPTab : NSObject<
    TPRequestLoader, TPUserNavigationController, TPTabPresenterDelegate>

@property(nonatomic, readonly) NSString* title;
@property(nonatomic, weak) id<TPTabDelegate> delegate;

@property(nonatomic, copy, readonly) NSURLRequest* currentRequest;
@property(nonatomic, copy, readonly) NSArray* requestStack;

@property(nonatomic, weak) id<TPTabPresenter> presenter;

@end
