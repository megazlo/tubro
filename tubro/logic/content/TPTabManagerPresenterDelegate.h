//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@protocol TPTabManagerPresenter;

@protocol TPTabManagerPresenterDelegate <NSObject>

@end