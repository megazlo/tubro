//  Created by Evgeniy Krasichkov on 21.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@class TPLink;

@protocol TPContentPresenter;

@interface TPContentViewControllerFactory : NSObject

+ (instancetype)sharedInstance;

- (UIViewController<TPContentPresenter>*)contentViewControllerForRequest:
    (NSURLRequest*)request;

@end
