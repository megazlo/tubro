//  Created by Evgeniy Krasichkov on 17.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import "TPContentView.h"
#import "TPNavigationReason.h"

@protocol TPContentPresenterDelegate;

@protocol TPContentPresenter <TPContentView>

@property(nonatomic, weak) id<TPContentPresenterDelegate> delegate;

@property(nonatomic, readonly) NSString* title;

- (void)startLoadingRequest:(NSURLRequest*)request
       withNavigationReason:(TPNavigationReason)reason;
- (void)stopLoading;
- (void)reload;

- (BOOL)isLoading;
- (BOOL)isLoaded;

@end
