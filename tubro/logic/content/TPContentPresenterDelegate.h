//  Created by Evgeniy Krasichkov on 17.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@class TPLink;

@protocol TPContentPresenter;

@protocol TPContentPresenterDelegate <NSObject>

- (void)contentPresenter:(id<TPContentPresenter>)contentPresenter
          didUpdateTitle:(NSString*)title;

- (BOOL)contentPresenter:(id<TPContentPresenter>)contentPresenter
    shouldStartNavigationWithRequest:(NSURLRequest*)request;
- (void)contentPresenter:(id<TPContentPresenter>)contentPresenter
    didCommitNavigationWithRequest:(NSURLRequest*)request;
- (void)contentPresenterDidFinishNavigation:
    (id<TPContentPresenter>)contentPresenter;

@end
