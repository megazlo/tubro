//  Created by Evgeniy Krasichkov on 11.04.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TPNavigationReason) {
  TPNavigationReasonNavigation = 0,
  TPNavigationReasonGoBackward,
  TPNavigationReasonGoForward,
};
