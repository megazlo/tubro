//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPNavigationReason.h"

@class TPLink;
@class TPTab;

@protocol TPTabPresenterDelegate;

@protocol TPTabPresenter <NSObject>

@property (nonatomic, weak) id<TPTabPresenterDelegate> delegate;

@property(nonatomic, readonly) NSString* title;

- (void)startLoadingRequest:(NSURLRequest*)request
                 withReason:(TPNavigationReason)reason;
- (void)stopLoading;
- (void)reload;

@end