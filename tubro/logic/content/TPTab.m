//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPTab.h"

#import "TPTabPresenter.h"

@interface TPTab ()

@property(nonatomic, copy, readwrite) NSArray* requestStack;

@property(nonatomic, assign) NSUInteger currentRequestIndex;

@end

@implementation TPTab

- (NSArray*)requestStack {
  if (!_requestStack) {
    _requestStack = @[];
  }
  return _requestStack;
}

- (NSURLRequest*)currentRequest {
  if (self.currentRequestIndex < self.requestStack.count) {
    return self.requestStack[self.currentRequestIndex];
  }
  return nil;
}

- (void)setCurrentRequest:(NSURLRequest*)currentRequest {
  NSAssert(currentRequest, @"");
  NSRange range =
      NSMakeRange(0, self.requestStack.count ? self.currentRequestIndex + 1
                                             : 0);
  NSArray* requestStackTailingWithCurrent =
      [self.requestStack subarrayWithRange:range];
  if (self.requestStack.count) {
    self.currentRequestIndex++;
  }
  self.requestStack =
      [requestStackTailingWithCurrent arrayByAddingObject:currentRequest];
}

- (void)startLoadingCurrentRequestWithReason:
    (TPNavigationReason)reason {
  [self.presenter startLoadingRequest:self.currentRequest
                           withReason:reason];
  [self.delegate tab:self didStartLoadingRequest:self.currentRequest];
  [self.delegate tab:self didUpdateNavigationController:self];
}

- (NSString*)title {
  return self.presenter.title;
}

#pragma mark TPRequestLoader

- (void)loadRequest:(NSURLRequest*)request {
  self.currentRequest = request;
  [self startLoadingCurrentRequestWithReason:TPNavigationReasonNavigation];
}

#pragma mark TPUserNavigationController

- (void)stopLoading {
  [self.presenter stopLoading];
  [self.delegate tabDidFinishLoadingRequest:self];
}

- (void)reload {
  [self.presenter reload];
  [self.delegate tab:self didStartLoadingRequest:self.currentRequest];
}

- (BOOL)canGoForward {
  return self.requestStack.count &&
         self.currentRequestIndex < self.requestStack.count - 1;
}

- (void)goForward {
  if ([self canGoForward]) {
    self.currentRequestIndex++;
    [self startLoadingCurrentRequestWithReason:
        TPNavigationReasonGoForward];
  }
}

- (BOOL)canGoBackward {
  return self.currentRequestIndex;
}

- (void)goBackward {
  if ([self canGoBackward]) {
    --self.currentRequestIndex;
    [self startLoadingCurrentRequestWithReason:
        TPNavigationReasonGoBackward];
  }
}

#pragma mark TPTabPresenterDelegate

- (void)tabPresenter:(id<TPTabPresenter>)tabPresenter
      didUpdateTitle:(NSString*)title {
  [self.delegate tab:self didUpdateTitle:title];
}

- (BOOL)tabPresenter:(id<TPTabPresenter>)tabPresenter
    shouldStartNavigationWithRequest:(NSURLRequest*)request {
  return YES;
}

- (void)tabPresenter:(id<TPTabPresenter>)tabPresenter
    didCommitNavigationWithRequest:(NSURLRequest*)request {
  if (![self.currentRequest.URL isEqual:request.URL]) {
    self.currentRequest = request;
    [self startLoadingCurrentRequestWithReason:
        TPNavigationReasonNavigation];
  }
}

- (void)tabPresenterDidFinishNavigation:(id<TPTabPresenter>)tabPresenter {
  [self.delegate tabDidFinishLoadingRequest:self];
}

@end
