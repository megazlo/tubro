//  Created by Evgeniy Krasichkov on 24.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPRequestLoader.h"
#import "TPUserNavigationController.h"

@protocol TPTabManagerStatusDelegate;
@protocol TPTabManagerPresenter;

@interface TPTabManager : NSObject<TPRequestLoader, TPUserNavigationController>

@property(nonatomic, weak) id<TPTabManagerStatusDelegate> statusDelegate;
@property(nonatomic, weak) id<TPTabManagerPresenter> tabManagerPresenter;

@property(nonatomic) NSInteger tabCount;

- (void)createTabAtIndex:(NSInteger)index;
- (void)activateTabAtIndex:(NSInteger)index;

@end
