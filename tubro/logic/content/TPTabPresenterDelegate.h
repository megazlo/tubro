//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@protocol TPTabPresenter;

@protocol TPTabPresenterDelegate <NSObject>

- (void)tabPresenter:(id<TPTabPresenter>)tabPresenter
      didUpdateTitle:(NSString*)title;

- (BOOL)tabPresenter:(id<TPTabPresenter>)tabPresenter
    shouldStartNavigationWithRequest:(NSURLRequest*)request;
- (void)tabPresenter:(id<TPTabPresenter>)tabPresenter
    didCommitNavigationWithRequest:(NSURLRequest*)request;
- (void)tabPresenterDidFinishNavigation:(id<TPTabPresenter>)tabPresenter;

@end