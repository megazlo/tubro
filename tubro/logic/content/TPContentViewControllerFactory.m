//  Created by Evgeniy Krasichkov on 21.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPContentViewControllerFactory.h"

#import "TPCommonMacros.h"
#import "TPContentPresenter.h"
#import "TPWebContentViewController.h"

@interface TPContentViewControllerFactory()

@property(nonatomic, strong) TPWebContentViewController* webVC;

@end

@implementation TPContentViewControllerFactory

+ (instancetype)sharedInstance {
  static TPContentViewControllerFactory* g_sharedInstance;
  static dispatch_once_t token;
  dispatch_once(&token, ^{
      g_sharedInstance = [[TPContentViewControllerFactory alloc] init];
  });
  return g_sharedInstance;
}

- (TPWebContentViewController*)webVC {
  if (!_webVC) {
    _webVC = [[TPWebContentViewController alloc] init];
  }
  return _webVC;
}

- (UIViewController<TPContentPresenter>*)contentViewControllerForRequest:
    (NSURLRequest*)request {
  UIViewController<TPContentPresenter>* contentVC = nil;
  contentVC = self.webVC;
  return contentVC;
}

@end
