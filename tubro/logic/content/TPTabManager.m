//  Created by Evgeniy Krasichkov on 24.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPTabManager.h"

#import "TPTabManagerPresenter.h"
#import "TPTabManagerStatusDelegate.h"
#import "TPTab.h"

@interface TPTabManager()<TPTabDelegate>

@property (nonatomic, copy) NSArray* tabs;

@property (nonatomic) TPTab* activeTab;
@property (nonatomic, assign) NSInteger activeTabIndex;

@end

@implementation TPTabManager

- (NSArray*)tabs {
  if (!_tabs) {
    _tabs = @[];
  }
  return _tabs;
}

- (TPTab*)activeTab {
  if (!self.tabs.count) {
    return nil;
  }
  if (self.activeTabIndex >= self.tabs.count) {
    self.activeTabIndex = self.tabs.count - 1;
  }
  return self.tabs[self.activeTabIndex];
}

- (NSInteger)tabCount {
  return self.tabs.count;
}

- (void)createTabAtIndex:(NSInteger)index {
  NSAssert(index <= self.tabCount, @"");
  NSMutableArray* mutableTabs = [self.tabs mutableCopy];
  BOOL isOnlyTab = !mutableTabs.count;
  TPTab* newTab = [[TPTab alloc] init];
  newTab.delegate = self;
  [mutableTabs insertObject:newTab atIndex:index];
  self.tabs = mutableTabs;
  if (isOnlyTab) {
    [self activateTabAtIndex:index];
  } else if (self.activeTabIndex >= index) {
    self.activeTabIndex++;
  }
}

- (void)activateTabAtIndex:(NSInteger)index {
  NSAssert(index <= self.tabCount, @"");
  self.activeTabIndex = index;
  [self.tabManagerPresenter tabManager:self tabDidActivate:self.activeTab];
  [self notifyDelegateDidUpdateTitle];
}

- (void)notifyDelegateDidUpdateTitle {
  [self.statusDelegate tabManager:self didUpdateTitle:self.activeTab.title];
}

- (void)notifyDelegateDidStartLoadingRequest {
  [self.statusDelegate tabManager:self
          didStartLoadingRequest:self.activeTab.currentRequest];
}

- (void)notifyDelegateDidUpdateNavigationController {
  [self.statusDelegate tabManager:self
      didUpdateNavigationController:self.activeTab];
}

#pragma mark TPRequestLoader

- (void)loadRequest:(NSURLRequest*)request {
  if (!self.tabCount) {
    [self createTabAtIndex:self.tabCount];
  }
  TPTab* tab = self.activeTab;
  NSAssert(tab, @"");
  [tab loadRequest:request];
}

#pragma mark TPUserNavigationController

- (void)stopLoading {
  [self.activeTab stopLoading];
}

- (void)reload {
  [self.activeTab reload];
}

- (BOOL)canGoForward {
  return [self.activeTab canGoForward];
}

- (void)goForward {
  [self.activeTab goForward];
}

- (BOOL)canGoBackward {
  return [self.activeTab canGoBackward];
}

- (void)goBackward {
  [self.activeTab goBackward];
}

#pragma mark TPTabDelegate

- (void)tab:(TPTab*)tab didUpdateTitle:(NSString*)title {
  if (tab == self.activeTab) {
    [self notifyDelegateDidUpdateTitle];
  }
}

- (void)tab:(TPTab*)tab didStartLoadingRequest:(NSURLRequest*)request {
  if (tab == self.activeTab) {
    [self notifyDelegateDidStartLoadingRequest];
  }
}

- (void)tab:(TPTab*)tab
    didUpdateNavigationController:(id<TPUserNavigationController>)controller {
  if (tab == self.activeTab) {
    [self notifyDelegateDidUpdateNavigationController];
  }
}

- (void)tabDidFinishLoadingRequest:(TPTab*)tab {
  if (tab == self.activeTab) {
    [self.statusDelegate tabManagerDidFinishLoadingRequest:self];
  }
}

@end
