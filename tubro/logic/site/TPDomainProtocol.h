//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPRemoteDataSourceProtocol.h"

@protocol TPDomainProtocol <NSObject>

@property (nonatomic, copy, readonly) NSString* title;
@property (nonatomic, copy, readonly) NSString* url;

@end
