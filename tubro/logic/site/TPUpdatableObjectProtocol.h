//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@protocol TPUpdatableObjectProtocol;

@protocol TPUpdatableObjectDelegate <NSObject>

- (void)objectDidUpdate:(id<TPUpdatableObjectProtocol>)object;

@end

@protocol TPUpdatableObjectProtocol <NSObject>

@property (nonatomic, weak) id<TPUpdatableObjectDelegate> delegate;

@end
