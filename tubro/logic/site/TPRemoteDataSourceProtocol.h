//  Created by Evgeniy Krasichkov on 03.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TPRemoteDataSourceStatus) {
  TPRemoteDataSourceStatusReady = 0,
  TPRemoteDataSourceStatusLoading,
  TPRemoteDataSourceStatusPartial,
  TPRemoteDataSourceStatusOK,
  TPRemoteDataSourceStatusLoadingError,
  TPRemoteDataSourceStatusParseError,
  TPRemoteDataSourceStatusNotConnectedToInternet,
  TPRemoteDataSourceStatusNotFound,
  TPRemoteDataSourceStatusServiceUnavailable,
  TPRemoteDataSourceStatusLoginRequired,
};

@protocol TPRemoteDataSourceProtocol <NSObject, NSCopying>

@property (nonatomic, readonly) TPRemoteDataSourceStatus status;

@end
