//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPParsedPage_Private.h"

static NSUInteger const kTPParsedPageOneParseAttemptMaxIterationCount = 500;

@implementation TPParsedPage

@synthesize dataSource = _dataSource;

- (instancetype)initWithURL:(NSString *)url {
  if ((self = [super init])) {
    NSParameterAssert(url);
    _url = url;
    _method = @"GET";
    _responseType = kTPResponseTypeHpple;
  }
  return self;
}

- (void)startOrContinueParseWithDataSource:(TPRemoteDataSource *)dataSource
                           completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  self.status = dataSource.status;
  [self startOrContinueParseWithResponseObject:dataSource.responseObject
                               completionBlock:completionBlock];
}

- (void)startOrContinueParseWithResponseObject:(id)responseObject
                               completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
#ifndef NDEBUG
  self.isParentParseCalled = YES;
#endif //NDEBUG
  completionBlock();
}

- (void)stripLastParsedObject {
}

- (void)reportParseIteration {
  NSAssert([self tp_isBackgroundThread], @"");
  ++self.iterationCount;
  if (self.iterationCount > kTPParsedPageOneParseAttemptMaxIterationCount) {
    self.iterationCount = 0;
    if (self.partialDataRetrieveBlock) {
      TPParsedPage* partialPage = [self copy];
      partialPage.status = TPRemoteDataSourceStatusPartial;
      [self tp_dispatchAsyncOnMainThread:^{
          self.partialDataRetrieveBlock(partialPage);
      }];
    }
  }
}

- (void)setDataSource:(TPRemoteDataSource *)dataSource {
  if (_dataSource != dataSource) {
    _dataSource = dataSource;
  }
}

#pragma comment NSCopying

- (id)copyWithZone:(NSZone *)zone {
  TPParsedPage* newObject = [[[self class] allocWithZone:zone] init];
  return newObject;
}

#pragma comment TPRemoteObjectProtocol

- (void)loadWithCompletionBlock:
    (TPParsedPageLoadCompletionBlock)completionBlock {
  [self loadWithCompletionBlock:completionBlock partialDataRetrieveBlock:nil];
}

- (void)loadWithCompletionBlock:
    (TPParsedPageLoadCompletionBlock)completionBlock
      partialDataRetrieveBlock:
    (TPParsedPageLoadPartialDataRetrieveBlock)partialDataRetrieveBlock {
  NSAssert([self tp_isMainThread], @"");
  self.completionBlock = completionBlock;
  self.partialDataRetrieveBlock = partialDataRetrieveBlock;
  if (self.status == TPRemoteDataSourceStatusReady) {
    [self startLoadOrParse];
  } else {
    NSAssert(NO, @"Status must be Ready");
  }
}

- (void)startLoadOrParse {
  if (self.dataSource &&
      self.dataSource.status != TPRemoteDataSourceStatusReady) {
    [self parseOnBackgroundThread];
  } else {
    [self loadAndParse];
  }
}

- (void)loadAndParse {
  self.status = TPRemoteDataSourceStatusLoading;
  
  TPRemoteDataSource* dataSource =
      [[TPRemoteDataSource alloc] initWithURL:self.url
                                       method:self.method
                                   parameters:self.parameters
                                 responseType:self.responseType];

  TPRemoteDataSourceLoadPartialDataRetrieveBlock localPartialRetrieveBlock = nil;
  if (self.partialDataRetrieveBlock) {
    localPartialRetrieveBlock = ^(TPRemoteDataSource* dataSource) {
        NSAssert([self tp_isMainThread], @"");
        self.dataSource = dataSource;
        [self parseOnBackgroundThread];
    };
  }
  TPRemoteDataSourceLoadCompletionBlock localCompletionBlock = ^{
      NSAssert([self tp_isMainThread], @"");
      self.dataSource = dataSource;
      [self parseOnBackgroundThread];
  };
  [dataSource loadWithCompletionBlock:localCompletionBlock
             partialDataRetrieveBlock:localPartialRetrieveBlock];
}

- (void)parseOnBackgroundThread {
  NSAssert([self tp_isMainThread], @"");
  if (!self.isParsingStarted) {
    self.isParsingStarted = YES;
    [self scheduleNextParseIteration];
  }
}

- (void)scheduleNextParseIteration {
  [self performSelector:@selector(parseIteration) withObject:nil afterDelay:0.1];
}

- (void)parseIteration {
  NSAssert([self tp_isMainThread], @"");
  TPRemoteDataSource* dataSource = self.dataSource;
  if (dataSource) {
    self.dataSource = nil;
    __weak TPParsedPage* weakSelf = self;
    [self tp_dispatchAsyncOnBackgroundThread:^{
        [weakSelf startOrContinueParseWithDataSource:dataSource
                                 completionBlock:^{
            NSAssert(weakSelf.isParentParseCalled, @"");
            [weakSelf tp_dispatchAsyncOnMainThread:^{
                if (dataSource.status != TPRemoteDataSourceStatusPartial) {
                  [weakSelf complete];
                } else {
                  [weakSelf scheduleNextParseIteration];
                }
            }];
        }];
    }];
  } else {
    [self scheduleNextParseIteration];
  }
}

- (void)complete {
  if (self.completionBlock) {
    self.completionBlock();
  }
  self.completionBlock = nil;
  self.partialDataRetrieveBlock = nil;
}

@end
