//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import "TPRemoteDataSourceProtocol.h"

@class TPParsedPage;
@class TPRemoteDataSource;

typedef void (^TPParsedPageLoadCompletionBlock)(void);
typedef void (^TPParsedPageParseCompletionBlock)(void);
typedef void (^TPParsedPageLoadPartialDataRetrieveBlock)(TPParsedPage* page);

@interface TPParsedPage : NSObject <TPRemoteDataSourceProtocol>

@property (nonatomic, copy) NSString* url;
@property (nonatomic, copy) NSString* method;
@property (nonatomic, copy) NSDictionary* parameters;
@property (nonatomic, copy) NSString* responseType;

@property (nonatomic, weak) TPParsedPage* source;

- (instancetype)initWithURL:(NSString*)url;

- (void)loadWithCompletionBlock:
    (TPParsedPageLoadCompletionBlock)completionBlock;

- (void)loadWithCompletionBlock:
    (TPParsedPageLoadCompletionBlock)completionBlock
      partialDataRetrieveBlock:
    (TPParsedPageLoadPartialDataRetrieveBlock)partialDataRetrieveBlock;

@end
