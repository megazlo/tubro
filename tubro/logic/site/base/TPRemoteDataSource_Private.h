//  Created by Evgeniy Krasichkov on 09.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPRemoteDataSource.h"

#import <AFNetworking/AFNetworking.h>

#import "NSObject+Threads.h"
#import "TPOnoResponseSerializer.h"
#import "TPJSONResponseSerializer.h"
#import "TPResponseSerializer.h"

@interface TPRemoteDataSource ()

@property (nonatomic, copy) NSString* url;
@property (nonatomic, copy) NSString* responseURL;
@property (nonatomic, assign) TPRemoteDataSourceStatus status;
@property (nonatomic, strong) id responseObject;
@property (nonatomic, copy)
    TPRemoteDataSourceLoadCompletionBlock loadCompletionBlock;
@property (nonatomic, copy)
    TPRemoteDataSourceLoadPartialDataRetrieveBlock partialDataRetrieveBlock;
@property (nonatomic, strong)
    AFHTTPRequestOperationManager* manager;
@property (nonatomic, assign) BOOL isLoaded;

- (void)requestOperation:(AFHTTPRequestOperation*)operation
    loadSuccessWithResponse:(id)responseObject;

- (void)requestOperation:(AFHTTPRequestOperation*)operation
    loadFailureWithError:(NSError*)error;

@end

