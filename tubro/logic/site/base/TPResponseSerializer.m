//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPResponseSerializer.h"

NSString* const kTPResponseSerializerErrorDomain =
    @"kTPResponseSerializerErrorDomain";

NSString* const kTPResponseTypeHpple = @"Hpple";
NSString* const kTPResponseTypeJSON = @"JSON";
