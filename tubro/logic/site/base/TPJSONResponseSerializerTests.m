//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPJSONResponseSerializer.h"
#import "TPResponseSerializer.h"

SPEC_BEGIN(TPJSONResponseSerializerSpec)

describe(@"TPJSONResponseSerializer", ^{
  __block TPJSONResponseSerializer* serializer = nil;
  __block NSError* error = nil;
  NSData* const validJSON =
      [@"{\"status\": \"OK\"}" dataUsingEncoding:NSUTF8StringEncoding];
  NSString* const kTestURL = @"http://d3.ru";

  beforeEach(^{
    serializer = [[TPJSONResponseSerializer alloc] init];
    serializer.acceptableContentTypes = nil;
    error = nil;
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[serializer shouldNot] beNil];
    });
  });

  context(@"when received HTTP response code 200", ^{
    NSURLResponse* const kResponse200 =
        [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:kTestURL]
                                    statusCode:200
                                   HTTPVersion:@"HTTP/1.0"
                                  headerFields:@{}];

    it(@"should return valid TFHpple object", ^{
      NSDictionary* json =
          [serializer responseObjectForResponse:kResponse200
                                           data:validJSON
                                          error:&error];
      [[json shouldNot] beNil];
      [[serializer.responseObject should] equal:json];
    });
    it(@"should not modify error object", ^{
      [serializer responseObjectForResponse:kResponse200
                                       data:validJSON
                                      error:&error];
      [[error should] beNil];
    });
  });

  context(@"when received HTTP response code 404", ^{
    NSURLResponse* const kResponse404 =
        [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:kTestURL]
                                    statusCode:404
                                   HTTPVersion:@"HTTP/1.0"
                                  headerFields:@{}];

    it(@"should return valid TFHpple object", ^{
      NSDictionary* json =
          [serializer responseObjectForResponse:kResponse404
                                           data:validJSON
                                          error:&error];
      [[json shouldNot] beNil];
      [[serializer.responseObject should] equal:json];
    });
    it(@"should return suitable error code", ^{
      [serializer responseObjectForResponse:kResponse404
                                       data:validJSON
                                      error:&error];
      [[error.domain should] equal:kTPResponseSerializerErrorDomain];
      [[theValue(error.code) should]
          equal:theValue(TPResponseSerializerErrorCodeNotFound)];
    });
  });

  context(@"when received HTTP response code 503", ^{
    NSURLResponse* const kResponse503 =
        [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:kTestURL]
                                    statusCode:503
                                   HTTPVersion:@"HTTP/1.0"
                                  headerFields:@{}];

    it(@"should return valid TFHpple object", ^{
      NSDictionary* json =
          [serializer responseObjectForResponse:kResponse503
                                           data:validJSON
                                          error:&error];
      [[json shouldNot] beNil];
      [[serializer.responseObject should] equal:json];
    });
    it(@"should return suitable error code", ^{
      [serializer responseObjectForResponse:kResponse503
                                       data:validJSON
                                      error:&error];
      [[error.domain should] equal:kTPResponseSerializerErrorDomain];
      [[theValue(error.code) should]
          equal:theValue(TPResponseSerializerErrorCodeServiceUnavailable)];
    });
  });

  context(@"when received any other invalid response", ^{
    NSURLResponse* const kResponseInvalid =
        [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:kTestURL]
                                    statusCode:505
                                   HTTPVersion:@"HDDP"
                                  headerFields:@{}];

    it(@"should return valid TFHpple object", ^{
      NSDictionary* json =
          [serializer responseObjectForResponse:kResponseInvalid
                                           data:validJSON
                                          error:&error];
      [[json shouldNot] beNil];
      [[serializer.responseObject should] equal:json];
    });
    it(@"should return error code @TPResponseSerializerErrorCodeOther", ^{
      [serializer responseObjectForResponse:kResponseInvalid
                                       data:validJSON
                                      error:&error];
      [[error.domain should] equal:kTPResponseSerializerErrorDomain];
      [[theValue(error.code) should]
          equal:theValue(TPResponseSerializerErrorCodeOther)];
    });
  });
});

SPEC_END