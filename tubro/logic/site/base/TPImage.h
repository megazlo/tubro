//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPImageProtocol.h"

@class ONOXMLElement;

@interface TPImage : NSObject <TPImageProtocol>

+ (instancetype)imageWithOnoElement:(ONOXMLElement*)element;
+ (NSArray*)imagesWithOnoElement:(ONOXMLElement*)element;

@end
