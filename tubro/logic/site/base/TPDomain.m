//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPDomain.h"

@interface TPDomain ()

@property (nonatomic, copy, readwrite) NSString* title;
@property (nonatomic, copy, readwrite) NSString* url;

@end 

@implementation TPDomain

+ (instancetype)domainWithTitle:(NSString*)title url:(NSString*)url {
  TPDomain* domain = [[TPDomain alloc] init];
  domain.title = title;
  domain.url = url;
  return domain;
}

@end
