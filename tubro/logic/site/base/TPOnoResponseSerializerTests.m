//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import <Ono/Ono.h>

#import "TPOnoResponseSerializer.h"
#import "TPResponseSerializer.h"

SPEC_BEGIN(TPOnoResponseSerializerSpec)

describe(@"TPOnoResponseSerializer", ^{
  __block TPOnoResponseSerializer* serializer = nil;
  __block NSError* error = nil;
  NSData* const validHTML =
      [@"<html><h1>title<h1/></html>" dataUsingEncoding:NSUTF8StringEncoding];
  NSString* const kTestURL = @"http://d3.ru";

  beforeEach(^{
    serializer = [[TPOnoResponseSerializer alloc] init];
    error = nil;
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[serializer shouldNot] beNil];
    });
  });

  context(@"when received HTTP response code 200", ^{
    NSURLResponse* const kResponse200 =
        [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:kTestURL]
                                    statusCode:200
                                   HTTPVersion:@"HTTP/1.0"
                                  headerFields:@{}];

    it(@"should return valid TFHpple object", ^{
      ONOXMLDocument* document =
          [serializer responseObjectForResponse:kResponse200
                                           data:validHTML
                                          error:&error];
      [[document shouldNot] beNil];
      [[serializer.responseObject should] equal:document];
    });
    it(@"should not modify error object", ^{
      [serializer responseObjectForResponse:kResponse200
                                       data:validHTML
                                      error:&error];
      [[error should] beNil];
    });
  });

  context(@"when received HTTP response code 404", ^{
    NSURLResponse* const kResponse404 =
        [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:kTestURL]
                                    statusCode:404
                                   HTTPVersion:@"HTTP/1.0"
                                  headerFields:@{}];

    it(@"should return valid TFHpple object", ^{
      ONOXMLDocument* document =
          [serializer responseObjectForResponse:kResponse404
                                           data:validHTML
                                          error:&error];
      [[document shouldNot] beNil];
      [[serializer.responseObject should] equal:document];
    });
    it(@"should return suitable error code", ^{
      [serializer responseObjectForResponse:kResponse404
                                       data:validHTML
                                      error:&error];
      [[error.domain should] equal:kTPResponseSerializerErrorDomain];
      [[theValue(error.code) should]
          equal:theValue(TPResponseSerializerErrorCodeNotFound)];
    });
  });

  context(@"when received HTTP response code 503", ^{
    NSURLResponse* const kResponse503 =
        [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:kTestURL]
                                    statusCode:503
                                   HTTPVersion:@"HTTP/1.0"
                                  headerFields:@{}];

    it(@"should return valid TFHpple object", ^{
      ONOXMLDocument* document =
          [serializer responseObjectForResponse:kResponse503
                                           data:validHTML
                                          error:&error];
      [[document shouldNot] beNil];
      [[serializer.responseObject should] equal:document];
    });
    it(@"should return suitable error code", ^{
      [serializer responseObjectForResponse:kResponse503
                                       data:validHTML
                                      error:&error];
      [[error.domain should] equal:kTPResponseSerializerErrorDomain];
      [[theValue(error.code) should]
          equal:theValue(TPResponseSerializerErrorCodeServiceUnavailable)];
    });
  });

  context(@"when received any other invalid response", ^{
    NSURLResponse* const kResponseInvalid =
        [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:kTestURL]
                                    statusCode:505
                                   HTTPVersion:@"HDDP"
                                  headerFields:@{}];

    it(@"should return valid TFHpple object", ^{
      ONOXMLDocument* document =
          [serializer responseObjectForResponse:kResponseInvalid
                                           data:validHTML
                                          error:&error];
      [[document shouldNot] beNil];
      [[serializer.responseObject should] equal:document];
    });
    it(@"should return error code @TPHppleResponseSerializerErrorCodeOther", ^{
      [serializer responseObjectForResponse:kResponseInvalid
                                       data:validHTML
                                      error:&error];
      [[error.domain should] equal:kTPResponseSerializerErrorDomain];
      [[theValue(error.code) should]
          equal:theValue(TPResponseSerializerErrorCodeOther)];
    });
  });
});

SPEC_END