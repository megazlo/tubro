//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "AFURLResponseSerialization.h"

#import "TPResponseSerializer.h"

@interface TPJSONResponseSerializer :
    AFJSONResponseSerializer<TPResponseSerializerProtocol>

@property (nonatomic, readonly) NSDictionary* responseObject;

@end
