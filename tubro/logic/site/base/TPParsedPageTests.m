//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPParsedPage_Private.h"
#import "TPRemoteDataSource.h"

SPEC_BEGIN(TPParsedPageSpec)

describe(@"TPParsedPage", ^{
  NSString* const kTestURL = @"http://localhost";
  __block TPParsedPage* page = nil;

  beforeEach(^{
    page = [[TPParsedPage alloc] initWithURL:kTestURL];
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[page shouldNot] beNil];
    });
    it(@"should have status Ready", ^{
      [[theValue(page.status) should]
          equal:theValue(TPRemoteDataSourceStatusReady)];
    });

    context(@"when load was started", ^{
      context(@"when status is 'ready'", ^{
        beforeEach(^{
          page.status = TPRemoteDataSourceStatusReady;
        });

        context(@"when dataSource is null or have status Ready", ^{
          it(@"should have status 'loading'", ^{
            [page loadWithCompletionBlock:^{}];
            [[theValue(page.status) should]
             equal:theValue(TPRemoteDataSourceStatusLoading)];
          });
          it(@"should not start parsing immediately", ^{
            SEL parseSel = @selector(parseOnBackgroundThread);
            [[page shouldNot] receive:parseSel];
            [page loadWithCompletionBlock:^{}];
          });
        });

        context(@"when dataSource have status not Ready", ^{
          __block TPRemoteDataSource* dataSource = nil;

          beforeEach(^{
            dataSource = [TPRemoteDataSource nullMock];
            [dataSource stub:@selector(status)
                   andReturn:theValue(TPRemoteDataSourceStatusOK)];
            page.dataSource = dataSource;
          });

          it(@"should not start remote source loading", ^{
            [[page.dataSource shouldNot] receive:@selector(loadWithCompletionBlock:)];
            [page loadWithCompletionBlock:^{}];
          });
          it(@"should start parsing immediately", ^{
            SEL parseSel = @selector(parseOnBackgroundThread);
            [[page should] receive:parseSel];
            [page loadWithCompletionBlock:^{}];
          });
        });
      });

      context(@"when status isn't 'ready'", ^{
        beforeEach(^{
          page.status = TPRemoteDataSourceStatusOK;
        });

        it(@"should raise", ^{
          [[theBlock(^{
            [page loadWithCompletionBlock:^{}];
          }) should] raise];
        });
      });
    });
  });

  context(@"when parse was occured", ^{
    it(@"should copy remote data status", ^{
      __block TPRemoteDataSourceStatus status = TPRemoteDataSourceStatusReady;
      TPRemoteDataSourceStatus dataSourceStatus =
          TPRemoteDataSourceStatusLoadingError;
      TPRemoteDataSource* dataSource = [TPRemoteDataSource nullMock];
      [dataSource stub:@selector(status)
             andReturn:theValue(dataSourceStatus)];
      [page startOrContinueParseWithDataSource:dataSource
                               completionBlock:^{
          status = page.status;
      }];
      [[expectFutureValue(theValue(status)) shouldEventually]
          equal:theValue(dataSourceStatus)];
    });
  });
});

SPEC_END