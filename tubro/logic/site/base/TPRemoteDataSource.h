//  Created by Evgeniy Krasichkov on 09.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPRemoteDataSourceProtocol.h"

@class TPRemoteDataSource;

typedef void (^TPRemoteDataSourceLoadCompletionBlock)(void);
typedef void (^TPRemoteDataSourceLoadPartialDataRetrieveBlock)(TPRemoteDataSource*);

@interface TPRemoteDataSource : NSObject <TPRemoteDataSourceProtocol, NSCopying>

@property (nonatomic, copy, readonly) NSString* url;
@property (nonatomic, copy, readonly) NSString* method;
@property (nonatomic, copy, readonly) NSDictionary* parameters;
@property (nonatomic, copy, readonly) NSString* responseType;

@property (nonatomic, copy, readonly) NSString* responseURL;
@property (nonatomic, strong, readonly) id responseObject;

- (instancetype)initWithURL:(NSString*)url
                     method:(NSString*)method
                 parameters:(NSDictionary*)parameters
               responseType:(NSString*)responseType;

- (void)loadWithCompletionBlock:
    (TPRemoteDataSourceLoadCompletionBlock)completionBlock
      partialDataRetrieveBlock:
    (TPRemoteDataSourceLoadPartialDataRetrieveBlock)partialDataRetrieveBlock;

@end
