//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPDomainProtocol.h"

@interface TPDomain : NSObject <TPDomainProtocol>

+ (instancetype)domainWithTitle:(NSString*)title url:(NSString*)url;

@end
