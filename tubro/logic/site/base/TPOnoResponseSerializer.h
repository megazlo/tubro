//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "AFURLResponseSerialization.h"

#import "TPResponseSerializer.h"

@class ONOXMLDocument;

@interface TPOnoResponseSerializer :
    AFHTTPResponseSerializer <TPResponseSerializerProtocol>

@property (nonatomic, readonly) ONOXMLDocument* responseObject;

@end
