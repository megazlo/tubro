//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPImage.h"

#import "NSObject+Threads.h"
#import "ONOXMLElement+Util.h"

#import <AFNetworking/AFNetworking.h>
#import <IFUnicodeURL/NSURL+IFUnicodeURL.h>

@interface TPImage ()

@property (nonatomic, copy, readwrite) NSString* url;

@end 

@implementation TPImage

@synthesize url = _url;

+ (instancetype)imageWithOnoElement:(ONOXMLElement*)element {
  NSString* url = element.attributes[@"src"];
  if (!url) return nil;
  NSURL* nsUrl = [NSURL URLWithUnicodeString:[url stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
  if (!nsUrl) return nil;
  TPImage* image = [[TPImage alloc] init];
  image.url = [nsUrl absoluteString];
  return image;
}

+ (NSArray*)imagesWithOnoElement:(ONOXMLElement*)element {
  NSArray* children = element.children;
  NSMutableArray* images = [NSMutableArray array];
  for (ONOXMLElement* child in children) {
    if ([child.tag isEqualToString:@"img"]) {
      TPImage* image = [TPImage imageWithOnoElement:child];
      if (image)
        [images addObject:image];
    } else {
      [images addObjectsFromArray:[self imagesWithOnoElement:child]];
    }
  }
  return [images copy];
}

- (BOOL)isEqual:(id)object {
  if ([object isKindOfClass:[self class]]) {
    TPImage* anotherImage = (TPImage*)object;
    return [anotherImage.url isEqualToString:self.url];
  }
  return NO;
}

@end
