//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import <AFNetworking/AFNetworking.h>
#import <Ono/Ono.h>

#import "TPRemoteDataSource_Private.h"
#import "TPOnoResponseSerializer.h"

SPEC_BEGIN(TPRemoteDataSourceSpec)

describe(@"TPRemoteDataSource", ^{
  NSString* const kTestURL = @"http://localhost";
  NSString* const kTestResponseURL = @"http://localhost/redirect";
  __block TPRemoteDataSource* site = nil;

  beforeEach(^{
    site = [[TPRemoteDataSource alloc] initWithURL:kTestURL
                                            method:@"GET"
                                        parameters:nil
                                      responseType:kTPResponseTypeHpple];
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[site shouldNot] beNil];
    });
    it(@"should have status Ready", ^{
      [[theValue(site.status) should]
          equal:theValue(TPRemoteDataSourceStatusReady)];
    });
    it(@"should set @url property", ^{
      [[site.url should] equal:kTestURL];
    });
  });

  context(@"when load is started", ^{
    it(@"should have status Loading", ^{
      [site loadWithCompletionBlock:^{ }
           partialDataRetrieveBlock:^(TPRemoteDataSource* dS) { }];
      [[theValue(site.status) should]
          equal:theValue(TPRemoteDataSourceStatusLoading)];
    });
  });

  context(@"when response received", ^{
    __block id responseObject = nil;
    __block AFHTTPRequestOperation* operation = nil;
    __block TPOnoResponseSerializer* serializer = nil;
    NSString* const kTestHTML = @"<html></html>";

    beforeEach(^{
      NSData* responseData = [kTestHTML dataUsingEncoding:NSUTF8StringEncoding];
      responseObject = [ONOXMLDocument HTMLDocumentWithData:responseData
                                                      error:nil];
      operation = [AFHTTPRequestOperation nullMock];
      NSURL* responseURL = [NSURL URLWithString:kTestResponseURL];
      NSHTTPURLResponse* response =
          [[NSHTTPURLResponse alloc] initWithURL:responseURL
                                      statusCode:200
                                     HTTPVersion:@"HTTP/1.0"
                                    headerFields:@{}];
      serializer = [[TPOnoResponseSerializer alloc] init];
      [serializer stub:@selector(responseObject) andReturn:responseObject];
      [operation stub:@selector(responseSerializer)
            andReturn:serializer];
      [operation stub:@selector(response) andReturn:response];
    });

    context(@"when request finished with success", ^{
      it(@"should set @status to @TPRemoteDataSourceStatusNotFound", ^{
        [site requestOperation:operation
            loadSuccessWithResponse:responseObject];
        [[theValue(site.status) should]
            equal:theValue(TPRemoteDataSourceStatusOK)];
      });
      it(@"should set @responseObject to object returned", ^{
        [site requestOperation:operation
            loadSuccessWithResponse:responseObject];
        [[site.responseObject should] equal:responseObject];
      });
      it(@"should init @responseURL", ^{
        [site requestOperation:operation
            loadSuccessWithResponse:responseObject];
        [[site.responseURL should] equal:kTestResponseURL];
      });
      it(@"should dispatch completion block", ^{
        __block BOOL completionBlockDispatched = NO;
        site.loadCompletionBlock = ^{
          completionBlockDispatched = YES;
        };
        [site requestOperation:operation
            loadSuccessWithResponse:responseObject];
        [[expectFutureValue(theValue(completionBlockDispatched))
            shouldEventually] beYes];
      });
    });

    context(@"when request finished with error", ^{
      __block NSError* error = nil;

      it(@"should init @responseURL", ^{
        [site requestOperation:operation
            loadFailureWithError:[[NSError alloc] init]];
        [[site.responseURL should] equal:kTestResponseURL];
      });

      context(@"when error is @TPHppleResponseSerializerErrorCodeNotFound", ^{
        beforeEach(^{
          NSInteger errorCode = TPResponseSerializerErrorCodeNotFound;
          error =
              [NSError errorWithDomain:kTPResponseSerializerErrorDomain
                                  code:errorCode
                              userInfo:nil];
        });

        it(@"should set @status to @TPRemoteDataSourceStatusNotFound", ^{
          [site requestOperation:operation loadFailureWithError:error];
          [[theValue(site.status) should]
              equal:theValue(TPRemoteDataSourceStatusNotFound)];
        });
        it(@"should set @responseObject to object returned", ^{
          [site requestOperation:operation loadFailureWithError:error];
          [[site.responseObject should] equal:responseObject];
        });
      });
      context(@"when error is "
               "@TPResponseSerializerErrorCodeServiceUnavailable", ^{
        beforeEach(^{
          NSInteger errorCode =
              TPResponseSerializerErrorCodeServiceUnavailable;
          error =
              [NSError errorWithDomain:kTPResponseSerializerErrorDomain
                                  code:errorCode
                              userInfo:nil];
        });

        it(@"should set @status to "
            "@TPRemoteDataSourceStatusServiceUnavailable", ^{
          [site requestOperation:operation loadFailureWithError:error];
          [[theValue(site.status) should]
              equal:theValue(TPRemoteDataSourceStatusServiceUnavailable)];
        });
        it(@"should set @responseObject to object returned", ^{
          [site requestOperation:operation loadFailureWithError:error];
          [[site.responseObject should] equal:responseObject];
        });
      });
      context(@"when error is @TPResponseSerializerErrorCodeOther", ^{
        beforeEach(^{
          NSInteger errorCode = TPResponseSerializerErrorCodeOther;
          error =
              [NSError errorWithDomain:kTPResponseSerializerErrorDomain
                                  code:errorCode
                              userInfo:nil];
        });

        it(@"should set @status to @TPRemoteDataSourceStatusParseError", ^{
          [site requestOperation:operation loadFailureWithError:error];
          [[theValue(site.status) should]
              equal:theValue(TPRemoteDataSourceStatusParseError)];
        });
        it(@"should set @responseObject to object returned", ^{
          [site requestOperation:operation loadFailureWithError:error];
          [[site.responseObject should] equal:responseObject];
        });
      });
      context(@"when error is @NSURLErrorNotConnectedToInternet", ^{
        beforeEach(^{
          NSInteger errorCode = NSURLErrorNotConnectedToInternet;
          error =
              [NSError errorWithDomain:NSURLErrorDomain
                                  code:errorCode
                              userInfo:nil];
        });

        it(@"should set @status to"
            "@TPRemoteDataSourceStatusNotConnectedToInternet", ^{
          [site requestOperation:operation loadFailureWithError:error];
          [[theValue(site.status) should]
              equal:theValue(TPRemoteDataSourceStatusNotConnectedToInternet)];
        });
      });
      context(@"when error is any other NSURLErrorDomain error", ^{
        beforeEach(^{
          NSInteger errorCode = 1234;
          error =
              [NSError errorWithDomain:NSURLErrorDomain
                                  code:errorCode
                              userInfo:nil];
        });

        it(@"should set @status to @TPRemoteDataSourceStatusLoadingError", ^{
          [site requestOperation:operation loadFailureWithError:error];
          [[theValue(site.status) should]
              equal:theValue(TPRemoteDataSourceStatusLoadingError)];
        });
      });
      context(@"when error is any other error", ^{
        beforeEach(^{
          NSInteger errorCode = 1234;
          error =
              [NSError errorWithDomain:@"34567"
                                  code:errorCode
                              userInfo:nil];
        });

        it(@"should set @status to @TPRemoteDataSourceStatusLoadingError", ^{
          [site requestOperation:operation loadFailureWithError:error];
          [[theValue(site.status) should]
              equal:theValue(TPRemoteDataSourceStatusLoadingError)];
        });
      });
    });
  });
});

SPEC_END