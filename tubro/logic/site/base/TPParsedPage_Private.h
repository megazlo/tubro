//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPParsedPage_Protected.h"

#import "NSObject+Threads.h"
#import "TPResponseSerializer.h"

@interface TPParsedPage ()

@property (nonatomic, assign) BOOL isParsingStarted;
@property (nonatomic, assign) NSUInteger iterationCount;
#ifndef NDEBUG
@property (nonatomic, assign) BOOL isParentParseCalled;
#endif // NDEBUG

@property (nonatomic, copy) TPParsedPageLoadCompletionBlock completionBlock;
@property (nonatomic, copy) TPParsedPageLoadPartialDataRetrieveBlock partialDataRetrieveBlock;

- (void)parseOnBackgroundThread;

@end
