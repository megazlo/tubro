//  Created by Evgeniy Krasichkov on 09.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPRemoteDataSource_Private.h"

@implementation TPRemoteDataSource

- (instancetype)initWithURL:(NSString*)url
                     method:(NSString*)method
                 parameters:(NSDictionary*)parameters
               responseType:(NSString*)responseType {
  if ((self = [super init])) {
    _url = url;
    _method = method;
    _parameters = parameters;
    _responseType = responseType;
  }
  return self;
}

#pragma comment TPRemoteObjectProtocol

- (void)loadWithCompletionBlock:
    (TPRemoteDataSourceLoadCompletionBlock)completionBlock
      partialDataRetrieveBlock:
    (TPRemoteDataSourceLoadPartialDataRetrieveBlock)partialDataRetrieveBlock {
  NSAssert(self.status == TPRemoteDataSourceStatusReady, @"");
  self.status = TPRemoteDataSourceStatusLoading;
  self.loadCompletionBlock = completionBlock;
  self.partialDataRetrieveBlock = partialDataRetrieveBlock;
  [self fetchRemoteData];
}

- (void)fetchRemoteData {
  void (^successBlock)(AFHTTPRequestOperation *operation, id responseObject) =
  ^(AFHTTPRequestOperation *operation, id responseObject) {
      [self requestOperation:operation
          loadSuccessWithResponse:responseObject];
  };
  void (^failureBlock)(AFHTTPRequestOperation *operation, NSError *error) =
  ^(AFHTTPRequestOperation *operation, NSError *error) {
      [self requestOperation:operation loadFailureWithError:error];
  };
  AFHTTPRequestOperation* operation = nil;
  if ([self.method isEqualToString:@"GET"]) {
    operation =
        [self.manager GET:self.url
            parameters:self.parameters
               success:successBlock
               failure:failureBlock];
  } else if ([self.method isEqualToString:@"POST"]) {
    operation =
        [self.manager POST:self.url
            parameters:self.parameters
               success:successBlock 
               failure:failureBlock];
  } else {
    NSAssert(NO, @"Unknown HTTP method: %@", self.method);
  }
  if (self.partialDataRetrieveBlock) {
    __weak AFHTTPRequestOperation* weakOperation = operation;
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead,
                                          long long totalBytesRead,
                                          long long totalBytesExpectedToRead) {
      [self performSelector:@selector(partialDataNotifyReceived:)
                   onThread:[AFURLConnectionOperation networkRequestThread]
                 withObject:weakOperation
              waitUntilDone:NO];
    }];
  }
}

- (void)partialDataNotifyReceived:(AFHTTPRequestOperation*)operation {
  NSData* partialData = [operation.outputStream
      propertyForKey:NSStreamDataWrittenToMemoryStreamKey];
  NSAssert([self tp_isBackgroundThread], @"");
  if ([partialData length]) {
    [self partialDataWasReceived:partialData withResponse:operation.response];
  }
}

- (void)partialDataWasReceived:(NSData*)partialData
                  withResponse:(NSURLResponse*)response {
  NSAssert([self tp_isBackgroundThread], @"");
  if (partialData) {
      id responseObject =
          [self.manager.responseSerializer
              responseObjectForResponse:response
                                   data:partialData
                                  error:nil];
      TPRemoteDataSource* partialDataSource = [[[self class] alloc] init];
      partialDataSource.status = TPRemoteDataSourceStatusPartial;
      partialDataSource.responseObject = responseObject;
      [self tp_dispatchAsyncOnMainThread:^{
          if (!self.isLoaded && self.partialDataRetrieveBlock) {
            self.partialDataRetrieveBlock(partialDataSource);
          }
      }];
  }
}

- (void)sendPartialDataToReceiver {
  NSAssert([self tp_isMainThread], @"");
  [self tp_dispatchAsyncOnBackgroundThread:^{
  }];
}

- (AFHTTPRequestOperationManager*)manager {
  if (!_manager) {
    AFHTTPRequestOperationManager* manager =
        [AFHTTPRequestOperationManager manager];
    if ([self.responseType isEqualToString:kTPResponseTypeHpple]) {
      manager.responseSerializer = [[TPOnoResponseSerializer alloc] init];
    } else if ([self.responseType isEqualToString:kTPResponseTypeJSON]) {
      manager.responseSerializer = [[TPJSONResponseSerializer alloc] init];
    }
    _manager = manager;
  }
  return _manager;
}

- (void)requestOperation:(AFHTTPRequestOperation*)operation
    loadSuccessWithResponse:(id)responseObject {
  NSAssert([self tp_isMainThread], @"");
  self.isLoaded = YES;
  [self requestOperation:operation
       completeWithResponseObject:responseObject
                            error:nil];
}

- (void)requestOperation:(AFHTTPRequestOperation*)operation
    loadFailureWithError:(NSError*)error {
  NSAssert([self tp_isMainThread], @"");
  self.isLoaded = YES;
  id<TPResponseSerializerProtocol> serializer =
      (id<TPResponseSerializerProtocol>)operation.responseSerializer;
  id responseObject = serializer.responseObject;
  [self requestOperation:operation
       completeWithResponseObject:responseObject
                            error:error];
}

- (void)requestOperation:(AFHTTPRequestOperation*)operation
       completeWithResponseObject:(id)responseObject
                            error:(NSError*)error {
  NSAssert([self tp_isMainThread], @"");
  if (!error) {
    self.status = TPRemoteDataSourceStatusOK;
  } else {
    if ([error.domain isEqualToString:kTPResponseSerializerErrorDomain]) {
      [self setStatusAccordingToSerializerErrorCode:error.code];
    } else if ([error.domain isEqualToString:NSURLErrorDomain]) {
      [self setStatusAccordingToNSURLErrorCode:error.code];
    } else {
      self.status = TPRemoteDataSourceStatusLoadingError;
    }
  }
  self.responseObject = responseObject;
  self.responseURL = operation.response.URL.absoluteString;
  if (self.loadCompletionBlock) {
    self.loadCompletionBlock();
    self.loadCompletionBlock = nil;
    self.partialDataRetrieveBlock = nil;
  }
}

- (void)setStatusAccordingToSerializerErrorCode:(NSInteger)errorCode {
  switch (errorCode) {
    case TPResponseSerializerErrorCodeNoError:
      break;
    case TPResponseSerializerErrorCodeNotFound:
      self.status = TPRemoteDataSourceStatusNotFound;
      break;
    case TPResponseSerializerErrorCodeServiceUnavailable:
      self.status = TPRemoteDataSourceStatusServiceUnavailable;
      break;
    case TPResponseSerializerErrorCodeOther:
      self.status = TPRemoteDataSourceStatusParseError;
      break;
  }
}

- (void)setStatusAccordingToNSURLErrorCode:(NSInteger)errorCode {
  switch (errorCode) {
    case NSURLErrorNotConnectedToInternet:
      self.status = TPRemoteDataSourceStatusNotConnectedToInternet;
      break;
    default:
      self.status = TPRemoteDataSourceStatusLoadingError;
      break;
  };
}

#pragma comment NSCopying

- (instancetype)copyWithZone:(NSZone *)zone {
  return self;
}

@end
