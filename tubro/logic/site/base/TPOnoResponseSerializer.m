//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPOnoResponseSerializer.h"

#import "TPResponseSerializer.h"

#import <Ono/Ono.h>

@interface TPOnoResponseSerializer ()

@property (nonatomic, strong) ONOXMLDocument* responseObject;

@end

@implementation TPOnoResponseSerializer

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error {
  NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse *)response;
  NSAssert([httpResponse isKindOfClass:[NSHTTPURLResponse class]], @"");
  if (![self validateResponse:httpResponse
                         data:data
                        error:error]) {
    [self convertError:error forResponse:httpResponse];
  }
//  NSLog(@"%@", [[NSString alloc] initWithData:data
//                                     encoding:NSUTF8StringEncoding]);
  self.responseObject = [ONOXMLDocument HTMLDocumentWithData:data error:nil];
  return self.responseObject;
}

- (void)convertError:(NSError *__autoreleasing *)error
         forResponse:(NSHTTPURLResponse*)response {
  NSInteger errorCode = TPResponseSerializerErrorCodeOther;
  if ((*error).code == NSURLErrorBadServerResponse) {
    if (response.statusCode == 404) {
      errorCode = TPResponseSerializerErrorCodeNotFound;
    } else if (response.statusCode == 503) {
      errorCode = TPResponseSerializerErrorCodeServiceUnavailable;
    }
  }
  *error = [NSError errorWithDomain:kTPResponseSerializerErrorDomain
                               code:errorCode
                           userInfo:nil];
}

@end
