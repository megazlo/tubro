//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPParsedPage.h"

#import "TPRemoteDataSource.h"

@interface TPParsedPage ()

@property (nonatomic, copy) TPRemoteDataSource* dataSource;
@property (nonatomic, assign) TPRemoteDataSourceStatus status;

- (void)startOrContinueParseWithDataSource:(TPRemoteDataSource*)dataSource
                           completionBlock:(TPParsedPageParseCompletionBlock)completionBlock;
- (void)startOrContinueParseWithResponseObject:(id)responseObject
                               completionBlock:(TPParsedPageParseCompletionBlock)completionBlock;
- (void)reportParseIteration;
- (void)stripLastParsedObject;

@end
