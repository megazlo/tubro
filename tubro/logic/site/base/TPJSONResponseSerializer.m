//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPJSONResponseSerializer.h"

#import "TPResponseSerializer.h"

@interface TPJSONResponseSerializer ()

@property (nonatomic, strong) NSDictionary* responseObject;

@end

@implementation TPJSONResponseSerializer

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data 
                          error:(NSError *__autoreleasing *)error {
  NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse *)response;
  NSAssert([httpResponse isKindOfClass:[NSHTTPURLResponse class]], @"");
//  NSLog(@"%@", [[NSString alloc] initWithData:data
//                                     encoding:NSUTF8StringEncoding]);
  self.responseObject = [super responseObjectForResponse:response
                                                    data:data
                                                   error:error];
  //NSAssert([self.responseObject isKindOfClass:[NSDictionary class]], @"");
  if (![self.responseObject isKindOfClass:[NSDictionary class]]) {
    self.responseObject = nil;
  }
  if (error && *error) {
    [self convertError:error forResponse:httpResponse];
  }
  return self.responseObject;
}

- (void)convertError:(NSError *__autoreleasing *)error
         forResponse:(NSHTTPURLResponse*)response {
  NSInteger errorCode = TPResponseSerializerErrorCodeOther;
  if ((*error).code == NSURLErrorBadServerResponse) {
    if (response.statusCode == 404) {
      errorCode = TPResponseSerializerErrorCodeNotFound;
    } else if (response.statusCode == 503) {
      errorCode = TPResponseSerializerErrorCodeServiceUnavailable;
    }
  }
  *error = [NSError errorWithDomain:kTPResponseSerializerErrorDomain
                               code:errorCode
                           userInfo:nil];
}

@end
