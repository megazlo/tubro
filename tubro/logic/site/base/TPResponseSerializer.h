//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

extern NSString* const kTPResponseSerializerErrorDomain;

extern NSString* const kTPResponseTypeHpple;
extern NSString* const kTPResponseTypeJSON;

typedef NS_ENUM(NSInteger, TPResponseSerializerErrorCode) {
  TPResponseSerializerErrorCodeNoError = 0,
  TPResponseSerializerErrorCodeNotFound,
  TPResponseSerializerErrorCodeServiceUnavailable,
  TPResponseSerializerErrorCodeOther,
};

@protocol TPResponseSerializerProtocol <NSObject>

@property (nonatomic, readonly) id responseObject;

@end
