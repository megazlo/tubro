//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

typedef void (^TPImageLoadCompletionBlock)(UIImage*);

@protocol TPImageProtocol

@property (nonatomic, copy, readonly) NSString* url;

@end
