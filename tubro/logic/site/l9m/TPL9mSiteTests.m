//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPL9mSite.h"

SPEC_BEGIN(TPL9mSiteSpec)

describe(@"TPL9mSite", ^{
  __block TPL9mSite* site = nil;

  beforeEach(^{
    site = [[TPL9mSite alloc] init];
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[site shouldNot] beNil];
    });
  });
});

SPEC_END