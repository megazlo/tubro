//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPCommentsProtocol.h"
#import "TPL9mParsedPage.h"

@interface TPL9mComments : TPL9mParsedPage <TPCommentsProtocol>

@end
