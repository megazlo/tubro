//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPPostProtocol.h"
#import "TPL9mParsedPage.h"

@class ONOXMLElement;

@interface TPL9mPost : TPL9mParsedPage <TPPostProtocol>

+ (instancetype)postWithUrl:(NSString*)url
              andOnoElement:(ONOXMLElement*)element
    commentsCountOnoElement:(ONOXMLElement*)commentsCountElement;
+ (instancetype)postWithOnoElement:(ONOXMLElement*)element;

@end
