//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPL9mParsedPage.h"
#import "TPParsedPage_Private.h"

SPEC_BEGIN(TPL9ParsedPageSpec)

describe(@"TPL9mParsedPage", ^{
  __block TPL9mParsedPage* page = nil;
  NSString* const kTestURL = @"http://localhost";
  NSString* const kTestLoginURL = @"http://localhost/login/";

  beforeEach(^{
    page = [[TPL9mParsedPage alloc] initWithURL:kTestURL];
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[page shouldNot] beNil];
    });
  });

  context(@"when url is redirected to /login/ page", ^{
    __block TPRemoteDataSource* dataSource = nil;
    beforeEach(^{
      dataSource = [TPRemoteDataSource nullMock];
      [dataSource stub:@selector(responseURL)
             andReturn:theValue(kTestLoginURL)];
    });

    context(@"when data source status is OK", ^{
      it(@"should set status LoginRequired", ^{
        __block TPRemoteDataSourceStatus status = TPRemoteDataSourceStatusReady;
        TPRemoteDataSourceStatus dataSourceStatus = TPRemoteDataSourceStatusOK;
        [dataSource stub:@selector(status)
               andReturn:theValue(dataSourceStatus)];
        [page startOrContinueParseWithDataSource:dataSource
                                 completionBlock:^{
            status = page.status;
        }];
        [[expectFutureValue(theValue(status)) shouldEventually]
            equal:theValue(TPRemoteDataSourceStatusLoginRequired)];
      });
    });
    context(@"when data source status is not OK", ^{
      it(@"should preserve status", ^{
        __block TPRemoteDataSourceStatus status = TPRemoteDataSourceStatusReady;
        TPRemoteDataSourceStatus dataSourceStatus =
            TPRemoteDataSourceStatusLoadingError;
        [dataSource stub:@selector(status)
               andReturn:theValue(dataSourceStatus)];
        [page startOrContinueParseWithDataSource:dataSource
                                 completionBlock:^{
            status = page.status;
        }];
        [[expectFutureValue(theValue(status)) shouldEventually]
            equal:theValue(dataSourceStatus)];
      });
    });
  });
});

SPEC_END