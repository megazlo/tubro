//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPL9mLoginPage.h"
#import "TPParsedPage_Protected.h"
#import "TPResponseSerializer.h"

SPEC_BEGIN(TPL9mLoginPageSpec)

describe(@"TPL9mLoginPage", ^{
  __block TPL9mLoginPage* page = nil;
  NSString* const kTestURL = @"http://localhost/login";

  beforeEach(^{
    page = [[TPL9mLoginPage alloc] initWithURL:kTestURL];
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[page shouldNot] beNil];
    });
  });

  context(@"when source was set", ^{
    __block TPParsedPage* originPage = nil;
    __block id dataSource = nil;
    
    beforeEach(^{
      originPage = [[TPParsedPage alloc] initWithURL:kTestURL];
      dataSource =
          [[TPRemoteDataSource alloc] initWithURL:kTestURL
                                           method:@"GET"
                                       parameters:nil
                                     responseType:kTPResponseTypeHpple];
      [originPage stub:@selector(dataSource) andReturn:dataSource];
    });

    context(@"when source status is LoginRequired", ^{
      beforeEach(^{
        [originPage stub:@selector(status)
               andReturn:theValue(TPRemoteDataSourceStatusLoginRequired)];
      });

      it(@"should create copy dataSource from source page", ^{
        page.source = originPage;
        [[page.dataSource should] equal:originPage.dataSource];
      });
    });
    context(@"when source status is not LoginRequired", ^{
      beforeEach(^{
        [originPage stub:@selector(status)
               andReturn:theValue(TPRemoteDataSourceStatusOK)];
      });

      it(@"should do nothing", ^{
        page.source = originPage;
        [[page.dataSource should] beNil];
      });
    });
  });
});

SPEC_END