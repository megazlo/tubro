//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPLoginPageProtocol.h"
#import "TPParsedPage.h"

@interface TPL9mLoginAction : TPParsedPage

@property (nonatomic, assign, readonly) BOOL success;

@end
