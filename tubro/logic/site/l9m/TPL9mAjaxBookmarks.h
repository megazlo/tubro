//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPBookmarksProtocol.h"
#import "TPL9mAjaxParsedPage.h"

@interface TPL9mAjaxBookmarks : TPL9mAjaxParsedPage <TPBookmarksProtocol>

@end
