//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPL9mLoginAction_Private.h"
#import "TPParsedPage_Protected.h"

SPEC_BEGIN(TPL9mLoginActionSpec)

describe(@"TPL9mLoginAction", ^{
  __block TPL9mLoginAction* page = nil;
  NSString* const kTestURL = @"http://localhost/login";

  beforeEach(^{
    page = [[TPL9mLoginAction alloc] initWithURL:kTestURL];
  });

  context(@"when data source did load", ^{
    __block TPRemoteDataSource* dataSource = nil;

    beforeEach(^{
      dataSource = [TPRemoteDataSource nullMock];
    });

    context(@"when status is OK", ^{
      beforeEach(^{
        [page stub:@selector(status)
            andReturn:theValue(TPRemoteDataSourceStatusOK)];
      });

      context(@"when response contains status OK", ^{
        NSDictionary* const kTestResponseSuccess = @{ @"status" : @"OK" };

        beforeEach(^{
          [dataSource stub:@selector(responseObject)
                 andReturn:kTestResponseSuccess];
        });

        it(@"should set @success to YES", ^{
          [page dataSourceDidLoad:dataSource];
          [[theValue(page.success) should] beYes];
        });
      });

      context(@"when response does not contains status OK", ^{
        NSDictionary* const kTestResponseFailed = @{ @"test" : @"test" };

        beforeEach(^{
          [dataSource stub:@selector(responseObject)
                 andReturn:kTestResponseFailed];
        });

        it(@"should set @success to NO", ^{
          [page dataSourceDidLoad:dataSource];
          [[theValue(page.success) should] beNo];
        });
      });
    });
    context(@"when status is not OK", ^{
      beforeEach(^{
        [page stub:@selector(status)
            andReturn:theValue(TPRemoteDataSourceStatusLoadingError)];
      });

      it(@"should set @success to NO", ^{
        [page dataSourceDidLoad:dataSource];
        [[theValue(page.success) should] beNo];
      });
    });
  });
});

SPEC_END