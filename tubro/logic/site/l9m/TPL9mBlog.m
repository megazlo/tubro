//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mBlog.h"

#import <Ono/ONOXMLDocument.h>

#import "TPL9mPost.h"
#import "NSObject+Threads.h"
#import "NSString+Utils.h"

@interface TPL9mBlog ()

@property (nonatomic, copy, readwrite) NSString* title;
@property (nonatomic, copy, readwrite) NSArray* posts;

@end

@implementation TPL9mBlog

@synthesize title;
@synthesize posts = _posts;

- (void)startOrContinueParseWithResponseObject:(id)responseObject
                               completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  [super startOrContinueParseWithResponseObject:responseObject
                                completionBlock:^{
      ONOXMLDocument* document = responseObject;
      if (![self parseDocument:document]) {
        if (self.status == TPRemoteDataSourceStatusOK) {
          self.status = TPRemoteDataSourceStatusParseError;
        }
      }
      completionBlock();
  }];
}

- (BOOL)parseDocument:(ONOXMLDocument*)document {
  NSString* titleXPathQuery = @"//head/title";
  ONOXMLElement* titleElement = [document firstChildWithXPath:titleXPathQuery];
  self.title = [[titleElement stringValue] tp_stringByTrimmingAllSpacesTabsAndNewlines];

  NSMutableArray* posts = self.posts ? [self.posts mutableCopy] : [@[] mutableCopy];
  NSString* xPathQueryContainer = @"div[@id='js-posts_holder']/";
  if (self.trimmedDataSource) {
    xPathQueryContainer = @"";
  }
  NSString* xPathQuery = [NSString stringWithFormat:
      @"(//%@div[contains(@class, 'post')])[position()>=%d]", xPathQueryContainer, (int)posts.count];
  [document enumerateElementsWithXPath:xPathQuery
                            usingBlock:^(ONOXMLElement* element,
                                         NSUInteger idx, BOOL* stop) {
      NSAssert([self tp_isBackgroundThread], @"");
      TPL9mPost* post = [TPL9mPost postWithOnoElement:element];
      NSAssert(post || self.status == TPRemoteDataSourceStatusPartial, @"");
      if (post) {
        [posts addObject:post];
      } else {
        *stop = YES;
      }
  }];
  self.posts = posts;
  return posts.count;
}

#pragma comment NSCopying

- (id)copyWithZone:(NSZone *)zone {
  TPL9mBlog* newObject = [super copyWithZone:zone];
  newObject.posts = self.posts;
  return newObject;
}

@end
