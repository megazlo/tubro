//  Created by Evgeniy Krasichkov on 02.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mSite.h"

#import <ChameleonFramework/Chameleon.h>

#import "NSObject+Threads.h"
#import "NSURL+Util.h"
#import "TPDomain.h"
#import "TPL9mBlog.h"
#import "TPL9mAjaxBlog.h"
#import "TPL9mAjaxBookmarks.h"
#import "TPL9mLoginPage.h"
#import "TPL9mComments.h"
#import "TPL9mUserInfoPage.h"
#import "TPRemoteDataSource.h"

NSString* const kTPL9mSiteDomainKey = @"domain";

typedef void (^TPL9mSiteUserInfoBlock)(TPRemoteDataSourceStatus status);

@interface TPL9mSite ()

@property (nonatomic, copy, readwrite) NSString* name;

@property (nonatomic, copy) NSString* domain;

@property (nonatomic, assign) BOOL isUserInfoUpdating;
@property (nonatomic, assign) BOOL isUserInfoValid;
@property (nonatomic, copy) NSString* userId;
@property (nonatomic, copy) NSString* userToken;

@end

@implementation TPL9mSite

@synthesize presenter;

- (id)initWithDomain:(NSString*)domain {
  if ((self = [super init])) {
    _domain = domain;
    _name = domain;
  }
  return self;
}

- (void)getUserTokenWithCompletionBlock:(TPL9mSiteUserTokenBlock)completionBlock {
  if (self.userToken) {
    completionBlock(self.userToken, TPRemoteDataSourceStatusOK);
  } else {
    [self updateUserInfoWithCompletionBlock:^(TPRemoteDataSourceStatus status) {
        completionBlock(self.userToken, status);
    }];
  }
}

- (void)updateUserInfoWithCompletionBlock:(TPL9mSiteUserInfoBlock)completionBlock {
  NSAssert([self tp_isMainThread], @"");
  if (self.isUserInfoUpdating) {
    [self waitForUpdateUserInfoWithCompletionBlock:completionBlock];
  } else {
    self.isUserInfoUpdating = YES;
    NSString* url = [NSString stringWithFormat:@"https://%@/my/", self.domain];
    TPL9mUserInfoPage* infoPage = [[TPL9mUserInfoPage alloc] initWithURL:url];
    [infoPage loadWithCompletionBlock:^{
        if (infoPage.status == TPRemoteDataSourceStatusOK) {
          self.userToken = infoPage.userToken;
          self.userId = infoPage.userId;
          self.isUserInfoValid = YES;
        } else {
          self.isUserInfoValid = NO;
        }
        self.isUserInfoUpdating = NO;
        completionBlock(infoPage.status);
    }];
  }
}

- (void)waitForUpdateUserInfoWithCompletionBlock:(TPL9mSiteUserInfoBlock)completionBlock {
  if (self.isUserInfoUpdating) {
    [self performSelector:@selector(waitForUpdateUserInfoWithCompletionBlock:)
               withObject:completionBlock
               afterDelay:0.1];
  } else {
    if (self.isUserInfoValid) {
      completionBlock(TPRemoteDataSourceStatusOK);
    } else {
      [self updateUserInfoWithCompletionBlock:completionBlock];
    }
  }
}

#pragma comment TPSiteProtocol

- (NSString*)urlToUser:(NSString*)userId {
  return [NSString stringWithFormat:@"https://%@/users/%@", self.domain, userId];
}

- (NSString*)urlToBlog:(NSString*)blogId {
  if (!blogId)
    return [NSString stringWithFormat:@"https://%@", self.domain];
  return [NSString stringWithFormat:@"https://%@.%@", blogId, self.domain];
}

- (NSString*)strippedBlogId:(NSString*)blogId {
  NSString* domainWithDot = [NSString stringWithFormat:@".%@", self.domain];
  return [blogId stringByReplacingOccurrencesOfString:domainWithDot
                                           withString:@""];
}

- (void)navigateToLoginPageWithProperties:(NSDictionary*)properties {
  [self.presenter site:self didNavigateToLoginPageWithProperties:properties];
}

- (TPParsedPage*)loginPageWithSource:(id<TPRemoteDataSourceProtocol>)source {
  NSString* loginPageURL =
      [NSString stringWithFormat:@"https://%@/login", self.domain];
  TPL9mLoginPage* newPage = [[TPL9mLoginPage alloc] initWithURL:loginPageURL];
  TPParsedPage* parcedPageSource = source;
  newPage.dataSource = parcedPageSource.dataSource;
  return newPage;
}

- (TPParsedPage*)blogPageWithUrl:(NSString*)url {
  NSURL* nsUrl = [NSURL URLWithString:url];
  if (nsUrl.pathComponents.count > 1 && [nsUrl.pathComponents[1] isEqualToString:@"my"]) {
    NSString* url = [NSString stringWithFormat:@"https://%@/ajax/interest/moar/", self.domain];
    TPL9mAjaxBlog* newPage =
        [[TPL9mAjaxBlog alloc] initWithURL:url];
    newPage.site = self;
    newPage.parameters = @{
        @"offset" : @"0",
        @"sort:" : @"1",
        @"period" : @"30",
        @"unread" : @"1",
    };
    newPage.method = @"POST";
    return newPage;
  } else {
    TPL9mBlog* newPage = [[TPL9mBlog alloc] initWithURL:url];
    return newPage;
  }
}

- (TPParsedPage*)commentsPageWithUrl:(NSString*)url {
  TPL9mComments* newPage = [[TPL9mComments alloc] initWithURL:url];
  return newPage;
}

- (TPParsedPage<TPBookmarksProtocol>*)bookmarksPage {
  NSString* url = [NSString stringWithFormat:@"https://%@/ajax/bookmarks/domain/list", self.domain];
  TPL9mAjaxBookmarks* newPage =
      [[TPL9mAjaxBookmarks alloc] initWithURL:url];
  newPage.site = self;
  newPage.parameters = @{
  };
  newPage.method = @"POST";
  return newPage;
}

- (NSArray*)defaultDomains {
  return @[
      [TPDomain domainWithTitle:@"Front Page" url:[NSString stringWithFormat:@"https://%@/", self.domain]],
      [TPDomain domainWithTitle:@"My Things" url:[NSString stringWithFormat:@"https://%@/my/", self.domain]],
  ];
}

#pragma mark TPRoutingSchemeProtocol

- (BOOL)canRoteUrl:(NSURL *)url {
  NSString* domain = [url tp_domainWithLevel:2];
  return [domain isEqual:self.domain];
}

- (BOOL)openUrl:(NSURL *)url
    withProperties:(NSDictionary*)properties
   preserveHistory:(BOOL)preserveHistory {
  NSAssert([self canRoteUrl:url], @"");
  BOOL openInEmbeddedBrowser = [properties[@"openInEmbeddedBrowser"] boolValue];
  if (openInEmbeddedBrowser ||
      ![self openNativeViewUrl:url
                withProperties:properties
               preserveHistory:preserveHistory]) {
    [self.presenter site:self
        didNavigateToWebPageWithUrl:url.absoluteString
                         properties:nil
                    preserveHistory:preserveHistory
                      ignoreRouting:openInEmbeddedBrowser];
  }
  return YES;
}

- (BOOL)openNativeViewUrl:(NSURL*)url
    withProperties:(NSDictionary*)properties
   preserveHistory:(BOOL)preserveHistory {
  NSAssert([self canRoteUrl:url], @"");
  if ([url path].length > 1) {
    if ([self openUrlWithPath:url properties:properties preserveHistory:preserveHistory]) {
      return YES;
    }
  } else {
    if ([self openUrlWithoutPath:url properties:properties preserveHistory:preserveHistory]) {
      return YES;
    }
  }
  return NO;
}

- (BOOL)openUrlWithPath:(NSURL*)url
             properties:(NSDictionary*)properties
        preserveHistory:(BOOL)preserveHistory {
  NSArray* pathComponents = [url pathComponents];
  if (pathComponents.count > 1) {
    if ([pathComponents[1] isEqualToString:@"comments"]) {
      [self navigateToCommentsWithUrl:url.absoluteString
                           properties:properties
                      preserveHistory:preserveHistory];
      return YES;
    }
  }
  if (pathComponents.count == 2) {
    if ([pathComponents[1] isEqualToString:@"my"]) {
      [self navigateToBlogWithUrl:url.absoluteString
                       properties:properties
                  preserveHistory:preserveHistory];
      return YES;
    }
  }
  return NO;
}

- (BOOL)openUrlWithoutPath:(NSURL*)url
                properties:(NSDictionary*)properties
           preserveHistory:(BOOL)preserveHistory {
  [self navigateToBlogWithUrl:url.absoluteString
                   properties:properties
              preserveHistory:preserveHistory];
  return YES;
}

- (void)navigateToBlogWithUrl:(NSString*)url
                   properties:(NSDictionary*)properties
              preserveHistory:(BOOL)preserveHistory {
  [presenter site:self
      didNavigateToBlogWithUrl:url
                    properties:properties
               preserveHistory:preserveHistory];
}

- (void)navigateToCommentsWithUrl:(NSString*)url
                       properties:(NSDictionary*)properties
                  preserveHistory:(BOOL)preserveHistory {
  [presenter site:self
      didNavigateToCommentsWithUrl:url
                        properties:properties
                   preserveHistory:preserveHistory];
}

- (UIColor*)primaryColorForPageType:(NSString *)pageType {
//  if ([pageType isEqualToString:kSitePageTypeBlog]) {
//    return [UIColor flatForestGreenColor];
//  }
//  return [UIColor flatGreenColorDark];
  return [UIColor flatForestGreenColor];
}

#pragma protocol NSCoding

- (instancetype)initWithCoder:(NSCoder*)coder {
  self = [super init];
  if (self) {
    NSString* domain = [coder decodeObjectForKey:kTPL9mSiteDomainKey];
    return [self initWithDomain:domain];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder*)coder {
  [coder encodeObject:_domain forKey:kTPL9mSiteDomainKey];
}

@end
