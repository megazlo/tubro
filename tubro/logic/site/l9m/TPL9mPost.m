//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mPost.h"

#import "NSString+Utils.h"
#import "ONOXMLElement+Util.h"
#import "NSAttributedString+Utils.h"
#import "TPImage.h"
#import "TPL9mComments.h"

@interface TPL9mPost ()

@property (nonatomic, copy, readwrite) NSString* userId;
@property (nonatomic, assign, readwrite) NSInteger rating;
@property (nonatomic, copy, readwrite) NSDate* date;

@property (nonatomic, copy, readwrite) NSAttributedString* body;
@property (nonatomic, copy, readwrite) NSArray* images;
@property (nonatomic, copy, readwrite) NSString* commentsUrl;

@property (nonatomic, copy, readwrite) NSString* blogId;
@property (nonatomic, assign, readwrite) NSUInteger commentsCount;
@property (nonatomic, assign, readwrite) NSUInteger newCommentsCount;
@property (nonatomic, assign, readwrite) BOOL isFiveStarsPost;
@property (nonatomic, assign, readwrite) BOOL isGold;

@end

@implementation TPL9mPost

+ (instancetype)postWithOnoElement:(ONOXMLElement*)element {
  return [self postWithUrl:nil andOnoElement:element commentsCountOnoElement:nil];
}

+ (instancetype)postWithUrl:(NSString *)url
              andOnoElement:(ONOXMLElement*)element
    commentsCountOnoElement:(ONOXMLElement*)commentsCountElement {
  ONOXMLElement* postContent =
      [[element tp_firstChildContainsClass:@"dt"]
          tp_firstChildContainsClass:@"dti"];
  if (!postContent) return nil;
  ONOXMLElement* postFooter =
      [[element tp_firstChildContainsClass:@"dd"]
          tp_firstChildContainsClass:@"ddi"];
  if (!postFooter) return nil;
  ONOXMLElement* postCommentLinks =
      [postFooter tp_firstChildContainsClass:@"b-post_comments_links"];
  NSArray* commentsLinks = [postCommentLinks childrenWithTag:@"a"];
  ONOXMLElement* commentsLink = commentsLinks.count ? commentsLinks[0] : nil;
  if (!commentsLink && !url) return nil;
  NSString* commentsText = nil;
  NSString* newCommentsText = nil;
  if (commentsLink) {
    ONOXMLElement* newCommentsLink = commentsLinks.count > 1 ? commentsLinks[1] : nil;
    commentsText = [commentsLink stringValue];
    newCommentsText = [newCommentsLink stringValue];
  } else {
    NSString* commentsCountText =
        [[commentsCountElement stringValue] tp_stringByTrimmingAllSpacesTabsAndNewlines];
    NSArray* commentsCounts = [commentsCountText componentsSeparatedByString:@", "];
    if (commentsCounts.count) commentsText = [commentsCounts firstObject];
    if (commentsCounts.count > 1) newCommentsText = commentsCounts[1];
    if ([newCommentsText isEqual:@"все новые"]) newCommentsText = commentsText;
  }
  if (!commentsText) return nil;
  ONOXMLElement* postVote =
      [[element tp_firstChildContainsClass:@"dd"]
          tp_firstChildContainsClass:@"vote"];
  ONOXMLElement* userElement =
      [postFooter tp_firstChildContainsClass:@"c_user"];
  if (!userElement) return nil;
  ONOXMLElement* dateElement =
      [postFooter tp_firstChildContainsClass:@"js-date"];
  if (!dateElement) return nil;
  ONOXMLElement* blogElement =
      [postFooter tp_firstChildContainsClass:@"b-post_domain"];
  ONOXMLElement* voteResultElement =
      [postVote tp_firstChildContainsClass:@"vote_result"];
  NSString* postUrl = url ? url : commentsLink.attributes[@"href"];
  if (!postUrl) return nil;
  postUrl = [postUrl tp_urlByAddingOmittedProtocol];
  NSString* dateString = [dateElement valueForAttribute:@"data-epoch_date"];
  NSDate* date = [NSDate dateWithTimeIntervalSince1970:[dateString floatValue]];
  if (!date) return nil;

  TPL9mPost* post = [[TPL9mPost alloc] initWithURL:postUrl];
  post.url = postUrl;
  post.blogId = blogElement.stringValue;
  post.commentsCount = [self commentsCountFromString:commentsText];
  post.newCommentsCount = [self commentsCountFromString:newCommentsText];
  NSAssert(post.commentsCount >= post.newCommentsCount, @"");
  post.userId = userElement.stringValue;
  post.rating = [voteResultElement.numberValue integerValue];
  post.date = date;
  NSAttributedString* body = [postContent tp_attributedStringValue];
  post.body = [body tp_attributedStringByTrimming:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  post.images = [TPImage imagesWithOnoElement:postContent];
  return post;
}

+ (NSUInteger)commentsCountFromString:(NSString*)text {
  NSRange range = [text rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  if (range.location != NSNotFound) {
    NSString* countString = [text substringToIndex:range.location];
    return [countString integerValue];
  }
  return 0;
}

- (BOOL)isEqual:(id)object {
  if ([object isKindOfClass:[self class]]) {
    TPL9mPost* anotherPost = (TPL9mPost*)object;
    return [anotherPost.body isEqualToAttributedString:self.body] &&
           [anotherPost.images isEqual:self.images] &&
           anotherPost.newCommentsCount == self.newCommentsCount &&
           anotherPost.commentsCount == self.commentsCount;
  }
  return NO;
}

@end
