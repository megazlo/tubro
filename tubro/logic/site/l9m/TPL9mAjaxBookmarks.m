//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mAjaxBookmarks.h"

#import <Ono/Ono.h>

#import "TPDomain.h"
#import "NSObject+Threads.h"

@interface TPL9mAjaxBookmarks ()

@property (nonatomic, copy, readwrite) NSArray* domains;

@end

@implementation TPL9mAjaxBookmarks

- (void)startOrContinueParseWithResponseObject:(id)responseObject
                               completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  [super startOrContinueParseWithResponseObject:responseObject
                                completionBlock:^{
      NSAssert([self tp_isBackgroundThread], @"");
      NSDictionary* ajax = responseObject;
      [self parseAJAX:ajax withCompletionBlock:completionBlock];
  }];
}

- (void)parseAJAX:(NSDictionary*)ajax
    withCompletionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  NSAssert([self tp_isBackgroundThread], @"");
  if (ajax && [ajax isKindOfClass:[NSDictionary class]]) {
    NSArray* domains = ajax[@"domains"];
    if (domains && [domains isKindOfClass:[NSArray class]]) {
      NSMutableArray* tpDomains = [@[] mutableCopy];
      for (NSDictionary* domain in domains) {
        if (domain && [domain isKindOfClass:[NSDictionary class]]) {
          NSString* url = domain[@"url"];
          NSString* title = domain[@"name"];
          NSAssert(url && [url isKindOfClass:[NSString class]] &&
                   title && [title isKindOfClass:[NSString class]], @"");
          if (url && [url isKindOfClass:[NSString class]] &&
              title && [title isKindOfClass:[NSString class]]) {
            url = [NSString stringWithFormat:@"https://%@", url];
            TPDomain* domain = [TPDomain domainWithTitle:title url:url];
            [tpDomains addObject:domain];
          }
        }
      }
      self.domains = tpDomains;
    }
  }
  if (!self.domains) {
    self.status = TPRemoteDataSourceStatusParseError;
  }
  completionBlock();
}

@end
