//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mParsedPage.h"

@class TPL9mSite;

@interface TPL9mAjaxParsedPage : TPL9mParsedPage

@property (nonatomic, weak) TPL9mSite* site;

@end
