//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mLoginPage.h"

#import "TPL9mLoginAction.h"
#import "TPParsedPage_Protected.h"
#import "TPResponseSerializer.h"

@implementation TPL9mLoginPage

#pragma comment TPParsedPage

- (void)startOrContinueParseWithResponseObject:(id)responseObject
                               completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  [super startOrContinueParseWithResponseObject:responseObject completionBlock:^{
      if (self.status == TPRemoteDataSourceStatusLoginRequired) {
        self.status = TPRemoteDataSourceStatusOK;
      }
      completionBlock();
  }];
}

#pragma comment TPLoginPageProtocol

- (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
                  captcha:(NSString *)captcha
          completionBlock:(TPLoginCompletionBlock)completionBlock {
  NSDictionary* parameters = @{
      @"username" : username,
      @"password" : password,
      @"forever" : @"1",
  };
  NSString* kLoginActionURL = @"https://leprosorium.ru/ajax/auth/login/";
  TPL9mLoginAction* loginAction =
      [[TPL9mLoginAction alloc] initWithURL:kLoginActionURL];
  loginAction.method = @"POST";
  loginAction.parameters = parameters;
  loginAction.responseType = kTPResponseTypeJSON;
  [loginAction loadWithCompletionBlock:^{
      completionBlock(loginAction.success);
  }];
}

#pragma comment TPSitePageProtocol

- (void)setSource:(TPParsedPage *)source {
  if (source.status == TPRemoteDataSourceStatusLoginRequired) {
    self.dataSource = source.dataSource;
  }
}

@end
