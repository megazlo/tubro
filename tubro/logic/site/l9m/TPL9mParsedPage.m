//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mParsedPage.h"

#import "TPRemoteDataSource.h"

@implementation TPL9mParsedPage

- (void)startOrContinueParseWithDataSource:(TPRemoteDataSource *)dataSource
                           completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  [super startOrContinueParseWithDataSource:dataSource
                            completionBlock:^{
      [self checkIfRedirectedToLoginPage:dataSource];
      completionBlock();
  }];
}

- (void)checkIfRedirectedToLoginPage:(TPRemoteDataSource *)dataSource {
  if (dataSource.status == TPRemoteDataSourceStatusOK &&
      ![dataSource.url isEqualToString:dataSource.responseURL]) {
    NSRange range = [dataSource.responseURL rangeOfString:@"/login/"
                                                  options:NSBackwardsSearch];
    if (range.location != NSNotFound &&
        (range.location + range.length == dataSource.responseURL.length)) {
      self.status = TPRemoteDataSourceStatusLoginRequired;
    }
  }
}

@end
