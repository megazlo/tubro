//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mLoginAction.h"

#import "TPParsedPage_Protected.h"

@interface TPL9mLoginAction ()

@property (nonatomic, assign) BOOL success;

- (void)dataSourceDidLoad:(TPRemoteDataSource *)dataSource;

@end