//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPCommentProtocol.h"
#import "TPL9mParsedPage.h"

@class ONOXMLElement;

@interface TPL9mComment : NSObject <TPCommentProtocol>

+ (instancetype)commentWithOnoElement:(ONOXMLElement*)element;

@end
