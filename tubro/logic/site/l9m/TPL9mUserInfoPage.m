//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mUserInfoPage.h"

#import <Ono/ONOXMLDocument.h>

#import "TPRemoteDataSource.h"

@interface TPL9mUserInfoPage ()

@property (nonatomic, copy, readwrite) NSString* userToken;
@property (nonatomic, copy, readwrite) NSString* userId;

@end

@implementation TPL9mUserInfoPage

- (void)startOrContinueParseWithResponseObject:(id)responseObject
                               completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  [super startOrContinueParseWithResponseObject:responseObject
                                completionBlock:^{
      ONOXMLDocument* document = responseObject;
      if (![self parseDocument:document]) {
        if (self.status != TPRemoteDataSourceStatusPartial) {
          self.status = TPRemoteDataSourceStatusParseError;
        }
      }
      completionBlock();
  }];
}

- (BOOL)parseDocument:(ONOXMLDocument*)document {
  NSString* const scriptXPathQuery = @"(//script)";
  __block NSString* token = nil;
  __block NSString* userId = nil;
  [document enumerateElementsWithXPath:scriptXPathQuery
                            usingBlock:^(ONOXMLElement *element,
                                         NSUInteger idx, BOOL *stop) {
    NSString* prefix = @"login : '";
    NSString* stringValue = element.stringValue;
    NSRange prefixRange = [stringValue rangeOfString:prefix];
    if (prefixRange.location != NSNotFound) {
      NSRange searchRange = NSMakeRange(prefixRange.location + prefixRange.length,
                                        stringValue.length - prefixRange.location - prefixRange.length);
      NSString* const postfix = @"'";
      NSRange postfixRange = [stringValue rangeOfString:postfix options:0 range:searchRange];
      if (postfixRange.location != NSNotFound) {
        userId = [stringValue substringWithRange:NSMakeRange(prefixRange.location + prefixRange.length,
                                                             postfixRange.location - prefixRange.location - prefixRange.length)];

        prefix = @"csrf_token : '";
        stringValue = element.stringValue;
        prefixRange = [stringValue rangeOfString:prefix];
        if (prefixRange.location != NSNotFound) {
          searchRange = NSMakeRange(prefixRange.location + prefixRange.length,
                                    stringValue.length - prefixRange.location - prefixRange.length);
          postfixRange = [stringValue rangeOfString:postfix options:0 range:searchRange];
          if (postfixRange.location != NSNotFound) {
            token = [stringValue substringWithRange:NSMakeRange(prefixRange.location + prefixRange.length,
                                                                postfixRange.location - prefixRange.location - prefixRange.length)];
            *stop = YES;
          }
        }
      }
    }
  }];
  if (!token && self.status != TPRemoteDataSourceStatusPartial)
    return NO;
  self.userToken = token;
  self.userId = userId;
  return YES;
}

@end
