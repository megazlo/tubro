//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mComments.h"

#import "NSObject+Threads.h"
#import "TPL9mComment.h"
#import "TPL9mPost.h"

#import <Ono/Ono.h>

@interface TPL9mComments ()

@property (nonatomic, copy, readwrite) NSString* title;
@property (nonatomic, strong, readwrite) id<TPPostProtocol> post;
@property (nonatomic, copy, readwrite) NSArray* comments;

@end

@implementation TPL9mComments

@synthesize title;
@synthesize post;
@synthesize comments = _comments;

- (void)dealloc {
  self.post = nil;
}

- (void)startOrContinueParseWithResponseObject:(id)responseObject
                               completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  [super startOrContinueParseWithResponseObject:responseObject
                                completionBlock:^{
      ONOXMLDocument* document = responseObject;
      if (![self parseDocument:document]) {
        if (self.status == TPRemoteDataSourceStatusOK) {
          self.status = TPRemoteDataSourceStatusParseError;
        }
      }
      completionBlock();
  }];
}

- (BOOL)parseDocument:(ONOXMLDocument*)document {
  NSString* titleXPathQuery = @"//head/title";
  ONOXMLElement* titleElement = [document firstChildWithXPath:titleXPathQuery];
  self.title = [titleElement stringValue];

  NSString* const postXPathQuery =
      @"(//div[@class='post_comments_page']/div[contains(@class, 'post')])"
      @"[position() > 1][position() < 3]";
  ONOXMLElement* postElement = [document firstChildWithXPath:postXPathQuery];
  NSString* const postCommentsCountXPathQuery =
      @"(//div[@id='js-comments']/div[@class='b-comments_controls']"
      @"/span[@class='b-comments_controls_sort']/span[@class='b-comments_controls_new_count'])";
  ONOXMLElement* postCommentsCountElement =
      [document firstChildWithXPath:postCommentsCountXPathQuery];
  self.post = [TPL9mPost postWithUrl:self.url
                       andOnoElement:postElement
             commentsCountOnoElement:postCommentsCountElement];
  NSAssert(self.post || self.status != TPRemoteDataSourceStatusOK, @"");
  NSMutableArray* comments = self.comments ? [self.comments mutableCopy] : [@[] mutableCopy];
  NSString* const commentsXPathQuery = [NSString stringWithFormat:
      @"(//div[@id='js-comments']/div[@id='js-commentsHolder']/div["
      @"contains(@class, 'comment')])[position()>=%d]",
      (int)comments.count];
  [document enumerateElementsWithXPath:commentsXPathQuery
                            usingBlock:^(ONOXMLElement* element,
                                         NSUInteger idx, BOOL* stop) {
      NSAssert([self tp_isBackgroundThread], @"");
      TPL9mComment* comment = [TPL9mComment commentWithOnoElement:element];
      NSAssert(comment || self.status != TPRemoteDataSourceStatusOK, @"");
      if (comment) {
        [comments addObject:comment];
        self.comments = [comments copy];
        [self reportParseIteration];
      } else {
        *stop = YES;
      }
  }];
  self.comments = [comments copy];
  if ((!postElement || !postCommentsCountElement) &&
      !self.comments.count &&
      self.status != TPRemoteDataSourceStatusPartial) {
    return NO;
  }
  return YES;
}

#pragma comment NSCopying

- (id)copyWithZone:(NSZone *)zone {
  TPL9mComments* newObject = [super copyWithZone:zone];
  newObject.post = self.post;
  newObject.comments = self.comments;
  return newObject;
}

@end
