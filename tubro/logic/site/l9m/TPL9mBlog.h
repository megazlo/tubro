//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPBlogProtocol.h"
#import "TPL9mParsedPage.h"

@interface TPL9mBlog : TPL9mParsedPage <TPBlogProtocol>

@property (nonatomic, assign) BOOL trimmedDataSource;

@end
