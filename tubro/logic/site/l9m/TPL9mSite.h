//  Created by Evgeniy Krasichkov on 02.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import "TPRemoteDataSourceProtocol.h"
#import "TPSiteProtocol.h"

typedef void (^TPL9mSiteUserTokenBlock)(NSString* userToken,
                                        TPRemoteDataSourceStatus status);

@interface TPL9mSite : NSObject <TPSiteProtocol>

- (id)initWithDomain:(NSString*)domain;

- (void)getUserTokenWithCompletionBlock:(TPL9mSiteUserTokenBlock)completionBlock;

@end
