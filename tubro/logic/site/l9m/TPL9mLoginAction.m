//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mLoginAction_Private.h"

@implementation TPL9mLoginAction

- (void)startOrContinueParseWithDataSource:(TPRemoteDataSource *)dataSource
                           completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  [super startOrContinueParseWithDataSource:dataSource
                            completionBlock:^{
      [self dataSourceDidLoad:dataSource];
      completionBlock();
  }];
}

- (void)dataSourceDidLoad:(TPRemoteDataSource *)dataSource {
  if (self.status == TPRemoteDataSourceStatusOK) {
    NSDictionary* dictionary = dataSource.responseObject;
    if ([dictionary[@"status"] isEqualToString:@"OK"]) {
      self.success = YES;
    }
  }
}

@end
