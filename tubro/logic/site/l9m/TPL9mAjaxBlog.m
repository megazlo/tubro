//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPL9mAjaxBlog.h"

#import <Ono/Ono.h>

#import "TPL9mBlog.h"
#import "NSObject+Threads.h"

@interface TPL9mAjaxBlog ()

@property (nonatomic, strong) TPL9mBlog* blog;

@end

@implementation TPL9mAjaxBlog

- (instancetype)initWithURL:(NSString *)url {
  if ((self = [super initWithURL:url])) {
    _blog = [[TPL9mBlog alloc] initWithURL:url];
    _blog.trimmedDataSource = YES;
  }
  return self;
}

- (NSString*)title {
  return self.blog.title;
}

- (NSArray*)posts {
  return self.blog.posts;
}

- (void)startOrContinueParseWithResponseObject:(id)responseObject
                               completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  [super startOrContinueParseWithResponseObject:responseObject
                                completionBlock:^{
      NSAssert([self tp_isBackgroundThread], @"");
      NSDictionary* ajax = responseObject;
      [self parseAJAX:ajax withCompletionBlock:completionBlock];
  }];
}

- (void)parseAJAX:(NSDictionary*)ajax
    withCompletionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  NSAssert([self tp_isBackgroundThread], @"");
  if (ajax && [ajax isKindOfClass:[NSDictionary class]]) {
    NSString* template = ajax[@"template"];
    if (template) {
      ONOXMLDocument* document =
          [ONOXMLDocument HTMLDocumentWithString:template
                                        encoding:NSUTF8StringEncoding
                                           error:nil];
      NSAssert(document, @"");
      if (document) {
        [self.blog startOrContinueParseWithResponseObject:document
                                          completionBlock:completionBlock];
        return;
      } else {
        self.status = TPRemoteDataSourceStatusParseError;
      }
    }
  }
  completionBlock();
}

#pragma comment NSCopying

- (id)copyWithZone:(NSZone *)zone {
  TPL9mAjaxBlog* newObject = [super copyWithZone:zone];
  newObject.blog = [self.blog copyWithZone:zone];
  return newObject;
}

@end
