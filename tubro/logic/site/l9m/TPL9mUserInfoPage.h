//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPParsedPage_Protected.h"
#import "TPL9mParsedPage.h"

@interface TPL9mUserInfoPage : TPL9mParsedPage

@property (nonatomic, copy, readonly) NSString* userToken;
@property (nonatomic, copy, readonly) NSString* userId;

@end
