//  Created by Evgeniy Krasichkov on 02.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@protocol TPSiteProtocol;
@protocol TPSitePresenterProtocol;

@interface TPSiteFactory : NSObject

@property (nonatomic, copy) NSArray* sites;
@property (nonatomic, weak) id<TPSitePresenterProtocol> presenter;

+ (instancetype)sharedInstance;
- (void)registerRoutingSchemes;

@end
