//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPUserGeneratedContentProtocol.h"

@protocol TPCommentProtocol <TPUserGeneratedContentProtocol>

@property (nonatomic, assign, readonly) BOOL unread;
@property (nonatomic, assign, readonly) NSInteger indent;

@end
