//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPRemoteDataSourceProtocol.h"

@protocol TPPostProtocol;

@protocol TPCommentsProtocol <TPRemoteDataSourceProtocol>

@property (nonatomic, copy, readonly) NSString* title;
@property (nonatomic, strong, readonly) id<TPPostProtocol> post;
@property (nonatomic, copy, readonly) NSArray* comments;

@end
