//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@class NSAttributedString;

@protocol TPUserGeneratedContentProtocol <NSObject>

@property (nonatomic, copy, readonly) NSString* url;

@property (nonatomic, copy, readonly) NSString* userId;
@property (nonatomic, assign, readonly) NSInteger rating;
@property (nonatomic, copy, readonly) NSDate* date;

@property (nonatomic, copy, readonly) NSAttributedString* body;
@property (nonatomic, copy, readonly) NSArray* images;

@end
