//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPRemoteDataSourceProtocol.h"

@protocol TPLoginPageProtocol;

typedef void (^TPLoginCompletionBlock)(BOOL success);

@protocol TPLoginPageProtocol <TPRemoteDataSourceProtocol>

- (void)loginWithUsername:(NSString*)username
                 password:(NSString*)password
                  captcha:(NSString*)captcha
          completionBlock:(TPLoginCompletionBlock)completionBlock;

@end
