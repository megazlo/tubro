//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPCommentsProtocol.h"
#import "TPUserGeneratedContentProtocol.h"

@protocol TPPostProtocol <TPUserGeneratedContentProtocol>

@property (nonatomic, copy, readonly) NSString* blogId;
@property (nonatomic, assign, readonly) NSUInteger commentsCount;
@property (nonatomic, assign, readonly) NSUInteger newCommentsCount;
@property (nonatomic, assign, readonly) BOOL isFiveStarsPost;
@property (nonatomic, assign, readonly) BOOL isGold;

@end
