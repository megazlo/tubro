//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPBlogProtocol.h"
#import "TPBookmarksProtocol.h"
#import "TPLoginPageProtocol.h"
#import "TPCommentsProtocol.h"
#import "TPRoutes.h"

@protocol TPSiteProtocol;
@class TPParsedPage;

extern NSString* const kSitePageTypeBlog;
extern NSString* const kSitePageTypeComments;

@protocol TPSitePresenterProtocol <NSObject>

- (void)site:(id<TPSiteProtocol>)site
    didNavigateToLoginPageWithProperties:(NSDictionary*)properties;
- (void)site:(id<TPSiteProtocol>)site
    didNavigateToWebPageWithUrl:(NSString*)url
                     properties:(NSDictionary*)properties
                preserveHistory:(BOOL)preserveHistory
                  ignoreRouting:(BOOL)ignoreRouting;
- (void)site:(id<TPSiteProtocol>)site
    didNavigateToBlogWithUrl:(NSString*)url
                  properties:(NSDictionary*)properties
             preserveHistory:(BOOL)preserveHistory;
- (void)site:(id<TPSiteProtocol>)site
    didNavigateToCommentsWithUrl:(NSString*)url
                      properties:(NSDictionary*)properties
                 preserveHistory:(BOOL)preserveHistory;

@end

@protocol TPSiteProtocol <TPRoutingSchemeProtocol>

@property (nonatomic, weak) id<TPSitePresenterProtocol> presenter;

@property (nonatomic, copy, readonly) NSString* name;

- (void)navigateToLoginPageWithProperties:(NSDictionary*)properties;

- (NSString*)urlToUser:(NSString*)userId;
- (NSString*)urlToBlog:(NSString*)blogId;
- (NSString*)strippedBlogId:(NSString*)blogId;

- (NSArray*)defaultDomains;

- (BOOL)openNativeViewUrl:(NSURL*)url
    withProperties:(NSDictionary*)properties
   preserveHistory:(BOOL)preserveHistory;

- (TPParsedPage<TPBookmarksProtocol>*)bookmarksPage;
- (TPParsedPage<TPLoginPageProtocol>*)loginPageWithSource:(id<TPRemoteDataSourceProtocol>)source;
- (TPParsedPage<TPBlogProtocol>*)blogPageWithUrl:(NSString*)url;
- (TPParsedPage<TPCommentsProtocol>*)commentsPageWithUrl:(NSString*)url;

- (UIColor*)primaryColorForPageType:(NSString*)pageType;

@end
