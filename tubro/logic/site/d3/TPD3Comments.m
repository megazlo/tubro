//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPD3Comments.h"

#import "NSObject+Threads.h"
#import "TPD3Comment.h"
#import "TPD3Post.h"

#import <Ono/Ono.h>

@interface TPD3Comments ()

@property (nonatomic, copy, readwrite) NSString* title;
@property (nonatomic, strong, readwrite) id<TPPostProtocol> post;
@property (nonatomic, copy, readwrite) NSArray* comments;

@end

@implementation TPD3Comments

@synthesize title;
@synthesize post;
@synthesize comments = _comments;

- (void)dealloc {
  self.post = nil;
}

- (void)startOrContinueParseWithResponseObject:(id)responseObject
                               completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  [super startOrContinueParseWithResponseObject:responseObject
                                completionBlock:^{
      ONOXMLDocument* document = responseObject;
      if (![self parseDocument:document]) {
        if (self.status == TPRemoteDataSourceStatusOK) {
          self.status = TPRemoteDataSourceStatusParseError;
        }
      }
      completionBlock();
  }];
}

- (BOOL)parseDocument:(ONOXMLDocument*)document {
  NSString* titleXPathQuery = @"//head/title";
  ONOXMLElement* titleElement = [document firstChildWithXPath:titleXPathQuery];
  self.title = [titleElement stringValue];

  NSString* const postXPathQuery =
      @"(//div[@class='post_comments_page' or @class='b-post_article']/div[contains(@class, 'post')])"
      @"[position() < 2]";
  ONOXMLElement* postElement = [document firstChildWithXPath:postXPathQuery];
  NSAssert(postElement || self.status != TPRemoteDataSourceStatusOK, @"");
  NSString* const postCommentsCountXPathQuery =
      @"(//div[@class='b-comments_controls_new_nav']"
      @"//span[@class='b-comments_count'])";
  ONOXMLElement* postCommentsCountElement =
      [document firstChildWithXPath:postCommentsCountXPathQuery];
  NSAssert(postCommentsCountElement || self.status != TPRemoteDataSourceStatusOK, @"");
  NSString* const postHeaderXPathQuery =
      @"(//div[@class='b-post_header_content']/div[@class='b-post_wrapper']/h1[@class='b-title'])"
      @"[position() < 2]";
  ONOXMLElement* postHeaderElement =
      [document firstChildWithXPath:postHeaderXPathQuery];
  self.post = [TPD3Post postWithUrl:self.url
                      andOnoElement:postElement
            commentsCountOnoElement:postCommentsCountElement
                   headerOnoElement:postHeaderElement];
  NSAssert(self.post || self.status != TPRemoteDataSourceStatusOK, @"");
  NSMutableArray* comments = self.comments ? [self.comments mutableCopy] : [@[] mutableCopy];
  NSString* const commentsXPathQuery = [NSString stringWithFormat:
      @"(//div[@id='js-comments']/div[@id='js-commentsHolder']//div["
      @"contains(@class, 'comment ')])[position()>=%d]",
      (int)comments.count];
  [document enumerateElementsWithXPath:commentsXPathQuery
                            usingBlock:^(ONOXMLElement* element,
                                         NSUInteger idx, BOOL* stop) {
      NSAssert([self tp_isBackgroundThread], @"");
      TPD3Comment* comment = [TPD3Comment commentWithOnoElement:element];
      NSAssert(comment || self.status != TPRemoteDataSourceStatusOK, @"");
      if (comment) {
        [comments addObject:comment];
        self.comments = [comments copy];
        [self reportParseIteration];
      } else {
        *stop = YES;
      }
  }];
  self.comments = [comments copy];
  if ((!postElement || !postCommentsCountElement) &&
      !self.comments.count &&
      self.status != TPRemoteDataSourceStatusPartial) {
    return NO;
  }
  return YES;
}

#pragma comment NSCopying

- (id)copyWithZone:(NSZone *)zone {
  TPD3Comments* newObject = [super copyWithZone:zone];
  newObject.post = self.post;
  newObject.comments = self.comments;
  return newObject;
}

@end
