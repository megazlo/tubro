//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPD3AjaxParsedPage.h"

#import "TPD3Site.h"
#import "TPResponseSerializer.h"

@implementation TPD3AjaxParsedPage

- (void)loadWithCompletionBlock:(TPParsedPageLoadCompletionBlock)completionBlock
       partialDataRetrieveBlock:(TPParsedPageLoadPartialDataRetrieveBlock)partialDataRetrieveBlock {
  NSAssert(self.site, @"");
  [self.site getUserTokenWithCompletionBlock:^(NSString *userToken,
                                               TPRemoteDataSourceStatus status) {
      if (status == TPRemoteDataSourceStatusOK) {
        NSAssert(userToken.length, @"");
        [self loadWithCompletionBlock:completionBlock
             partialDataRetrieveBlock:partialDataRetrieveBlock
                            userToken:userToken];
      } else {
        self.status = status;
        completionBlock();
      }
  }];
}

- (void)loadWithCompletionBlock:(TPParsedPageLoadCompletionBlock)completionBlock
       partialDataRetrieveBlock:(TPParsedPageLoadPartialDataRetrieveBlock)partialDataRetrieveBlock
                      userToken:(NSString*)userToken {
  self.responseType = kTPResponseTypeJSON;
  if (!self.parameters) {
    self.parameters = @{};
  }
  NSDictionary* parameters = [self.parameters mutableCopy];
  [parameters setValue:userToken forKey:@"csrf_token"];
  self.parameters = [parameters copy];

  [super loadWithCompletionBlock:completionBlock
        partialDataRetrieveBlock:partialDataRetrieveBlock];
}

@end
