//  Created by Evgeniy Krasichkov on 02.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import "TPRemoteDataSourceProtocol.h"
#import "TPSiteProtocol.h"

typedef void (^TPD3SiteUserTokenBlock)(NSString* userToken,
                                       TPRemoteDataSourceStatus status);

@interface TPD3Site : NSObject <TPSiteProtocol, NSCoding>

- (id)initWithDomain:(NSString*)domain;

- (void)getUserTokenWithCompletionBlock:(TPD3SiteUserTokenBlock)completionBlock;

@end
