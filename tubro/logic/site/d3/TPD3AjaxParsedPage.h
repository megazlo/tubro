//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPD3ParsedPage.h"

@class TPD3Site;

@interface TPD3AjaxParsedPage : TPD3ParsedPage

@property (nonatomic, weak) TPD3Site* site;

@end
