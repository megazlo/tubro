//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPBookmarksProtocol.h"
#import "TPD3AjaxParsedPage.h"

@interface TPD3AjaxBookmarks : TPD3AjaxParsedPage <TPBookmarksProtocol>

@end
