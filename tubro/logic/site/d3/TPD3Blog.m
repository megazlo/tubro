//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPD3Blog.h"

#import <Ono/ONOXMLDocument.h>

#import "TPD3Post.h"
#import "NSObject+Threads.h"
#import "NSString+Utils.h"

@interface TPD3Blog ()

@property (nonatomic, copy, readwrite) NSString* title;
@property (nonatomic, copy, readwrite) NSArray* posts;

@end

@implementation TPD3Blog

@synthesize title;
@synthesize posts = _posts;

- (void)startOrContinueParseWithResponseObject:(id)responseObject
                               completionBlock:(TPParsedPageParseCompletionBlock)completionBlock {
  [super startOrContinueParseWithResponseObject:responseObject
                                completionBlock:^{
      ONOXMLDocument* document = responseObject;
      if (![self parseDocument:document]) {
        if (self.status == TPRemoteDataSourceStatusOK) {
          self.status = TPRemoteDataSourceStatusParseError;
        }
      }
      completionBlock();
  }];
}

- (BOOL)parseDocument:(ONOXMLDocument*)document {
  NSString* titleXPathQuery = @"//head/title";
  ONOXMLElement* titleElement = [document firstChildWithXPath:titleXPathQuery];
  self.title = [[titleElement stringValue] tp_stringByTrimmingAllSpacesTabsAndNewlines];

  NSMutableArray* posts = self.posts ? [self.posts mutableCopy] : [@[] mutableCopy];
  NSString* xPathQueryContainer = @"div[@id='js-posts_holder']//";
  if (self.trimmedDataSource) {
    xPathQueryContainer = @"";
  }
  NSString* xPathQuery = [NSString stringWithFormat:
      @"(//%@div[@class!='b-posts_holder' and (@class='post' or contains(@class, 'post ') or contains(@class, ' post'))])[position()>=%d]", xPathQueryContainer, (int)posts.count];
  [document enumerateElementsWithXPath:xPathQuery
                            usingBlock:^(ONOXMLElement* element,
                                         NSUInteger idx, BOOL* stop) {
      NSAssert([self tp_isBackgroundThread], @"");
      TPD3Post* post = [TPD3Post postWithOnoElement:element];
      NSAssert(post || self.status == TPRemoteDataSourceStatusPartial, @"");
      if (post) {
        [posts addObject:post];
      } else {
        *stop = YES;
      }
  }];
  self.posts = posts;
  return posts.count;
}

#pragma comment NSCopying

- (id)copyWithZone:(NSZone *)zone {
  TPD3Blog* newObject = [super copyWithZone:zone];
  newObject.posts = self.posts;
  return newObject;
}

@end
