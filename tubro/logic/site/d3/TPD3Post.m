
//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPD3Post.h"

#import "NSString+Utils.h"
#import "ONOXMLElement+Util.h"
#import "NSAttributedString+Utils.h"
#import "TPImage.h"
#import "TPL9mComments.h"

@interface TPD3Post ()

@property (nonatomic, copy, readwrite) NSString* userId;
@property (nonatomic, assign, readwrite) NSInteger rating;
@property (nonatomic, copy, readwrite) NSDate* date;

@property (nonatomic, copy, readwrite) NSAttributedString* body;
@property (nonatomic, copy, readwrite) NSArray* images;
@property (nonatomic, copy, readwrite) NSString* commentsUrl;

@property (nonatomic, copy, readwrite) NSString* blogId;
@property (nonatomic, assign, readwrite) NSUInteger commentsCount;
@property (nonatomic, assign, readwrite) NSUInteger newCommentsCount;
@property (nonatomic, assign, readwrite) BOOL isFiveStarsPost;
@property (nonatomic, assign, readwrite) BOOL isGold;

@end

@implementation TPD3Post

+ (instancetype)postWithOnoElement:(ONOXMLElement*)element {
  return [self postWithUrl:nil
             andOnoElement:element
   commentsCountOnoElement:nil
          headerOnoElement:nil];
}

+ (instancetype)postWithUrl:(NSString*)url
              andOnoElement:(ONOXMLElement*)element
    commentsCountOnoElement:(ONOXMLElement*)commentsCountElement
           headerOnoElement:(ONOXMLElement*)headerElement {
  ONOXMLElement* postContentHolder = [element tp_firstChildContainsClass:@"dt"];
  ONOXMLElement* postContent = nil;
  if (postContentHolder) {
    postContent =
        [postContentHolder tp_firstChildContainsClass:@"dti"];
    if (!postContent)
      postContent = [postContentHolder tp_firstChildContainsClass:@"post_body"];
  } else {
    postContent = [element tp_firstChildContainsClass:@"b-post_article_content"];
  }
  ONOXMLElement* postHeader =
    [postContentHolder firstChildWithTag:@"h3"];
  if (!postHeader) {
    postHeader = [[element tp_firstChildContainsClass:@"b-post_comments_page_header"]
                      firstChildWithTag:@"h3"];
  }
  if (!postHeader) {
    postHeader = headerElement;
  }
  if (!postHeader) return nil;
  ONOXMLElement* postFooter =
      [element tp_firstChildContainsClass:@"dd"];
  if (!postFooter) return nil;
  ONOXMLElement* innerPostFooter =
      [[[postFooter tp_firstChildContainsClass:@"b-post_footer_about"]
          tp_firstChildContainsClass:@"b-i-post_footer_about"]
              tp_firstChildContainsClass:@"b-post_footer_opts"];
  if (innerPostFooter)
    postFooter = innerPostFooter;
  innerPostFooter =
      [[[[postFooter tp_firstChildContainsClass:@"b-post_wrapper"]
          tp_firstChildContainsClass:@"b-post_footer_about"]
              tp_firstChildContainsClass:@"b-i-post_footer_about"]
                  tp_firstChildContainsClass:@"b-post_footer_opts"];
  if (innerPostFooter)
    postFooter = innerPostFooter;
  ONOXMLElement* postCommentLinks =
      [postFooter tp_firstChildContainsClass:@"b-post_comments_links"];
  NSArray* commentsLinks = [postCommentLinks childrenWithTag:@"a"];
  ONOXMLElement* commentsLink = commentsLinks.count ? commentsLinks[0] : nil;
  if (!commentsLink && !url) return nil;
  NSString* commentsText = nil;
  NSString* newCommentsText = nil;
  if (commentsLink) {
    ONOXMLElement* newCommentsLink = commentsLinks.count > 1 ? commentsLinks[1] : nil;
    commentsText = [commentsLink stringValue];
    newCommentsText = [newCommentsLink stringValue];
  } else {
    NSString* commentsCountText =
        [[commentsCountElement stringValue] tp_stringByTrimmingAllSpacesTabsAndNewlines];
    commentsCountText =
        [commentsCountText stringByTrimmingCharactersInSet:
            [NSCharacterSet characterSetWithCharactersInString:@"()"]];
    NSArray* commentsCounts = [commentsCountText componentsSeparatedByString:@", "];
    if (commentsCounts.count) commentsText = [commentsCounts firstObject];
    if (commentsCounts.count > 1) newCommentsText = commentsCounts[1];
    if ([newCommentsText isEqual:@"все новые"]) newCommentsText = commentsText;
  }
  if (!commentsText) return nil;
  ONOXMLElement* postVote =
      [postFooter tp_firstChildContainsClass:@"vote"];
  ONOXMLElement* userElement =
      [postFooter tp_firstChildContainsClass:@"c_user"];
  if (!userElement) return nil;
  ONOXMLElement* dateElement =
      [postFooter tp_firstChildContainsClass:@"js-date"];
  if (!dateElement) return nil;
  ONOXMLElement* blogElement =
      [postFooter tp_firstChildContainsClass:@"b-post_domain"];
  ONOXMLElement* voteResultElement =
      [postVote tp_firstChildContainsClass:@"vote_result"];
  NSString* postUrl = url ? url : commentsLink.attributes[@"href"];
  if (!postUrl) return nil;
  postUrl = [postUrl tp_urlByAddingOmittedProtocol];
  NSString* dateString = [dateElement valueForAttribute:@"data-epoch_date"];
  NSDate* date = [NSDate dateWithTimeIntervalSince1970:[dateString floatValue]];
  if (!date) return nil;

  TPD3Post* post = [[TPD3Post alloc] initWithURL:postUrl];
  post.url = postUrl;
  post.blogId = blogElement.stringValue;
  post.commentsCount = [self commentsCountFromString:commentsText];
  post.newCommentsCount = [self commentsCountFromString:newCommentsText];
  NSAssert(post.commentsCount >= post.newCommentsCount, @"");
  post.userId = userElement.stringValue;
  post.rating = [voteResultElement.numberValue integerValue];
  post.date = date;
  NSAttributedString* body = [postContent tp_attributedStringValue];
  body = [body tp_attributedStringByTrimming:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  NSAttributedString* header = [postHeader tp_attributedStringValue];
  header = [header tp_attributedStringByTrimming:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  header = [header tp_attributedStringByAppendAttributedString:
               [NSAttributedString tp_attributedStringWithString:@"\n"]];
  body = body ? [header tp_attributedStringByAppendAttributedString:body] : header;
  post.body = body;
  post.images = [TPImage imagesWithOnoElement:postContent];
  return post;
}

+ (NSUInteger)commentsCountFromString:(NSString*)text {
  NSRange range = [text rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  if (range.location == NSNotFound) {
    range.location = text.length;
  }
  NSString* countString = [text substringToIndex:range.location];
  return [countString integerValue];
  return 0;
}

- (BOOL)isEqual:(id)object {
  if ([object isKindOfClass:[self class]]) {
    TPD3Post* anotherPost = (TPD3Post*)object;
    return [anotherPost.body isEqualToAttributedString:self.body] &&
           [anotherPost.images isEqual:self.images] &&
           anotherPost.newCommentsCount == self.newCommentsCount &&
           anotherPost.commentsCount == self.commentsCount;
  }
  return NO;
}

@end
