//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPD3Comment.h"

#import "NSString+Utils.h"
#import "ONOXMLElement+Util.h"
#import "NSAttributedString+Utils.h"
#import "TPImage.h"
#import "TPD3Comments.h"

@interface TPD3Comment ()

@property (nonatomic, copy, readwrite) NSString* userId;
@property (nonatomic, copy, readwrite) NSString* userUrl;
@property (nonatomic, assign, readwrite) NSInteger rating;
@property (nonatomic, copy, readwrite) NSDate* date;

@property (nonatomic, copy, readwrite) NSAttributedString* body;
@property (nonatomic, copy, readwrite) NSArray* images;
@property (nonatomic, assign, readwrite) BOOL unread;
@property (nonatomic, assign, readwrite) NSInteger indent;

@end

@implementation TPD3Comment

@synthesize url;

+ (instancetype)commentWithOnoElement:(ONOXMLElement*)element {
  ONOXMLElement* commentInner =
      [element tp_firstChildContainsClass:@"comment_inner"];
  ONOXMLElement* commentContent =
      [commentInner tp_firstChildContainsClass:@"c_body"];
  if (!commentContent) return nil;
  ONOXMLElement* commentFooter =
      [commentInner tp_firstChildContainsClass:@"c_footer"];
  if (!commentFooter) return nil;
  ONOXMLElement* userElement =
      [commentFooter tp_firstChildContainsClass:@"c_user"];
  if (!userElement) return nil;
  ONOXMLElement* dateElement =
      [commentFooter tp_firstChildContainsClass:@"js-date"];
  if (!dateElement) return nil;
  ONOXMLElement* commentFooterVote =
      [commentFooter tp_firstChildContainsClass:@"vote"];
  ONOXMLElement* voteResultElement =
      [commentFooterVote tp_firstChildContainsClass:@"vote_result"];
  NSString* dateString = [dateElement valueForAttribute:@"data-epoch_date"];
  NSDate* date = [NSDate dateWithTimeIntervalSince1970:[dateString floatValue]];
  if (!date) return nil;

  TPD3Comment* comment = [[TPD3Comment alloc] init];
  comment.userId = userElement.stringValue;
  comment.rating = [voteResultElement.numberValue integerValue];
  comment.date = date;
  NSAttributedString* body = [commentContent tp_attributedStringValue];
  comment.body = [body tp_attributedStringByTrimming:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  comment.images = [TPImage imagesWithOnoElement:commentContent];
  comment.unread = [element tp_isContainsClass:@"new"];
  comment.indent = [self indentWithOnoElement:element];
  return comment;
}

+ (NSInteger)indentWithOnoElement:(ONOXMLElement*)element {
  NSArray* classes = [element tp_classes];
  for (NSString* class in classes) {
    NSString* const kIndentPrefix = @"indent_";
    if ([class rangeOfString:kIndentPrefix].location == 0) {
      return [[class stringByReplacingOccurrencesOfString:kIndentPrefix
                                               withString:@""] integerValue];
    }
  }
  NSAssert(NO, @"");
  return 0;
}

@end
