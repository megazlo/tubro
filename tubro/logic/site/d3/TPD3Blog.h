//  Created by Evgeniy Krasichkov on 12.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPBlogProtocol.h"
#import "TPD3ParsedPage.h"

@interface TPD3Blog : TPD3ParsedPage <TPBlogProtocol>

@property (nonatomic, assign) BOOL trimmedDataSource;

@end
