//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPRemoteDataSourceProtocol.h"

@protocol TPBookmarksProtocol <TPRemoteDataSourceProtocol>

@property (nonatomic, copy, readonly) NSArray* domains;

@end
