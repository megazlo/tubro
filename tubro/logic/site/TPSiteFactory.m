//  Created by Evgeniy Krasichkov on 02.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPSiteFactory.h"

#import "TPD3Site.h"
#import "TPRoutes.h"
#import "TPL9mSite.h"

NSString* const kSitePageTypeBlog = @"kSitePageTypeBlog";
NSString* const kSitePageTypeComments = @"kSitePageTypeComments";
NSString* const kTPSiteFactorySitesListKey = @"TPSiteFactorySitesListKey";

@interface TPSiteFactory ()

@end

@implementation TPSiteFactory

@synthesize sites = _sites;

+ (instancetype)sharedInstance {
static TPSiteFactory* sharedInstance = nil;
  if (!sharedInstance) {
    sharedInstance = [[TPSiteFactory alloc] init];
  }
  return sharedInstance;
}

+ (NSArray*)defaultSites {
  return @[ [[TPD3Site alloc] initWithDomain:@"d3.ru"],
            [[TPL9mSite alloc] initWithDomain:@"leprosorium.ru"] ];
}

- (NSArray*)sites {
  if (!_sites) {
    self.sites = [self loadSites];
    if (!_sites) {
      self.sites = [[self class] defaultSites];
    }
  }
  return _sites;
}

- (void)setSites:(NSArray*)sites {
  if (_sites != sites) {
    _sites = [sites copy];
    [self saveSites:_sites];
    [self populatePresenter];
  }
}

- (NSArray*)loadSites {
  NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
  NSData *dataRepresentingSavedArray =
      [currentDefaults objectForKey:kTPSiteFactorySitesListKey];
  if (dataRepresentingSavedArray != nil) {
    return [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
  }
  return nil;
}

- (void)saveSites:(NSArray*)sites {
  NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
  NSData* dataRepresentingSavedArray =
      [NSKeyedArchiver archivedDataWithRootObject:sites];
  [currentDefaults setObject:dataRepresentingSavedArray
                      forKey:kTPSiteFactorySitesListKey];
  [currentDefaults synchronize];
}

- (void)setPresenter:(id<TPSitePresenterProtocol>)presenter {
  if (_presenter != presenter) {
    _presenter = presenter;
    [self populatePresenter];
  }
}

- (void)populatePresenter {
  for (id<TPSiteProtocol> site in self.sites) {
    site.presenter = self.presenter;
  }
}

- (void)registerRoutingSchemes {
  for (id<TPRoutingSchemeProtocol> site in self.sites) {
    [[TPRoutes sharedInstance] addRoutingSchemeWithDefaultPriority:site];
  }
}

@end
