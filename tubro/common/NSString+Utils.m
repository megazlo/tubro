//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "NSString+Utils.h"

@implementation NSString (Utils)

- (instancetype)tp_stringByTrimmingAllSpacesTabsAndNewlines {
  NSString *squashed = [self tp_stringByRemovingAllExcessSpacesTabsAndNewlines];

  return [squashed stringByTrimmingCharactersInSet:
              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (instancetype)tp_stringByRemovingAllExcessSpacesTabsAndNewlines {
  return
      [self stringByReplacingOccurrencesOfString:@"\\s+"
                                      withString:@" "
                                         options:NSRegularExpressionSearch
                                           range:NSMakeRange(0, self.length)];
}

- (instancetype)tp_urlByAddingOmittedProtocol {
  if ([self rangeOfString:@"//"].location == 0) {
    return [@"http:" stringByAppendingString:self];
  }
  return [self copy];
}

@end
