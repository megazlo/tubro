//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import "TPThread.h"

@interface TPNonThreadSafe : NSObject <TPNonThreadSafeProtocol>

- (id)initOnThread:(TPThread*)thread;

@end
