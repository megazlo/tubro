//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPThread.h"

SPEC_BEGIN(TPThreadSpec)

describe(@"TPThread", ^{
  NSString* const kThreadName = @"TestThread";
  __block TPThread* thread = nil;

  beforeEach(^{
    thread = [[TPThread alloc] initWithLabel:kThreadName];
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[thread shouldNot] beNil];
    });
    it(@"should conform to protocol TPNonThreadSafe", ^{
      [[thread should] conformToProtocol:@protocol(TPNonThreadSafeProtocol)];
    });
    it(@"should be background thread", ^{
      [[theValue([thread isValidThread]) should] beNo];
    });
  });

  context(@"when @dispatchAsyncOnValidThread was called", ^{
    it(@"should call block on valid thread", ^{
      __block BOOL isValidThread = NO;
      [thread dispatchAsyncOnValidThread:^{
          isValidThread = [thread isValidThread];
      }];
      [[expectFutureValue(theValue(isValidThread)) shouldEventually] beYes];
    });
  });
});

SPEC_END