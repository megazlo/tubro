//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPThread.h"

const char* kTPThreadDispatchQueueIdKey = "kTPThreadDispatchQueueIdKey";

@interface TPThread ()

@property (nonatomic, assign) BOOL isMainThread;
@property (nonatomic, strong) dispatch_queue_t dispatchQueue;
@property (nonatomic, copy) NSString* label;

@end

@implementation TPThread

+ (instancetype)mainThread {
static TPThread* mainThread = nil;
static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    if (!mainThread) {
      mainThread = [[TPThread alloc] initMainThread];
    }
  });
  return mainThread;
}

+ (instancetype)parserThread {
static TPThread* parserThread = nil;
static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    if (!parserThread) {
      parserThread = [[TPThread alloc] initWithLabel:@"TPParserThread"];
    }
  });
  return parserThread;
}

- (id)initMainThread {
  if ((self = [super init])) {
    _isMainThread = YES;
  }
  return self;
}

- (id)initWithLabel:(NSString *)label {
  if ((self = [super init])) {
    _dispatchQueue =
        dispatch_queue_create([label UTF8String], DISPATCH_QUEUE_SERIAL);
    dispatch_queue_set_specific(
        _dispatchQueue,
        kTPThreadDispatchQueueIdKey,
        (__bridge void*)self,
        NULL);
  }
  return self;
}

#pragma comment TPNonThreadSafeProtocol

- (BOOL)isValidThread {
  if (self.isMainThread) {
    return [NSThread isMainThread];
  }
  return dispatch_get_specific(kTPThreadDispatchQueueIdKey) ==
         (__bridge void*)self;
}

- (void)dispatchAsyncOnValidThread:(TPNonThreadSafeProtocolBlock)block {
  if ([self isValidThread]) {
    block();
  } else {
    if (self.isMainThread) {
      dispatch_async(dispatch_get_main_queue(), block);
    } else {
      dispatch_async(self.dispatchQueue, block);
    }
  }
}

@end
