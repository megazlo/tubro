//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

typedef void (^TPObjectBlock)(void);

@interface NSObject (Threads)

- (BOOL)tp_isMainThread;
- (BOOL)tp_isBackgroundThread;

- (void)tp_dispatchAsyncOnMainThread:(TPObjectBlock)block;
- (void)tp_dispatchAsyncOnBackgroundThread:(TPObjectBlock)block;

@end
