//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "NSObject+Threads.h"

@implementation NSObject (Threads)

- (BOOL)tp_isMainThread {
  return [NSThread isMainThread];
}

- (BOOL)tp_isBackgroundThread {
  return ![self tp_isMainThread];
}

- (void)tp_dispatchAsyncOnMainThread:(TPObjectBlock)block {
  dispatch_async(dispatch_get_main_queue(), block);
}

- (void)tp_dispatchSyncOnMainThread:(TPObjectBlock)block {
  if ([self tp_isMainThread]) {
    block();
  } else {
    dispatch_sync(dispatch_get_main_queue(), block);
  }
}

- (void)tp_dispatchAsyncOnBackgroundThread:(TPObjectBlock)block {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                 block);
}

@end
