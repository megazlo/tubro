//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import "TPNonThreadSafeProtocol.h"

@interface TPThread : NSObject<TPNonThreadSafeProtocol>

+ (instancetype)mainThread;
+ (instancetype)parserThread;

- (id)initWithLabel:(NSString*)label;

@end
