//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

typedef void (^TPNonThreadSafeProtocolBlock)(void);

@protocol TPNonThreadSafeProtocol <NSObject>

- (BOOL)isValidThread;
- (void)dispatchAsyncOnValidThread:(TPNonThreadSafeProtocolBlock)block;

@end
