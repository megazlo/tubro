//  Created by Evgeniy Krasichkov on 10.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPNonThreadSafe.h"

@interface TPNonThreadSafe ()

@property (nonatomic, strong) TPThread* thread;

@end

@implementation TPNonThreadSafe

- (id)initOnThread:(TPThread *)thread {
  if ((self = [super init])) {
    _thread = thread;
  }
  return self;
}

- (id)init {
  NSAssert(NO, @"use initOnThread instead");
  return nil;
}

#pragma comment TPNonThreadSafeProtocol

- (BOOL)isValidThread {
  return [self.thread isValidThread];
}

- (void)dispatchAsyncOnValidThread:(TPNonThreadSafeProtocolBlock)block {
  [self.thread dispatchAsyncOnValidThread:block];
}

@end
