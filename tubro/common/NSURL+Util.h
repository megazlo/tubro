//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@interface NSURL (Util)

- (NSURL*)tp_urlByReplacingHost:(NSString*)newHost;
- (NSURL*)tp_urlByReplacingScheme:(NSString*)newScheme;

- (NSString*)tp_domainWithLevel:(NSUInteger)level;
- (NSString*)tp_domainComponentWithLevel:(NSUInteger)level;

@end
