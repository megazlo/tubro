// Copyright (c) 2013 Yandex LLC. All rights reserved.
// Author: Vladislav Alekseev <beefon@yandex-team.ru>

#import "NSObject+YBIBRO.h"

#import "NSObject+TryPerformSelector.h"
#import "NSString+YCAdditions.h"

@implementation NSObject (YBIBRO)

+ (NSString*)yb_parseIdentifier:(NSString*)identifier {
  NSString* cachedIdentifier =
      [[self yb_cachedIdentifiers] objectForKey:identifier];
  if (cachedIdentifier) {
    return cachedIdentifier;
  }

  NSString* parsedIdentifier = [self yb_decodeIdentifier:identifier];

  [[self yb_cachedIdentifiers] setObject:parsedIdentifier forKey:identifier];
  return parsedIdentifier;
}

+ (NSCache*)yb_cachedIdentifiers {
  static NSCache* _instance;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _instance = [[NSCache alloc] init];
  });
  return _instance;
}

+ (NSString*)yb_decodeIdentifier:(NSString*)encodedIdentifier {
  NSAssert(encodedIdentifier, @"Identifier to decode should not be nil");
  return [[encodedIdentifier yc_reversedString] yc_base64DecodedString];
}

+ (SEL)yb_actionForIdentifier:(NSString*)identifier {
  SEL action = NSSelectorFromString([self yb_parseIdentifier:identifier]);
  return action;
}

+ (NSString*)yb_identifierForAction:(SEL)action {
  NSString* actionStr = NSStringFromSelector(action);
  return [[actionStr yc_base64EncodedString] yc_reversedString];
}

- (id)yb_performActionWithIdentifier:(NSString*)identifier {
  SEL action = [[self class] yb_actionForIdentifier:identifier];
  return [self yc_tryPerformObjectSelector:action];
}

- (void)yb_performVoidActionWithIdentifier:(NSString*)identifier {
  SEL action = [[self class] yb_actionForIdentifier:identifier];
  [self yc_tryPerformVoidSelector:action];
}

- (id)yb_performActionWithIdentifier:(NSString*)identifier object:(id)object {
  SEL action = [[self class] yb_actionForIdentifier:identifier];
  return [self yc_tryPerformObjectSelector:action withObject:object];
}

- (void)yb_performVoidActionWithIdentifier:(NSString*)identifier
                                    object:(id)object {
  SEL action = [[self class] yb_actionForIdentifier:identifier];
  [self yc_tryPerformVoidSelector:action withObject:object];
}

- (void)yb_performVoidActionWithIdentifier:(NSString*)identifier
                                       int:(int)integer {
  NSInvocation* invocation = [self yb_invocationForIdentifier:identifier];
  [invocation setArgument:&integer atIndex:2];
  [invocation invoke];
}

- (void)yb_performVoidActionWithIdentifier:(NSString*)identifier
                                      BOOL:(BOOL)boolean {
  NSInvocation* invocation = [self yb_invocationForIdentifier:identifier];
  [invocation setArgument:&boolean atIndex:2];
  [invocation invoke];
}

- (int)yb_performIntActionWithIdentifier:(NSString*)identifier {
  NSInvocation* invocation = [self yb_invocationForIdentifier:identifier];
  [invocation invoke];

  int returnValue = 0;
  [invocation getReturnValue:&returnValue];
  return returnValue;
}

- (id)yb_performActionWithIdentifier:(NSString*)identifier int:(int)integer {
  NSInvocation* invocation = [self yb_invocationForIdentifier:identifier];
  [invocation setArgument:&integer atIndex:2];
  [invocation invoke];

  id returnValue = nil;
  [invocation getReturnValue:&returnValue];
  return returnValue;
}

- (double)yb_performDoubleActionWithIdentifier:(NSString*)identifier {
  NSInvocation* invocation = [self yb_invocationForIdentifier:identifier];
  [invocation invoke];

  double returnValue = 0;
  [invocation getReturnValue:&returnValue];
  return returnValue;
}

- (BOOL)yb_performBoolActionWithIdentifier:(NSString*)identifier {
  NSInvocation* invocation = [self yb_invocationForIdentifier:identifier];
  [invocation invoke];

  BOOL returnValue = NO;
  [invocation getReturnValue:&returnValue];
  return returnValue;
}

#pragma mark - Method invocation

- (NSInvocation*)yb_invocationForIdentifier:(NSString*)identifier {
  SEL action = [[self class] yb_actionForIdentifier:identifier];
  return [self yb_methodInvocationForSelectorOrNil:action];
}

- (NSInvocation*)yb_methodInvocationForSelectorOrNil:(SEL)aSelector {
  if (![self respondsToSelector:aSelector]) {
    NSLog(@"Warning: %@ doesn't respond to selector (%@)",
        NSStringFromClass([self class]), NSStringFromSelector(aSelector));
    return nil;
  }
  NSMethodSignature* methodSignature =
      [[self class] instanceMethodSignatureForSelector:aSelector];
  if (methodSignature == nil) {
    methodSignature = [[self class] methodSignatureForSelector:aSelector];
  }
  NSInvocation* invocation =
      [NSInvocation invocationWithMethodSignature:methodSignature];
  [invocation setSelector:aSelector];
  [invocation setTarget:self];
  return invocation;
}

@end
