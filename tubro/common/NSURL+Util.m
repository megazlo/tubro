//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "NSURL+Util.h"

@implementation NSURL (Util)

- (NSURL*)tp_urlByReplacingHost:(NSString*)newHost {
  NSURLComponents* components = [NSURLComponents componentsWithURL:self
                                           resolvingAgainstBaseURL:YES];
  components.host = newHost;
  return [components URL];
}

- (NSURL*)tp_urlByReplacingScheme:(NSString*)newScheme {
  NSURLComponents* components = [NSURLComponents componentsWithURL:self
                                           resolvingAgainstBaseURL:YES];
  components.scheme = newScheme;
  return [components URL];
}

- (NSString*)tp_domainWithLevel:(NSUInteger)level {
  NSAssert(level > 0, @"");
  NSString* host = [self host];
  if (host) {
    NSArray* domainComponents = [host componentsSeparatedByString:@"."];
    if (domainComponents.count >= level) {
      NSString* domain = @"";
      for (NSUInteger componentNumber = domainComponents.count - level;
           componentNumber != domainComponents.count; ++componentNumber) {
        domain =
            [domain stringByAppendingString:domainComponents[componentNumber]];
        if (componentNumber != domainComponents.count - 1) {
          domain = [domain stringByAppendingString:@"."];
        }
      }
      return [domain lowercaseString];
    }
  }
  return nil;
}

- (NSString *)tp_domainComponentWithLevel:(NSUInteger)level {
  NSAssert(level > 0, @"");
  NSString* host = [self host];
  if (host) {
    NSArray* domainComponents = [host componentsSeparatedByString:@"."];
    if (domainComponents.count >= level) {
      return [domainComponents[domainComponents.count - level] lowercaseString];
    }
  }
  return nil;
}

@end
