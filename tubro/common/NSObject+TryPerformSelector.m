// Copyright (c) 2014 Yandex LLC. All rights reserved.
// Author: Andrey Sikerin <asikerin@yandex-team.ru>

#import "NSObject+TryPerformSelector.h"

@implementation NSObject (YCTryPerformSelector)

- (id)yc_tryPerformVoidSelector:(SEL)aSelector {
  id (^doBlock)() = ^id {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self performSelector:aSelector];
#pragma clang diagnostic pop
    return nil;
  };
  return [self yc_tryPerformSelector:aSelector doBlock:doBlock];
}

- (id)yc_tryPerformVoidSelector:(SEL)aSelector withObject:(id)object {
  id (^doBlock)() = ^id {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self performSelector:aSelector withObject:object];
#pragma clang diagnostic pop
    return nil;
  };
  return [self yc_tryPerformSelector:aSelector doBlock:doBlock];
}

- (id)yc_tryPerformObjectSelector:(SEL)aSelector {
  id (^doBlock)() = ^id {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    return [self performSelector:aSelector];
#pragma clang diagnostic pop
  };
  return [self yc_tryPerformSelector:aSelector doBlock:doBlock];
}

- (id)yc_tryPerformObjectSelector:(SEL)aSelector withObject:(id)object {
  id (^doBlock)() = ^id {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    return [self performSelector:aSelector withObject:object];
#pragma clang diagnostic pop
  };
  return [self yc_tryPerformSelector:aSelector doBlock:doBlock];
}

- (id)yc_tryPerformSelector:(SEL)aSelector doBlock:(id (^)())block {
  if ([self respondsToSelector:aSelector]) {
    return block();
  }
  NSLog(@"Warning: %@ doesn't respond to selector (%@)",
      NSStringFromClass([self class]), NSStringFromSelector(aSelector));
  return nil;
}

@end
