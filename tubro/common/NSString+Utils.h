//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@interface NSString (Utils)

- (instancetype)tp_stringByTrimmingAllSpacesTabsAndNewlines;
- (instancetype)tp_stringByRemovingAllExcessSpacesTabsAndNewlines;
- (instancetype)tp_urlByAddingOmittedProtocol;

@end
