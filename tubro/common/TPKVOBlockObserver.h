// Copyright (c) 2015 Yandex LLC. All rights reserved.
// Author: Andrey Mishanin <amishanin@yandex-team.ru>

#import <Foundation/Foundation.h>

typedef void (^TPKVOBlockObserverHandler)(id change);

@interface TPKVOBlockObserver : NSObject

- (instancetype)initWithObject:(id)object
                       keyPath:(NSString*)keyPath
            valueChangeHandler:(TPKVOBlockObserverHandler)valueChangeHandler;

@end
