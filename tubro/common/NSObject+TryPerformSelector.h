// Copyright (c) 2014 Yandex LLC. All rights reserved.
// Author: Andrey Sikerin <asikerin@yandex-team.ru>

#import <Foundation/Foundation.h>

@interface NSObject (YCTryPerformSelector)

- (id)yc_tryPerformVoidSelector:(SEL)aSelector;
- (id)yc_tryPerformVoidSelector:(SEL)aSelector withObject:(id)object;

- (id)yc_tryPerformObjectSelector:(SEL)aSelector;
- (id)yc_tryPerformObjectSelector:(SEL)aSelector withObject:(id)object;

@end
