// Copyright (c) 2013 Yandex LLC. All rights reserved.
// Author: Vladislav Alekseev <beefon@yandex-team.ru>

#ifndef BASE_IOS_NS_OBJECT_YBIBRO_H_
#define BASE_IOS_NS_OBJECT_YBIBRO_H_

#import <Foundation/Foundation.h>

/**
 See detailed documentation:
 https://wiki.yandex-team.ru/yandexmobile/browser/ios/private-apis

 You can use these services to encode identifiers:
 * https://paste.yandex-team.ru/22300
 * or
 * http://www.string-functions.com/base64encode.aspx to encode to Base64
 * http://www.string-functions.com/reverse.aspx to reverse the string
 */

@interface NSObject (YBIBRO)

+ (NSString*)yb_parseIdentifier:(NSString*)identifier;
+ (SEL)yb_actionForIdentifier:(NSString*)identifier;
+ (NSString*)yb_identifierForAction:(SEL)action;

- (id)yb_performActionWithIdentifier:(NSString*)identifier;
- (void)yb_performVoidActionWithIdentifier:(NSString*)identifier;
- (id)yb_performActionWithIdentifier:(NSString*)identifier object:(id)object;
- (void)yb_performVoidActionWithIdentifier:(NSString*)identifier
                                    object:(id)object;
- (void)yb_performVoidActionWithIdentifier:(NSString*)identifier
                                       int:(int)integer;
- (void)yb_performVoidActionWithIdentifier:(NSString*)identifier
                                      BOOL:(BOOL)boolean;

- (int)yb_performIntActionWithIdentifier:(NSString*)identifier;
- (id)yb_performActionWithIdentifier:(NSString*)identifier int:(int)integer;

- (double)yb_performDoubleActionWithIdentifier:(NSString*)identifier;
- (BOOL)yb_performBoolActionWithIdentifier:(NSString*)identifier;

@end

#endif  // BASE_IOS_NS_OBJECT_YBIBRO_H_
