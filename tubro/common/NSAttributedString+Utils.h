//  Created by Evgeniy Krasichkov on 17.08.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@interface NSAttributedString (Utils)

+ (instancetype)tp_attributedStringWithString:(NSString*)string;
+ (instancetype)tp_attributedStringWithString:(NSString*)string
                                          url:(NSString*)url;
+ (NSString*)tp_defaultLinkText;

- (NSAttributedString*)tp_attributedStringByTrimming:(NSCharacterSet*)set;
- (NSAttributedString*)tp_attributedStringByAppendAttributedString:(NSAttributedString*)string;
- (NSAttributedString*)tp_attributedStringByRemovingAllLinks;

@end
