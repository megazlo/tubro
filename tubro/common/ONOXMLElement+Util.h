//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Ono/Ono.h>

@class NSAttributedString;

@interface ONOXMLElement (Util)

- (ONOXMLElement*)tp_firstChildContainsClass:(NSString*)className;
- (NSArray*)tp_classes;
- (BOOL)tp_isContainsClass:(NSString*)className;

- (NSAttributedString*)tp_attributedStringValue;

@end
