// Copyright (c) 2015 Yandex LLC. All rights reserved.
// Author: Andrey Mishanin <amishanin@yandex-team.ru>

#ifndef BASE_IOS_NS_STRING_YCADDITIONS_H_
#define BASE_IOS_NS_STRING_YCADDITIONS_H_

#import <Foundation/Foundation.h>

@interface NSString (YCAdditions)

@property(nonatomic, copy, readonly) NSString* yc_base64EncodedString;
@property(nonatomic, copy, readonly) NSString* yc_base64DecodedString;
@property(nonatomic, copy, readonly) NSString* yc_reversedString;

@end

#endif
