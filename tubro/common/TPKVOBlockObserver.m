// Copyright (c) 2015 Yandex LLC. All rights reserved.
// Author: Andrey Mishanin <amishanin@yandex-team.ru>

#import "TPKVOBlockObserver.h"

static void* kKVOUniqueToken = &kKVOUniqueToken;

@interface TPKVOBlockObserver ()

@property(nonatomic, weak, readonly) id object;
@property(nonatomic, copy, readonly) NSString* keyPath;
@property(nonatomic, copy, readonly) void (^valueChangeHandler)(id);

@end

@implementation TPKVOBlockObserver

- (instancetype)initWithObject:(id)object
                       keyPath:(NSString*)keyPath
            valueChangeHandler:(TPKVOBlockObserverHandler)valueChangeHandler {
  self = [super init];
  if (self) {
    _object = object;
    _keyPath = [keyPath copy];
    _valueChangeHandler = [valueChangeHandler copy];

    [_object addObserver:self
              forKeyPath:_keyPath
                 options:NSKeyValueObservingOptionNew
                 context:kKVOUniqueToken];
  }
  return self;
}

- (void)observeValueForKeyPath:(NSString*)keyPath
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context {
  BOOL superclassShouldObserveChange = context != kKVOUniqueToken;
  if (superclassShouldObserveChange) {
    [super observeValueForKeyPath:keyPath
                         ofObject:object
                           change:change
                          context:context];
    return;
  }

  if ([keyPath isEqualToString:self.keyPath] && self.valueChangeHandler) {
    self.valueChangeHandler(change[NSKeyValueChangeNewKey]);
  }
}

- (void)dealloc {
  [_object removeObserver:self
               forKeyPath:_keyPath
                  context:kKVOUniqueToken];
}

@end
