//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "UIImageView+Util.h"

#import <YLImageView+WebCache.h>

#import "NSObject+Threads.h"
#import "TPMemoryManager.h"

@interface YLImageView (TPMemoryManagedObject) <TPMemoryManagedObject>

@end

@implementation YLImageView (Util)

+ (UIImage*)placeholderImage {
  return [UIImage imageNamed:@"imagePlaceholder"];
}

+ (UIImage*)errorPlaceholderImage {
  return [UIImage imageNamed:@"imageErrorPlaceholder"];
}

- (void)tp_setImageWithUrl:(NSString *)url {
  [self tp_setImageWithUrl:url placeholderImage:nil];
}

- (void)tp_setImageWithUrl:(NSString*)url
          placeholderImage:(UIImage*)placeholderImage {
  TPMemoryManager* memoryManager = [TPMemoryManager sharedInstance];
  if (url.length) {
    if ([memoryManager isMinFreeMemoryAmountAvailable]) {
      NSURL* nsUrl = [NSURL URLWithString:url];
      SDWebImageOptions options = SDWebImageLowPriority |
                                  SDWebImageRetryFailed |
                                  SDWebImageContinueInBackground;
      [self sd_setImageWithURL:nsUrl
              placeholderImage:(placeholderImage ? placeholderImage : [[self class] placeholderImage])
                       options:options
                     completed:^(UIImage* image,
                                 NSData* data,
                                 NSError* error,
                                 SDImageCacheType cacheType,
                                 NSURL* imageURL) {
                         if (error) {
                           [self setImage:[[self class] errorPlaceholderImage]];
                         }
                     }];
      [memoryManager registerMemoryManagedObject:self];
    } else {
      [self setImage:[[self class] placeholderImage]];
    }
  } else {
    [memoryManager unregisterMemoryManagedObject:self];
    [self sd_setImageWithURL:nil];
  }
}

@end

@implementation UIImageView (TPMemoryManagedObject)

#pragma mark TPMemoryManagedObject

- (void)freeMemory {
  [self setImage:[[self class] placeholderImage]];
}

@end
