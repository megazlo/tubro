//  Created by Evgeniy Krasichkov on 11.10.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPMemoryManager.h"

#import <mach/mach.h>
#import <SDWebImage/SDImageCache.h>

#import "NSObject+Threads.h"

const vm_size_t kOptimalFreeMemoryAmount = 200 * 1024 * 1024;
const vm_size_t kMinFreeMemoryAmount = 50 * 1024 * 1024;

vm_size_t usedMemory(void) {
  struct task_basic_info info;
  mach_msg_type_number_t size = sizeof(info);
  kern_return_t kerr = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)&info, &size);
  return (kerr == KERN_SUCCESS) ? info.resident_size : 0; // size in bytes
}

vm_size_t freeMemory(void) {
  mach_port_t host_port = mach_host_self();
  mach_msg_type_number_t host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
  vm_size_t pagesize;
  vm_statistics_data_t vm_stat;

  host_page_size(host_port, &pagesize);
  (void) host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size);
  return vm_stat.free_count * pagesize;
}

@interface TPWeakReference : NSObject

@property (nonatomic, weak) id nonretainedObjectValue;

+ (instancetype)weakReferenceWithObject:(id)object;

@end

@implementation TPWeakReference

- (instancetype)initWithObject:(id)object {
  if ((self = [super init])) {
    _nonretainedObjectValue = object;
  }
  return self;
}

+ (instancetype)weakReferenceWithObject:(id)object {
  return [[self alloc] initWithObject:object];
}

@end

@interface TPMemoryManager ()

@property (nonatomic, strong) NSMutableArray* objects;
@property (nonatomic, assign) BOOL memoryReleaseProhibited;

@end

@implementation TPMemoryManager

+ (instancetype)sharedInstance {
  static TPMemoryManager* sharedInstance = nil;
  if (!sharedInstance) {
    sharedInstance = [[TPMemoryManager alloc] init];
  }
  return sharedInstance;
}

- (instancetype)init {
  if ((self = [super init])) {
    _objects = [[NSMutableArray alloc] init];
  }
  return self;
}

- (void)registerMemoryManagedObject:(id<TPMemoryManagedObject>)object {
  NSAssert([self tp_isMainThread], @"");
  [self unregisterMemoryManagedObject:object];
  [self.objects addObject:[TPWeakReference weakReferenceWithObject:object]];

  //[self checkAndFreeMemoryIfNeeded];
  // Turned off because of instability
}

- (void)unregisterMemoryManagedObject:(id<TPMemoryManagedObject>)object {
  NSAssert([self tp_isMainThread], @"");
  [self removeExpiredReferences];
  NSMutableIndexSet* indexesToRemove = [[NSMutableIndexSet alloc] init];
  NSUInteger index = 0;
  for (TPWeakReference* weakReference in self.objects) {
    id<TPMemoryManagedObject> weakObject = [weakReference nonretainedObjectValue];
    if (weakObject == object) {
      [indexesToRemove addIndex:index];
    }
    ++index;
  }
  [self.objects removeObjectsAtIndexes:indexesToRemove];
}

- (void)removeExpiredReferences {
  NSAssert([self tp_isMainThread], @"");
  NSMutableIndexSet* indexesToRemove = [[NSMutableIndexSet alloc] init];
  NSUInteger index = 0;
  for (TPWeakReference* weakReference in self.objects) {
    id<TPMemoryManagedObject> weakObject = [weakReference nonretainedObjectValue];
    if (!weakObject) {
      [indexesToRemove addIndex:index];
    }
    ++index;
  }
  [self.objects removeObjectsAtIndexes:indexesToRemove];
}

- (void)checkAndFreeMemoryIfNeeded {
  NSAssert([self tp_isMainThread], @"");
  if (!self.memoryReleaseProhibited) {
    vm_size_t freeMemoryAmount = freeMemory();
    //NSLog(@"%lu", (unsigned long)freeMemoryAmount);
    if (freeMemoryAmount < kOptimalFreeMemoryAmount) {
      self.memoryReleaseProhibited = YES;
      [[SDImageCache sharedImageCache] clearMemory];
      [self performSelector:@selector(freeHalfOfObjectsIfNeeded)
                 withObject:nil
                 afterDelay:1.0];
    }
  }
}

- (void)freeHalfOfObjectsIfNeeded {
  NSAssert([self tp_isMainThread], @"");
  vm_size_t freeMemoryAmount = freeMemory();
  //NSLog(@"%lu", (unsigned long)freeMemoryAmount);
  if (freeMemoryAmount < kOptimalFreeMemoryAmount) {
    [self removeExpiredReferences];
    NSMutableIndexSet* indexesToRemove = [[NSMutableIndexSet alloc] init];
    for (NSUInteger object_n = 0; object_n < self.objects.count / 2; ++object_n) {
      id<TPMemoryManagedObject> object = [[self.objects objectAtIndex:object_n] nonretainedObjectValue];
      [object freeMemory];
      [indexesToRemove addIndex:object_n];
    }
    [self.objects removeObjectsAtIndexes:indexesToRemove];
    [self performSelector:@selector(allowMemoryRelease)
               withObject:nil
               afterDelay:1.0];
  }
}

- (void)allowMemoryRelease {
  NSAssert([self tp_isMainThread], @"");
  self.memoryReleaseProhibited = NO;
}

- (BOOL)isMinFreeMemoryAmountAvailable {
  return YES;
//  NSAssert([self tp_isMainThread], @"");
//  vm_size_t freeMemoryAmount = freeMemory();
//  return freeMemoryAmount > kMinFreeMemoryAmount;
}

- (NSUInteger)usedMemory {
  vm_size_t usedMemoryAmount = usedMemory();
  return usedMemoryAmount;
}

- (NSUInteger)freeMemory {
  vm_size_t freeMemoryAmount = freeMemory();
  return freeMemoryAmount;
}

- (NSUInteger)memoryManagedObjectsCount {
  return [self.objects count];
}

@end
