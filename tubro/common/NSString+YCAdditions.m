// Copyright (c) 2015 Yandex LLC. All rights reserved.
// Author: Andrey Mishanin <amishanin@yandex-team.ru>

#import "NSString+YCAdditions.h"

@implementation NSString (YCAdditions)

- (NSString*)yc_base64EncodedString {
  NSData* data = [self dataUsingEncoding:NSUTF8StringEncoding];
  return [data base64EncodedStringWithOptions:0];
}

- (NSString*)yc_base64DecodedString {
  NSData* data =
      [[NSData alloc] initWithBase64EncodedString:self options:0];
  NSString* decodedString =
      [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
  return decodedString;
}

- (NSString*)yc_reversedString {
  NSMutableString* reversedString =
      [NSMutableString stringWithCapacity:[self length]];
  NSStringEnumerationOptions options = NSStringEnumerationReverse |
      NSStringEnumerationByComposedCharacterSequences;
  [self enumerateSubstringsInRange:NSMakeRange(0, [self length])
                           options:options
                        usingBlock:^(NSString* substring,
                                     NSRange substringRange,
                                     NSRange enclosingRange,
                                     BOOL* stop) {
    [reversedString appendString:substring];
  }];
  return [reversedString copy];
}

@end
