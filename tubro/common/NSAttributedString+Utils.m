//  Created by Evgeniy Krasichkov on 17.08.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "NSAttributedString+Utils.h"

#import <UIKit/UIKit.h>

#import "NSString+Utils.h"
#import "ONOXMLElement+Util.h"

@implementation NSAttributedString (Utils)

+ (instancetype)tp_attributedStringWithString:(NSString*)string {
  return [self tp_attributedStringWithString:string url:nil];
}

+ (instancetype)tp_attributedStringWithString:(NSString*)string
                                          url:(NSString*)url {
  NSAttributedString* result = [[NSAttributedString alloc] initWithString:string];
  if (url) {
    NSMutableAttributedString* mutableResult = [result mutableCopy];
    NSDictionary* attributes = @{
        NSLinkAttributeName : url
    };
    [mutableResult setAttributes:attributes range:NSMakeRange(0, mutableResult.length)];
    return [mutableResult copy];
  }
  return result;
}

+ (NSString*)tp_defaultLinkText {
  return @"[link]";
}

- (NSAttributedString*)tp_attributedStringByTrimming:(NSCharacterSet*)set {
  NSCharacterSet *invertedSet = set.invertedSet;
  NSString *string = self.string;
  unsigned int loc, len;

  NSRange range = [string rangeOfCharacterFromSet:invertedSet];
  loc = (range.length > 0) ? (int)range.location : 0;

  range = [string rangeOfCharacterFromSet:invertedSet options:NSBackwardsSearch];
  len = (range.length > 0) ? (int)NSMaxRange(range) - loc : (int)string.length - loc;

  return [self attributedSubstringFromRange:NSMakeRange(loc, len)];
}

- (NSAttributedString*)tp_attributedStringByAppendAttributedString:(NSAttributedString*)string {
  NSMutableAttributedString* result = [self mutableCopy];
  [result appendAttributedString:string];
  return [result copy];
}

- (NSAttributedString*)tp_attributedStringByRemovingAllLinks {
  NSMutableAttributedString* result = [self mutableCopy];
  [result removeAttribute:NSLinkAttributeName
                         range:NSMakeRange(0, result.length)];
  NSCharacterSet* nonWhitespaceCharacters =
      [[NSCharacterSet whitespaceCharacterSet] invertedSet];
  BOOL defaultLinkTextFound = YES;
  do {
    defaultLinkTextFound = NO;
    NSString* string = result.string;
    NSRange range = [string rangeOfString:[[self class] tp_defaultLinkText]];
    if (range.location != NSNotFound) {
      defaultLinkTextFound = YES;
      NSRange nonWhitespaceRange =
          [string rangeOfCharacterFromSet:nonWhitespaceCharacters
                                  options:0
                                    range:NSMakeRange(range.location + range.length,
                                                      string.length - range.location - range.length)];
      NSUInteger removeRangeLength = range.length;
      if (nonWhitespaceRange.location != NSNotFound) {
        removeRangeLength = nonWhitespaceRange.location - range.location;
      }
      [result replaceCharactersInRange:NSMakeRange(range.location, removeRangeLength)
                            withString:@""];
    }
  } while (defaultLinkTextFound);

  return [result copy];
}

@end
