// Copyright (c) 2014 Yandex LLC. All rights reserved.
// Author: Andrey Mishanin <amishanin@yandex-team.ru>

#ifndef TB_REQUIRES_SUPER
#define TB_REQUIRES_SUPER __attribute__((objc_requires_super))
#endif

#ifndef TB_UNAVAILABLE
#define TB_UNAVAILABLE(_reason) __attribute__((unavailable(_reason)))
#endif

#ifndef TB_DEPRECATED
#define TB_DEPRECATED(_reason) __attribute__((deprecated(_reason)))
#endif

#ifndef TB_USED
#define TB_USED __attribute__((used))
#endif

#ifndef TB_FAIL_METHOD_NOT_OVERRIDDEN
#define TB_FAIL_METHOD_NOT_OVERRIDDEN NSAssert(NO, \
    @"You must override %@ in a subclass", NSStringFromSelector(_cmd))
#endif

#ifndef TB_FAIL_NOT_IMPLEMENTED
#define TB_FAIL_NOT_IMPLEMENTED NSAssert(NO, @"Not implemented")
#endif

#define TP_KEY_PATH(OBJ, PATH) \
    @(((void)(NO && ((void)OBJ.PATH, NO)), #PATH))
