// Copyright (c) 2013 Yandex LLC. All rights reserved.
// Author: Vyacheslav Artemev <artemevv@yandex-team.ru>

#import <UIKit/UIKit.h>

@interface UIDevice (YBAdditions)

+ (BOOL)yb_deviceIsiPhone6PlusLike;
+ (BOOL)yb_deviceIsiPhone6Like;
+ (BOOL)yb_deviceIsiPhone5Like;
+ (BOOL)yb_isOSVersionIsEqualOrHigherThan8;
+ (BOOL)yb_isOSVersionIsEqualOrHigherThan7;
+ (BOOL)yb_isOSVersionLowerThanIOS7;
+ (BOOL)yb_isOSVersionEqual:(NSString*)version;

+ (NSUInteger)yb_majorVersion;
+ (NSUInteger)yb_minorVersion;

+ (BOOL)yb_isPhone;
+ (BOOL)yb_hasRetinaDisplay;
+ (BOOL)yb_supportsLiveBlur;
+ (NSString*)yb_platformString;

@end
