// Copyright (c) 2013 Yandex LLC. All rights reserved.
// Author: Andrey Mishanin <amishanin@yandex-team.ru>

#import <UIKit/UIKit.h>

@interface UIViewController (YBContainmentAdditions)

- (void)yb_addViewControlledBy:(UIViewController*)viewController
            asSubviewWithFrame:(CGRect)viewFrame;
- (void)yb_addViewControlledBy:(UIViewController*)viewController
               asSubviewToView:(UIView*)hostView
                     withFrame:(CGRect)viewFrame;

- (void)yb_removeFromParentViewController;
- (void)yb_removeViewControlledBy:(UIViewController*)viewController;

@end
