//  Created by Evgeniy Krasichkov on 11.10.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@protocol TPMemoryManagedObject <NSObject>

- (void)freeMemory;

@end

@interface TPMemoryManager : NSObject

+ (instancetype)sharedInstance;

- (void)registerMemoryManagedObject:(id<TPMemoryManagedObject>)object;
- (void)unregisterMemoryManagedObject:(id<TPMemoryManagedObject>)object;

- (BOOL)isMinFreeMemoryAmountAvailable;

- (NSUInteger)usedMemory;
- (NSUInteger)freeMemory;
- (NSUInteger)memoryManagedObjectsCount;

@end
