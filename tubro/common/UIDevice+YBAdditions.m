// Copyright (c) 2013 Yandex LLC. All rights reserved.
// Author: Vyacheslav Artemev <artemevv@yandex-team.ru>

#import "UIDevice+YBAdditions.h"

#import <GBDeviceInfo/GBDeviceInfo.h>

static inline bool CGSizeEqualToSizeNoOrientation(CGSize size1, CGSize size2) {
  return (size1.width == size2.width && size1.height == size2.height) ||
      (size1.width == size2.height && size1.height == size2.width);
}

@implementation UIDevice (YBAdditions)

+ (GBDeviceDetails*)deviceDetails {
  static GBDeviceDetails* deviceDetails = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    deviceDetails = [GBDeviceInfo deviceDetails];
  });
  return deviceDetails;
}

+ (BOOL)yb_deviceIsiPhone6PlusLike {
  return [self yb_isDeviceSizeEqualToSize:CGSizeMake(414, 736)];
}

+ (BOOL)yb_deviceIsiPhone6Like {
  return [self yb_isDeviceSizeEqualToSize:CGSizeMake(375, 667)];
}

+ (BOOL)yb_deviceIsiPhone5Like {
  return [self yb_isDeviceSizeEqualToSize:CGSizeMake(320, 568)];
}

+ (BOOL)yb_isDeviceSizeEqualToSize:(CGSize)size {
  CGSize screenSize = [UIScreen mainScreen].bounds.size;
  return CGSizeEqualToSizeNoOrientation(screenSize, size);
}

+ (BOOL)yb_isPhone {
  return ([[UIDevice currentDevice] userInterfaceIdiom] ==
          UIUserInterfaceIdiomPhone);
}

+ (BOOL)yb_isOSVersionIsEqualOrHigherThan8 {
  return [self yb_majorVersion] >= 8;
}

+ (BOOL)yb_isOSVersionIsEqualOrHigherThan7 {
  return [self yb_majorVersion] >= 7;
}

+ (BOOL)yb_isOSVersionLowerThanIOS7 {
  return [self yb_majorVersion] < 7;
}

+ (BOOL)yb_isOSVersionEqual:(NSString*)version {
  return [[UIDevice currentDevice].systemVersion isEqualToString:version];
}

+ (NSUInteger)yb_majorVersion {
  return [self deviceDetails].majoriOSVersion;
}

+ (NSUInteger)yb_minorVersion {
  return [self deviceDetails].minoriOSVersion;
}

+ (BOOL)yb_hasRetinaDisplay {
  return ([[UIScreen mainScreen] scale] > 1);
}

+ (BOOL)yb_supportsLiveBlur {
  static BOOL supports = NO;

  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    GBDeviceDetails* details = [GBDeviceInfo deviceDetails];
    if (details.majoriOSVersion < 7) {
      supports = NO;
    } else if (details.model == GBDeviceModelUnknown ||
               details.family == GBDeviceFamilySimulator) {
      supports = YES;
    } else if (details.family == GBDeviceFamilyiPhone) {
      supports = (details.model >= GBDeviceModeliPhone4S);
    } else if (details.family == GBDeviceFamilyiPad) {
      supports = (details.model >= GBDeviceModeliPad4);
    } else if (details.family == GBDeviceFamilyiPod) {
      supports = (details.model >= GBDeviceModeliPod5);
    }
  });

  return supports;
}

+ (NSString*)yb_platformString {
  return [self yb_isPhone] ? @"iphone" : @"ipad";
}

@end
