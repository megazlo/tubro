//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import <YLImageView.h>

@interface YLImageView (Util)

- (void)tp_setImageWithUrl:(NSString*)url;
- (void)tp_setImageWithUrl:(NSString*)url
          placeholderImage:(UIImage*)placeholderImage;

@end
