//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "ONOXMLElement+Util.h"

#import <UIKit/UIKit.h>

#import "NSString+Utils.h"
#import "NSAttributedString+Utils.h"

@implementation ONOXMLElement (Util)

- (ONOXMLElement *)tp_firstChildContainsClass:(NSString *)className {
  for (ONOXMLElement* child in self.children) {
    if ([child tp_isContainsClass:className])
      return child;
  }
  return nil;
}

- (NSArray *)tp_classes {
  NSString* classAttribute = self.attributes[@"class"];
  return [classAttribute componentsSeparatedByString:@" "];
}

- (BOOL)tp_isContainsClass:(NSString *)className {
  return [[self tp_classes] containsObject:className];
}

- (NSAttributedString*)tp_attributedStringValue {
  NSMutableAttributedString* result = [[NSMutableAttributedString alloc] init];
  [self attributedStringValueByAppendToString:result];
  return result;
}

- (void)attributedStringValueByAppendToString:(NSMutableAttributedString*)result {
  for (id child in self.childrenIncludingText) {
    if ([child isKindOfClass:[NSString class]]) {
      NSString* childString = child;
        NSDictionary* attributes = @{
            NSFontAttributeName :
                [UIFont fontWithName:@"HelveticaNeue" size:14.0] };
      NSAttributedString* attributedString =
          [[NSAttributedString alloc] initWithString:[childString tp_stringByRemovingAllExcessSpacesTabsAndNewlines]
                                          attributes:attributes];
      [result appendAttributedString:attributedString];
    } else if ([child isKindOfClass:[ONOXMLElement class]]) {
      ONOXMLElement* childElement = child;
      NSUInteger prevLength = result.length;
      [childElement attributedStringValueByAppendToString:result];
      if ([childElement.tag isEqualToString:@"br"]) {
        [result appendAttributedString:
            [[NSAttributedString alloc] initWithString:@"\n"]];
      }
      NSRange range = NSMakeRange(prevLength,
                                  result.length - prevLength);
      if ([childElement.tag isEqualToString:@"b"]) {
        NSDictionary* attributes = @{
            NSFontAttributeName :
                [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0] };
        [result addAttributes:attributes range:range];
      } else if ([childElement.tag isEqualToString:@"i"]) {
        NSDictionary* attributes = @{
            NSFontAttributeName :
                [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:14.0] };
        [result addAttributes:attributes range:range];
      } else if ([childElement.tag isEqualToString:@"a"]) {
        NSString* url = [childElement valueForAttribute:@"href"];
        if ([NSURL URLWithString:url]) {
          NSAttributedString* linkText =
              [result attributedSubstringFromRange:range];
          if (![[linkText string] tp_stringByTrimmingAllSpacesTabsAndNewlines].length) {
            NSAttributedString* defaultLinkText =
                [[NSAttributedString alloc] initWithString:[NSAttributedString tp_defaultLinkText]];
            [result appendAttributedString:defaultLinkText];
            range.length = defaultLinkText.length;
          }
          NSDictionary* attributes = @{
              NSLinkAttributeName : url
          };
          [result addAttributes:attributes range:range];
        }
      }
    }
  }
}

@end
