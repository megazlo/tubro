// Copyright (c) 2013 Yandex LLC. All rights reserved.
// Author: Andrey Mishanin <amishanin@yandex-team.ru>

#import "UIViewController+YBContainmentAdditions.h"

@implementation UIViewController (YBContainmentAdditions)

- (void)yb_addViewControlledBy:(UIViewController*)viewController
            asSubviewWithFrame:(CGRect)viewFrame {
  [self yb_addViewControlledBy:viewController
               asSubviewToView:self.view
                     withFrame:viewFrame];
}

- (void)yb_addViewControlledBy:(UIViewController*)viewController
               asSubviewToView:(UIView*)hostView
                     withFrame:(CGRect)viewFrame {
  NSParameterAssert(viewController);
  NSParameterAssert(hostView);
  if (viewController == nil || hostView == nil) {
    return;
  }

  [self addChildViewController:viewController];
  viewController.view.frame = viewFrame;
  [hostView addSubview:viewController.view];
  [viewController didMoveToParentViewController:self];
}

- (void)yb_removeFromParentViewController {
  [self willMoveToParentViewController:nil];
  [self.view removeFromSuperview];
  [self removeFromParentViewController];
}

- (void)yb_removeViewControlledBy:(UIViewController*)viewController {
  if (viewController == nil) {
    return;
  }

  NSParameterAssert(viewController.parentViewController == self);
  if (viewController.parentViewController != self) {
    return;
  }

  [viewController yb_removeFromParentViewController];
}

@end
