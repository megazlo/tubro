//  Created by Evgeniy Krasichkov on 16.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@class TPRootViewController;

@interface TPAppDelegate : UIResponder <UIApplicationDelegate>

@property(nonatomic, strong) UIWindow* window;
@property(nonatomic, strong) TPRootViewController* rootViewController;

@end

