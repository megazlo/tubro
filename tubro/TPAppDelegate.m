//  Created by Evgeniy Krasichkov on 16.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPAppDelegate.h"

#import "TPRootViewController.h"
#import "TPTabManager.h"

@interface TPAppDelegate () <UISplitViewControllerDelegate>

@end

@implementation TPAppDelegate

- (BOOL)application:(UIApplication*)application
    didFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
  [UIView performWithoutAnimation:^{ [self.window makeKeyAndVisible]; }];
  return YES;
}

- (UIWindow*)window {
  if (_window == nil) {
    CGRect windowFrame = [[UIScreen mainScreen] bounds];
    _window = [[UIWindow alloc] initWithFrame:windowFrame];
    _window.rootViewController = self.rootViewController;
    self.rootViewController.view.frame = windowFrame;

    // temp
    [self.rootViewController.fsm setState:TPRootStateWebView];
    NSURL* url = [NSURL URLWithString:@"https://yandex.ru"];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    [self.rootViewController.tabManager loadRequest:request];
  }
  return _window;
}

- (TPRootViewController*)rootViewController {
  if (_rootViewController == nil) {
    _rootViewController = [TPRootViewController controller];
  }
  return _rootViewController;
}

@end
