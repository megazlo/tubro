//  Created by Evgeniy Krasichkov on 11.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPTheme.h"

@implementation TPTheme

+ (instancetype)sharedInstance {
  static TPTheme* sharedInstance = nil;
  if (!sharedInstance) {
    sharedInstance = [[self alloc] init];
  }
  return sharedInstance;
}

- (UIColor*)backgroundColor {
  return [[UIColor alloc] initWithRed:255.0/255.0
                                green:43.0/255.0
                                 blue:87.0/255.0
                                alpha:1.0];
}

- (UIColor*)omniboxTitleColor {
  return [[UIColor alloc] initWithRed:226.0/255.0
                                green:255.0/255.0
                                 blue:253.0/255.0
                                alpha:1.0];
}

- (UIColor*)omniboxQueryColor {
  return [[UIColor alloc] initWithRed:130.0/255.0
                                green:129.0/255.0
                                 blue:164.0/255.0
                                alpha:1.0];
}

@end
