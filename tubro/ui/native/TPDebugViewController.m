//  Created by Evgeniy Krasichkov on 09.08.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPDebugViewController.h"

#import <TTTAttributedLabel/TTTAttributedLabel.h>

#import "NSAttributedString+Utils.h"
#import "TPMemoryManager.h"

static CGFloat const kAnimationFrameLength = 0.2;

@interface TPTransparentView : UIView

@end

@implementation TPTransparentView

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent*)event {
  for (UIView* view in self.subviews) {
    if (view.userInteractionEnabled) {
      if ([view pointInside:[view convertPoint:point fromView:self]
                  withEvent:event]) {
        return YES;
      }
    }
  }
  return NO;
}

@end

@interface TPDebugViewController ()

@property(nonatomic, weak) UIView* parentView;

@property(nonatomic, strong) NSTimer* timer;
@property(nonatomic, strong) TTTAttributedLabel* memoryUsage;

@end

@implementation TPDebugViewController

- (instancetype)initWithParentView:(UIView*)parentView {
  if ((self = [super initWithNibName:nil bundle:nil])) {
    _parentView = parentView;
  }
  return self;
}

- (void)loadView {
  self.view = [[TPTransparentView alloc] init];
}

- (void)viewDidLoad {
  self.memoryUsage = [[TTTAttributedLabel alloc] initWithFrame:CGRectZero];
  [self.memoryUsage setBackgroundColor:[UIColor whiteColor]];
  [self.view addSubview:self.memoryUsage];

  [self.view layoutSubviews];

  self.timer = [NSTimer scheduledTimerWithTimeInterval:kAnimationFrameLength
                                                target:self
                                              selector:@selector(onTimerFires:)
                                              userInfo:nil
                                               repeats:YES];
  [super viewDidLoad];
}

- (void)onTimerFires:(NSTimer*)timer {
  NSUInteger freeMemory = [[TPMemoryManager sharedInstance] freeMemory] / 1024 / 1024;
  NSUInteger usedMemory = [[TPMemoryManager sharedInstance] usedMemory] / 1024 / 1024;
  NSUInteger objectsCount = [[TPMemoryManager sharedInstance] memoryManagedObjectsCount];
  NSString* memoryUsageString =
      [NSString stringWithFormat:@"used:%luMB free:%luMB objects:%lu",
          (unsigned long)usedMemory,
          (unsigned long)freeMemory,
          (unsigned long)objectsCount];
  NSAttributedString* memoryUsageAttributedString =
      [NSAttributedString tp_attributedStringWithString:memoryUsageString];
  [self.memoryUsage setText:memoryUsageAttributedString];
}

- (void)viewDidLayoutSubviews {
  self.view.frame = self.parentView.bounds;
  CGRect memoryUsageFrame =
      CGRectMake(0, self.view.bounds.size.height - 14,
                 self.view.bounds.size.width, 14);
  self.memoryUsage.frame = memoryUsageFrame;
}

@end
