//  Created by Evgeniy Krasichkov on 14.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPWebPageViewController.h"

#import "TPRoutes.h"
#import "TPSiteProtocol.h"

@interface TPWebPageViewController () <UIWebViewDelegate>

@end

@implementation TPWebPageViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  self.title = @"Web";

  self.webView.scalesPageToFit = YES;
  [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView
    shouldStartLoadWithRequest:(NSURLRequest *)request
                navigationType:(UIWebViewNavigationType)navigationType {
  if ([request.URL isEqual:request.mainDocumentURL]) {
    NSURL* url = request.URL;
    if (self.ignoreRouting) {
      return YES;
    }
    if (![self.site canRoteUrl:url]) {
      [[TPRoutes sharedInstance] openUrl:request.URL withProperties:nil preserveHistory:YES];
      return NO;
    }
    if ([self.site openNativeViewUrl:url withProperties:nil preserveHistory:YES]) {
      return NO;
    }
  }
  return YES;
}

@end
