//  Created by Evgeniy Krasichkov on 14.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPAppearance.h"
#import "TPParsedPage.h"

@protocol TPSiteProtocol;

@interface TPSitePageViewController : UITableViewController

@property (nonatomic, strong) TPParsedPage* page;

@property (nonatomic, copy) NSString* url;
@property (nonatomic, strong) id<TPSiteProtocol> site;
@property (nonatomic, weak) TPParsedPage* originPage;
@property (nonatomic, strong, readonly) TPSitePageAppearance* appearance;

- (void)prepareForReuse;

- (void)loadPage;

- (void)pageDidLoad;
- (void)pageDidRetrievePartialData:(TPParsedPage*)page;

- (NSArray*)contentArrayForSection:(NSInteger)section;
- (id)contentAtIndexPath:(NSIndexPath*)indexPath;
- (void)setContent:(id)content atIndexPath:(NSIndexPath*)indexPath;

- (void)displayNotificationWithTitle:(NSString*)title
                             message:(NSString*)message;

- (NSString*)titleFromPage:(TPParsedPage*)page;
- (id)contentArrayFromPage:(TPParsedPage*)page forSection:(NSInteger)section;
- (NSInteger)numberOfContentSections;
- (BOOL)newContentReplacesOld;
- (BOOL)needInsertContent:(id)content
                withIndex:(NSInteger)contentIndex
           inContentArray:(NSArray*)contentArray
                  section:(NSInteger)section;
- (void)updateCellAtIndexPath:(NSIndexPath*)indexPath
                  withContent:(id)content;
- (NSInteger)rowWhereInsertContent:(id)content
                         withIndex:(NSInteger)contentIndex
                    inContentArray:(NSArray*)contentArray
                           section:(NSInteger)section
             replaceCurrentContent:(BOOL*)replaceCurrentContent;
- (UITableViewCell*)tableView:(UITableView*)tableView
               cellForContent:(id)content
                  atIndexPath:(NSIndexPath*)indexPath;
- (NSString*)sitePageType;

@end
