//  Created by Evgeniy Krasichkov on 14.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPSitePageViewController.h"

#import <OLGhostAlertView/OLGhostAlertView.h>
#import <TLYShyNavBar/TLYShyNavBarManager.h>

#import "TPLoginPageViewController.h"
#import "TPSitePageAppearance.h"
#import "TPSiteProtocol.h"
#import "TPRemoteDataSource.h"
#import "TPProgressTableViewCell.h"

static NSString* const kProgressCellId = @"LoadingIndicator";

@interface TPSitePageViewController ()

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) NSMutableArray* contentArrayForSection;
@property (nonatomic, strong, readwrite) TPSitePageAppearance* appearance;

@end

@implementation TPSitePageViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  self.shyNavBarManager.scrollView = self.tableView;
  self.shyNavBarManager.expansionResistance = 100;

  [self.tableView registerNib:[self progressCellNib]
       forCellReuseIdentifier:kProgressCellId];
  self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

  UIRefreshControl* refreshControl = [[UIRefreshControl alloc] init];
  [refreshControl addTarget:self
                     action:@selector(loadPage)
           forControlEvents:UIControlEventValueChanged];
  [self setRefreshControl:refreshControl];
  self.title = @"";

  UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:nil
                                                                    action:nil];
  [self.navigationItem setBackBarButtonItem:backButtonItem];

  [self loadPage];
}

- (void)applyAppearance {
  if (self.site) {
    self.appearance =
        [[TPAppearance sharedInstance] appearanceForSite:self.site pageType:[self sitePageType]];
    self.navigationController.navigationBar.barTintColor = self.appearance.navigationBarBackgroundColor;
    self.navigationController.navigationBar.tintColor = self.appearance.navigationBarTintColor;
    NSMutableDictionary* titleTextAttributes = [self.navigationController.navigationBar.titleTextAttributes mutableCopy];
    if (!titleTextAttributes) titleTextAttributes = [@{} mutableCopy];
    [titleTextAttributes setObject:self.appearance.navigationBarTintColor forKey:NSForegroundColorAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = [titleTextAttributes copy];
    self.navigationController.toolbar.barTintColor = self.appearance.navigationBarBackgroundColor;
    self.navigationController.toolbar.tintColor = self.appearance.navigationBarTintColor;
    [[UIApplication sharedApplication] setStatusBarStyle:self.appearance.preferedStatusBarStyle];
    self.tableView.backgroundColor = self.appearance.backgroundColor;
    self.tableView.separatorColor = self.appearance.separatorColor;
  }
}

- (void)viewDidDisappear:(BOOL)animated {
  if ([self isBeingDismissed] || [self isMovingFromParentViewController]) {
    [self doClean];
  }
  [super viewDidDisappear:animated];
}

- (void)doClean {
  self.view = nil;
  self.contentArrayForSection = nil;
}

- (UINib*)progressCellNib {
  return [UINib nibWithNibName:@"TPProgressTableViewCell" bundle:nil];
}

- (UITableViewCell*)dequeueReusableProgressCellForIndexPath:(NSIndexPath*)indexPath {
  TPProgressTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:kProgressCellId
                                                                       forIndexPath:indexPath];
  cell.backgroundColor = self.appearance.backgroundColor;
  cell.activityIndicator.color = self.appearance.activityIndicatorColor;
  return cell;
}

- (void)viewWillAppear:(BOOL)animated {
  [self applyAppearance];
}

- (void)viewDidAppear:(BOOL)animated {
  [self applyAppearance];
  self.navigationItem.backBarButtonItem.title = @"";
  [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

- (void)setUrl:(NSString *)url {
  if (_url != url) {
    _url = url;
    if (self.isViewLoaded) {
      [self loadPage];
    }
  } else {
    if (self.isViewLoaded) {
      [self loadPage];
    }
  }
}

- (void)prepareForReuse {
  self.contentArrayForSection = nil;
  self.title = @"";
  [self.tableView reloadData];
}

- (void)loadPage {
  [self beginLoading];
  self.page = nil;  // Force new page creation
  [self applyAppearance];

  __weak TPSitePageViewController* weakSelf = self;
  __block BOOL isFirstContent = [self newContentReplacesOld];
  TPParsedPageLoadCompletionBlock completionBlock = ^() {
      [weakSelf checkAndProcessContentFromPage:weakSelf.page replace:isFirstContent];
      isFirstContent = NO;
      weakSelf.page = nil;
      [weakSelf pageDidLoad];
  };
  TPParsedPageLoadPartialDataRetrieveBlock partialDataRetrieveBlock = ^(TPParsedPage* page) {
      [weakSelf checkAndProcessContentFromPage:page replace:isFirstContent];
      isFirstContent = NO;
      [weakSelf pageDidRetrievePartialData:page];
  };
  [self.page loadWithCompletionBlock:completionBlock
            partialDataRetrieveBlock:partialDataRetrieveBlock];
}

- (void)pageDidLoad {
}

- (void)pageDidRetrievePartialData:(TPParsedPage*)page {
}

- (void)setContentFromPage:(TPParsedPage*)page {
  [self checkAndProcessContentFromPage:page replace:YES];
}

- (void)addContentFromPage:(TPParsedPage*)page {
  [self checkAndProcessContentFromPage:page replace:NO];
}

- (NSMutableArray*)contentArrayForSection {
  if (!_contentArrayForSection)
    _contentArrayForSection = [@[] mutableCopy];
  while (_contentArrayForSection.count < [self numberOfContentSections]) {
    [_contentArrayForSection addObject:[@[] mutableCopy]];
  }
  return _contentArrayForSection;
}

- (void)checkAndProcessContentFromPage:(TPParsedPage*)page
                               replace:(BOOL)replace {
  [self.refreshControl endRefreshing];
  if (page.status == TPRemoteDataSourceStatusOK ||
      page.status == TPRemoteDataSourceStatusPartial) {
    [self processContentFromPage:page replace:replace];
  } else {
    [self processContentPageError:page];
  }
}

- (void)processContentFromPage:(TPParsedPage*)page
                       replace:(BOOL)replace {
  [self.tableView beginUpdates];
  for (NSInteger section = 0; section != [self numberOfContentSections]; ++section) {
    [self processContentFromPage:page section:section replace:replace];
  }
  [self.tableView endUpdates];
  if (page.status == TPRemoteDataSourceStatusOK) {
    [self endLoading];
  }
}

- (void)processContentPageError:(TPParsedPage*)page {
  [self endLoading];

  NSString* title = nil;
  NSString* message = nil;

  if (page.status == TPRemoteDataSourceStatusLoginRequired) {
    [self.site navigateToLoginPageWithProperties:@{ @"originPage" : page }];
    title = @"Login required";
    message = @"You're transfered to the login page";
  } else if (page.status == TPRemoteDataSourceStatusParseError) {
    [[TPRoutes sharedInstance] openUrl:[NSURL URLWithString:page.url]
                        withProperties:@{ @"openInEmbeddedBrowser" : @YES }
                       preserveHistory:YES];
    title = @"Parse error";
    message = @"You've been transfered to the embedded browser to check what's happened";
  } else if (page.status == TPRemoteDataSourceStatusNotConnectedToInternet) {
    title = @"No connection";
    message = @"It seems like you not connected to Internet";
  } else if (page.status == TPRemoteDataSourceStatusLoadingError) {
    title = @"Loading error";
    message = @"Something goes wrong";
  } else if (page.status == TPRemoteDataSourceStatusNotFound) {
    title = @"Page not found";
    message = @"Ooops. Broken link?";
  } else if (page.status == TPRemoteDataSourceStatusServiceUnavailable) {
    title = @"Service Unavailable";
    message = @"Something goes wrong";
  }

  if (title) {
    [self displayNotificationWithTitle:title message:message];
  }
}

- (void)displayNotificationWithTitle:(NSString*)title
                             message:(NSString*)message {
  OLGhostAlertView* ghastly =
  [[OLGhostAlertView alloc] initWithTitle:title
                                  message:message];
  ghastly.style = OLGhostAlertViewStyleDark;
  ghastly.bottomContentMargin = 40;
  [ghastly show];
}

- (void)processContentFromPage:(TPParsedPage*)page
                       section:(NSInteger)section
                       replace:(BOOL)replace {
  self.title = [self titleFromPage:page];
  NSArray* pageContentArray = [self contentArrayFromPage:page forSection:section];
  NSMutableArray* contentArray = self.contentArrayForSection[section];

  if (replace) {
    NSMutableArray* deletePaths = [@[] mutableCopy];
    for (NSUInteger rowToDelete = 0; rowToDelete != contentArray.count; ++rowToDelete) {
      [deletePaths addObject:[NSIndexPath indexPathForRow:rowToDelete inSection:section]];
    }
    [self.tableView deleteRowsAtIndexPaths:deletePaths
                          withRowAnimation:UITableViewRowAnimationFade];
    [contentArray removeAllObjects];
  }

  NSMutableArray* insertPaths = [@[] mutableCopy];
  NSMutableArray* updatePaths = [@[] mutableCopy];
  NSInteger contentIndex = 0;
  for (id content in pageContentArray) {
    if ([self needInsertContent:content
                      withIndex:contentIndex
                 inContentArray:contentArray
                        section:section]) {
      BOOL replaceCurrentContent = NO;
      NSInteger insertRow = [self rowWhereInsertContent:content
                                              withIndex:contentIndex
                                         inContentArray:contentArray
                                                section:section
                                  replaceCurrentContent:&replaceCurrentContent];
      NSIndexPath* indexPath = [NSIndexPath indexPathForRow:insertRow inSection:section];
      if (replaceCurrentContent && insertRow < contentArray.count) {
        [updatePaths addObject:indexPath];
        [contentArray setObject:content atIndexedSubscript:insertRow];
      } else {
        NSAssert(indexPath.row == contentArray.count, @"");
        [insertPaths addObject:indexPath];
        [contentArray insertObject:content atIndex:insertRow];
      }
    }
    contentIndex++;
  }
  for (NSIndexPath* indexPath in updatePaths) {
    [self updateCellAtIndexPath:indexPath
                    withContent:[self contentAtIndexPath:indexPath]];
  }
  [self.tableView insertRowsAtIndexPaths:insertPaths
                        withRowAnimation:UITableViewRowAnimationFade];
}

- (BOOL)newContentReplacesOld {
  return YES;
}

- (BOOL)needInsertContent:(id)content
                withIndex:(NSInteger)contentIndex
           inContentArray:(NSArray*)contentArray
                  section:(NSInteger)section {
  return YES;
}

- (void)updateCellAtIndexPath:(NSIndexPath*)indexPath
                  withContent:(id)content {
}

- (NSInteger)rowWhereInsertContent:(id)content
                         withIndex:(NSInteger)contentIndex
                    inContentArray:(NSArray*)contentArray
                           section:(NSInteger)section
             replaceCurrentContent:(BOOL*)replaceCurrentContent {
  *replaceCurrentContent = YES;
  return contentIndex;
}

- (void)beginLoading {
  self.isLoading = YES;
}

- (void)endLoading {
  self.isLoading = NO;
}

- (void)setIsLoading:(BOOL)isLoading {
  if (_isLoading != isLoading) {
    _isLoading = isLoading;
    NSInteger lastSection = [self numberOfSectionsInTableView:self.tableView];
    [self.tableView beginUpdates];
    if (isLoading) {
      [self.tableView insertSections:[NSIndexSet indexSetWithIndex:lastSection - 1]
                    withRowAnimation:UITableViewRowAnimationTop];
    } else {
      [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:lastSection]
                    withRowAnimation:UITableViewRowAnimationTop];
    }
    [self.tableView endUpdates];
  }
}

- (NSString*)titleFromPage:(TPParsedPage*)page {
  return @"";
}

- (NSArray*)contentArrayFromPage:(TPParsedPage *)page forSection:(NSInteger)section {
  return nil;
}

- (NSInteger)numberOfContentSections {
  return 0;
}

- (NSArray *)contentArrayForSection:(NSInteger)section {
  return self.contentArrayForSection[section];
}

- (id)contentAtIndexPath:(NSIndexPath *)indexPath {
  return self.contentArrayForSection[indexPath.section][indexPath.row];
}

- (void)setContent:(id)content atIndexPath:(NSIndexPath *)indexPath {
  [self.contentArrayForSection[indexPath.section] insertObject:content
                                                       atIndex:indexPath.row];
}

- (UITableViewCell*)tableView:(UITableView*)tableView
               cellForContent:(id)content
                  atIndexPath:(NSIndexPath*)indexPath {
  return [self dequeueReusableProgressCellForIndexPath:indexPath];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return [self numberOfContentSections] + (self.isLoading ? 1 : 0);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  if (section < [self numberOfContentSections]) {
    if (section < self.contentArrayForSection.count) {
      return [self.contentArrayForSection[section] count];
    }
    return 0;
  }
  return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.section < [self numberOfContentSections]) {
    id content = [self contentAtIndexPath:indexPath];
    return [self  tableView:tableView cellForContent:content atIndexPath:indexPath];
  }
  return [self dequeueReusableProgressCellForIndexPath:indexPath];
}

- (NSString*)sitePageType {
  return kSitePageTypeBlog;
}

@end
