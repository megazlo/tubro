//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPRemoteDataSourceProtocol.h"
#import "TPSitePageViewController.h"

SPEC_BEGIN(TPSitePageViewControllerSpec)

describe(@"TPSitePageViewController", ^{
  __block TPSitePageViewController* controller = nil;

  beforeEach(^{
    controller =
        [[TPSitePageViewController alloc] init];
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[controller shouldNot] beNil];
    });
  });

  context(@"when page was set", ^{
    __block id page = nil;

    beforeEach(^{
      page = [KWMock nullMockForProtocol:@protocol(TPRemoteDataSourceProtocol)];
    });
  });
});

SPEC_END