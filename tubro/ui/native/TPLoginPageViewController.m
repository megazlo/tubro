//  Created by Evgeniy Krasichkov on 14.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPLoginPageViewController_Private.h"

#import "TPLoginWebPageViewController.h"

static NSString* const kLoginWebPageSegue = @"LoginWebPage";

@implementation TPLoginForm

- (NSDictionary*)usernameField {
  return @{
      FXFormFieldTitle : @"username",
      @"textField.autocapitalizationType" : @(UITextAutocapitalizationTypeNone),
      };
}

- (NSDictionary*)passwordField {
  return @{
      FXFormFieldTitle : @"password",
      };
}

- (NSDictionary*)captchaField {
  return @{
      FXFormFieldTitle : @"captcha",
      };
}

- (NSArray *)fields {
  return @[
      @{ FXFormFieldKey : @"username", FXFormFieldHeader : @"Login" },
      @"password",
//      @{ FXFormFieldKey : @"captchaImage", FXFormFieldHeader : @"Captcha" },
//      @"captcha"
      ];
}

@end

@interface TPLoginPageViewController ()

@property (nonatomic, strong) TPParsedPage* loginParsedPage;

@end

@implementation TPLoginPageViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self createForm];

  self.navigationItem.rightBarButtonItem =
      [[UIBarButtonItem alloc] initWithTitle:@"Login"
                                       style:UIBarButtonItemStylePlain
                                      target:self
                                      action:@selector(onDoneClicked:)];
  self.doneButton = self.navigationItem.rightBarButtonItem;
  self.doneButton.enabled = NO;
  [self loadPage];
}

- (void)loadPage {
  __weak TPLoginPageViewController* weakSelf = self;
  TPParsedPageLoadCompletionBlock completionBlock = ^() {
      NSAssert([self tp_isMainThread], @"");
      weakSelf.page = (id<TPLoginPageProtocol>)self.loginParsedPage;
      weakSelf.loginParsedPage = nil;
  };
  self.loginParsedPage = [self.site loginPageWithSource:self.originPage];
  [self.loginParsedPage loadWithCompletionBlock:completionBlock];
}

- (void)createForm {
  self.form = [[TPLoginForm alloc] init];

  self.formController = [[FXFormController alloc] init];
  self.formController.tableView = self.formTableView;
  self.formController.delegate = self;
  self.formController.form = self.form;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  [self.formTableView reloadData];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

- (void)setPage:(TPParsedPage<TPLoginPageProtocol> *)page {
  _page = page;
  self.doneButton.enabled = (page.status == TPRemoteDataSourceStatusOK);
  self.progressView.hidden = YES;
}

- (void)onDoneClicked:(UIButton *)sender {
  self.doneButton.enabled = NO;
  self.progressView.hidden = NO;
  [self.page loginWithUsername:self.form.username
                      password:self.form.password
                       captcha:self.form.captcha
               completionBlock:^(BOOL success) {
      [self loginCompleteWithSuccess:success];
  }];
}

- (void)loginCompleteWithSuccess:(BOOL)success {
  if (success) {
    [self.navigationController popViewControllerAnimated:YES];
    UIViewController* previousController =
        [self.navigationController.viewControllers lastObject];
    if (previousController && [previousController isKindOfClass:[TPSitePageViewController class]]) {
      TPSitePageViewController* pageVC = (TPSitePageViewController*)previousController;
      [pageVC loadPage];
    }
  } else {
    self.doneButton.enabled = YES;
    self.progressView.hidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
    //[self performSegueWithIdentifier:kLoginWebPageSegue sender:nil];
  }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:kLoginWebPageSegue]) {
    TPLoginWebPageViewController* vc =
        (TPLoginWebPageViewController*)[segue destinationViewController];
    vc.url = self.page.url;
  }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 0;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
  return 0;
}

  @end
