//  Created by Evgeniy Krasichkov on 14.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPSitePageViewController.h"

@protocol TPPostProtocol;

@interface TPPostViewController : TPSitePageViewController

@property IBOutlet UIBarButtonItem* interestCommentTypeButton;
@property IBOutlet UIBarButtonItem* nextInterestCommentButton;
@property IBOutlet UIBarButtonItem* prevInterestCommentButton;
@property IBOutlet UIBarButtonItem* interestCommentsNavigationStatus;

@property (nonatomic, strong) id<TPPostProtocol> post;

- (IBAction)onInterestCommentTypeButtonTapped:(id)sender;
- (IBAction)onNextInterestCommentButtonTapped:(id)sender;
- (IBAction)onPrevInterestCommentButtonTapped:(id)sender;
- (IBAction)onGoToLastCommentButtonTapped:(id)sender;

@end
