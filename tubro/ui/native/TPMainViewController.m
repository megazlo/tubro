//  Created by Evgeniy Krasichkov on 09.08.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPMainViewController.h"

#import "TPBlogToPostTransition.h"
#import "TPBlogViewController.h"
#import "TPDebugViewController.h"
#import "TPFadeTransition.h"
#import "TPLoginPageViewController.h"
#import "TPPostViewController.h"
#import "TPRoundTransition.h"
#import "TPRoutes.h"
#import "TPSlideMenuLeftTableViewController.h"
#import "TPSiteProtocol.h"
#import "TPSiteFactory.h"

@interface TPMainViewController () <TPSitePresenterProtocol,
                                    UINavigationControllerDelegate,
                                    UIGestureRecognizerDelegate>

@property (nonatomic, strong) TPDebugViewController* debugViewController;

@end

@implementation TPMainViewController

- (void)viewDidLoad {
  [TPSiteFactory sharedInstance].presenter = self;
  [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
#if 0 && !defined(NDEBUG)
  self.debugViewController =
      [[TPDebugViewController alloc] initWithParentView:self.view];
  [self.view addSubview:self.debugViewController.view];
#endif  // !defined(NDEBUG)
  [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

- (NSString *)segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath {
  TPSlideMenuLeftTableViewController* leftMenu =
      (TPSlideMenuLeftTableViewController*)self.leftMenu;
  return [leftMenu segueIdentifierForIndexPath:indexPath];
}

- (void)configureLeftMenuButton:(UIButton *)button {
  CGRect frame = CGRectMake(0, 0, 25, 13);
  button.frame = frame;
  button.backgroundColor = [UIColor clearColor];
  [button setImage:[UIImage imageNamed:@"simpleMenuButton"] forState:UIControlStateNormal];
}

#pragma mark TPSitePresenterProtocol

- (void)site:(id<TPSiteProtocol>)site
    didNavigateToLoginPageWithProperties:(NSDictionary *)properties {
  [self showViewControllerWithClass:[TPLoginPageViewController class]
                         identifier:@"login"
                             forUrl:nil
                               site:site
                         properties:properties
                    preserveHistory:YES];
}

- (void)site:(id<TPSiteProtocol>)site didNavigateToWebPageWithUrl:(NSString*)url
                                                       properties:(NSDictionary*)properties
                                                  preserveHistory:(BOOL)preserveHistory
                                                    ignoreRouting:(BOOL)ignoreRouting {
  if (ignoreRouting) {
    NSMutableDictionary* mutableProperties =
        properties ? [properties mutableCopy] : [@{} mutableCopy];
    mutableProperties[@"ignoreRouting"] = @YES;
    properties = [mutableProperties copy];
  }

  [self showViewControllerWithClass:[UIViewController class]
                         identifier:@"browser"
                             forUrl:url
                               site:site
                         properties:properties
                    preserveHistory:preserveHistory];
}

- (void)site:(id<TPSiteProtocol>)site didNavigateToBlogWithUrl:(NSString *)url
                                                    properties:(NSDictionary*)properties
                                              preserveHistory:(BOOL)preserveHistory {
  [self showViewControllerWithClass:[TPBlogViewController class]
                         identifier:@"blog"
                             forUrl:url
                               site:site
                         properties:properties
                    preserveHistory:preserveHistory];
}

- (void)site:(id<TPSiteProtocol>)site didNavigateToCommentsWithUrl:(NSString *)url
                                                        properties:(NSDictionary*)properties
                                                   preserveHistory:(BOOL)preserveHistory {
  [self showViewControllerWithClass:[TPPostViewController class]
                         identifier:@"post"
                             forUrl:url
                               site:site
                         properties:properties
                    preserveHistory:YES];
}

- (void)showViewControllerWithClass:(Class)class
                         identifier:(NSString*)identifier
                             forUrl:(NSString*)url
                               site:(id<TPSiteProtocol>)site
                         properties:(NSDictionary*)properties
                    preserveHistory:(BOOL)preserveHistory {
  UINavigationController* navigationController = [self currentActiveNVC];
  navigationController.delegate = self;
  BOOL needPush = NO;
  id controller = nil;
  if (!preserveHistory &&
      navigationController.viewControllers.count == 1 &&
      [navigationController.viewControllers[0] isKindOfClass:class]) {
    controller = navigationController.viewControllers[0];
    [controller prepareForReuse];
  } else {
    NSAssert(preserveHistory, @"");
    UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    controller = [mainStoryboard instantiateViewControllerWithIdentifier:identifier];
    needPush = YES;
  }
  [controller setSite:site];
  [controller setUrl:url];
  for (NSString* propertyKey in [properties allKeys]) {
    [controller setValue:properties[propertyKey] forKey:propertyKey];
  }
  if (needPush) {
    [navigationController pushViewController:controller animated:YES];
  }
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
                         interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController {
  return nil;
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
  if (operation == UINavigationControllerOperationPush) {
    if ([fromVC isKindOfClass:[TPBlogViewController class]] &&
        [toVC isKindOfClass:[TPPostViewController class]]) {
      return [[TPBlogToPostTransition alloc] init];
    }
  } else if (operation == UINavigationControllerOperationPop) {
    if ([fromVC isKindOfClass:[TPPostViewController class]] &&
        [toVC isKindOfClass:[TPBlogViewController class]]) {
      return [[TPRoundTransition alloc] init];
    }
  }
  return [[TPFadeTransition alloc] init];
}

@end
