//  Created by Evgeniy Krasichkov on 09.08.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPSlideMenuLeftTableViewController.h"

#import "TPBlogViewController.h"
#import "TPDomain.h"
#import "TPMainViewController.h"
#import "TPParsedPage.h"
#import "TPBookmarksProtocol.h"
#import "TPRoutes.h"
#import "TPSiteFactory.h"
#import "TPSiteProtocol.h"

@interface TPSlideMenuLeftTableViewController ()

@property (nonatomic, copy) NSArray* sites;
@property (nonatomic, assign) NSInteger currentSiteIndex;
@property (nonatomic, copy) NSArray* siteLinks;
@property (nonatomic, copy) NSArray* bookmarksPages;
@property (nonatomic, copy) NSArray* isBookmarksPageLoading;
@property (nonatomic, copy) NSArray* isBookmarksPageError;

@end

@implementation TPSlideMenuLeftTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

  [self reloadSites];
  self.tableView.allowsMultipleSelection = YES;
}

- (void)reloadSites {
  self.sites = [TPSiteFactory sharedInstance].sites;
  [self loadBookmarksPages];
  [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  if (![self.tableView indexPathsForSelectedRows].count) {
    NSIndexPath* defaultSiteIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView selectRowAtIndexPath:defaultSiteIndexPath
                                animated:NO
                          scrollPosition:UITableViewScrollPositionNone];
    [self selectRowAtIndexPath:defaultSiteIndexPath];
    NSIndexPath* defaultDomainIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    [self.tableView selectRowAtIndexPath:defaultDomainIndexPath
                                animated:NO
                          scrollPosition:UITableViewScrollPositionNone];
    [self selectRowAtIndexPath:defaultDomainIndexPath];
  }
}

- (void)loadBookmarksPages {
  for (NSInteger siteIndex = 0; siteIndex != self.sites.count; ++siteIndex) {
    [self loadBookmarksPagesForSiteWithIndex:siteIndex];
  }
}

- (void)loadBookmarksPagesForSiteWithIndex:(NSInteger)siteIndex {
  NSMutableArray* siteLinks = self.siteLinks ? [self.siteLinks mutableCopy] : [@[] mutableCopy];
  NSMutableArray* bookmarksPages = self.bookmarksPages ? [self.bookmarksPages mutableCopy] : [@[] mutableCopy];
  NSMutableArray* isBookmarksPageLoading =
      self.isBookmarksPageLoading ? [self.isBookmarksPageLoading mutableCopy] : [@[] mutableCopy];
  NSMutableArray* isBookmarksPageError =
      self.isBookmarksPageError ? [self.isBookmarksPageError mutableCopy] : [@[] mutableCopy];
  id<TPSiteProtocol> site = self.sites[siteIndex];
  TPParsedPage<TPBookmarksProtocol>* page = [site bookmarksPage];
  [siteLinks setObject:[site defaultDomains] atIndexedSubscript:siteIndex];
  [bookmarksPages setObject:page atIndexedSubscript:siteIndex];
  [isBookmarksPageLoading setObject:@YES atIndexedSubscript:siteIndex];
  [isBookmarksPageError setObject:@NO atIndexedSubscript:siteIndex];
  __weak TPSlideMenuLeftTableViewController* weakSelf = self;
  [page loadWithCompletionBlock:^{
      [weakSelf bookmarkPageDidLoad:page];
  }];
  self.bookmarksPages = bookmarksPages;
  self.isBookmarksPageLoading = isBookmarksPageLoading;
  self.isBookmarksPageError = isBookmarksPageError;
  self.siteLinks = siteLinks;
  [self reloadData];
}

- (void)reloadData {
  NSArray* indexPaths = [self.tableView indexPathsForSelectedRows];
  [self.tableView reloadData];
  for (NSIndexPath* path in indexPaths) {
    [self.tableView selectRowAtIndexPath:path
                                animated:NO
                          scrollPosition:UITableViewScrollPositionNone];
  }
}

- (void)bookmarkPageDidLoad:(TPParsedPage<TPBookmarksProtocol>*)page {
  NSInteger index = [self.bookmarksPages indexOfObject:page];
  if (index != NSNotFound) {
    NSMutableArray* isBookmarksPageError = [self.isBookmarksPageError mutableCopy];
    NSMutableArray* isBookmarksPageLoading = [self.isBookmarksPageLoading mutableCopy];
    [isBookmarksPageLoading setObject:@NO atIndexedSubscript:index];
    if (page.status == TPRemoteDataSourceStatusOK) {
      [isBookmarksPageError setObject:@NO atIndexedSubscript:index];
      [self setDomains:page.domains atIndex:index];
    } else {
      [isBookmarksPageError setObject:@YES atIndexedSubscript:index];
    }
    self.isBookmarksPageLoading = isBookmarksPageLoading;
    self.isBookmarksPageError = isBookmarksPageError;
    [self reloadData];
  }
}

- (void)setDomains:(NSArray*)domains atIndex:(NSInteger)index {
  NSMutableArray* siteLinks = [self.siteLinks mutableCopy];
  NSArray* links = [self.sites[index] defaultDomains];
  links = [links arrayByAddingObjectsFromArray:domains];
  [siteLinks setObject:links atIndexedSubscript:index];
  self.siteLinks = siteLinks;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)segueIdentifierForIndexPath:(NSIndexPath*)indexPath {
  return @"SiteNavigationController";
}

- (void)selectRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.section == 1) {
    if (indexPath.row < [self.siteLinks[self.currentSiteIndex] count]) {
      TPDomain* domain = self.siteLinks[self.currentSiteIndex][indexPath.row];
      [self.mainVC.currentActiveNVC popToRootViewControllerAnimated:NO];
      [[TPRoutes sharedInstance]
          openUrl:[NSURL URLWithString:domain.url] withProperties:nil preserveHistory:NO];
      [self.mainVC closeLeftMenu];
    } else if ([self.isBookmarksPageError[self.currentSiteIndex] boolValue]) {
      NSArray* selectedRows = [self.tableView indexPathsForSelectedRows];
      for (NSIndexPath* row in selectedRows) {
        if (row.section == indexPath.section) {
          [self deselectUnnecessaryRowsAccordingToJustSelected:row];
          break;
        }
      }
      [self loadBookmarksPagesForSiteWithIndex:self.currentSiteIndex];
      return;
    }
  } else {
    self.currentSiteIndex = indexPath.row;
    [self reloadData];
    NSIndexPath* defaultDomainIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    [self.tableView selectRowAtIndexPath:defaultDomainIndexPath
                                animated:NO
                          scrollPosition:UITableViewScrollPositionNone];
    [self selectRowAtIndexPath:defaultDomainIndexPath];
  }
  [self deselectUnnecessaryRowsAccordingToJustSelected:indexPath];
}

- (void)deselectUnnecessaryRowsAccordingToJustSelected:(NSIndexPath*)indexPath {
  NSArray* selectedRows = [self.tableView indexPathsForSelectedRows];
  for (NSIndexPath* row in selectedRows) {
    if (row.section == indexPath.section && row.row != indexPath.row) {
      [self.tableView deselectRowAtIndexPath:row animated:YES];
    }
  }
  [self.tableView selectRowAtIndexPath:indexPath
                              animated:YES
                        scrollPosition:UITableViewScrollPositionNone];
}

- (BOOL)isCurrentSiteLoginRequired {
  TPParsedPage<TPBookmarksProtocol>* page = self.bookmarksPages[self.currentSiteIndex];
  return page.status == TPRemoteDataSourceStatusLoginRequired;
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 2;
}

- (NSInteger)tableView:(UITableView *)tableView
    numberOfRowsInSection:(NSInteger)section {
  if (section == 1) {
    NSInteger linksCount = [self.siteLinks[self.currentSiteIndex] count];
    if ([self.isBookmarksPageLoading[self.currentSiteIndex] boolValue] ||
        [self.isBookmarksPageError[self.currentSiteIndex] boolValue]) {
      linksCount++;
    }
    return linksCount;
  }
  return self.sites.count > 1 ? self.sites.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.section == 1) {
    if (indexPath.row < [self.siteLinks[self.currentSiteIndex] count]) {
      UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"Domain"
                                                                   forIndexPath:indexPath];
      TPDomain* domain = self.siteLinks[self.currentSiteIndex][indexPath.row];
      cell.textLabel.text = domain.title;
      cell.detailTextLabel.text = domain.url;
      return cell;
    } else if ([self.isBookmarksPageLoading[self.currentSiteIndex] boolValue]) {
      return [self.tableView dequeueReusableCellWithIdentifier:@"LoadingIndicator"
                                                  forIndexPath:indexPath];
    } else {
      UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"ErrorMessage"
                                                                   forIndexPath:indexPath];
      if ([self isCurrentSiteLoginRequired]) {
        cell.textLabel.text = @"Login required";
      } else {
        cell.textLabel.text = @"Loading error";
      }
      return cell;
    }
  }
  UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"Site"
                                                               forIndexPath:indexPath];
  id<TPSiteProtocol> site = self.sites[indexPath.row];
  cell.textLabel.text = site.name;
  return cell;
}

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [self selectRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView
    didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
  [self selectRowAtIndexPath:indexPath];
}

- (NSString *)tableView:(UITableView *)tableView
    titleForHeaderInSection:(NSInteger)section {
  if (section == 1) {
    return @"Domains";
  }
  return self.sites.count > 1 ? @"Sites" : nil;
}

@end
