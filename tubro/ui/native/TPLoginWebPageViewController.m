//  Created by Evgeniy Krasichkov on 22.11.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPLoginWebPageViewController.h"

#import "TPSitePageViewController.h"

@interface TPLoginWebPageViewController ()

@end

@implementation TPLoginWebPageViewController

- (void)viewDidLoad {
  self.ignoreRouting = YES;
  [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
  if ([request.URL isEqual:request.mainDocumentURL]) {
    NSURL* url = request.URL;
    if ([url.scheme isEqual:@"http"] || [url.scheme isEqual:@"https"]) {
      if ([url.absoluteString rangeOfString:self.url].location == NSNotFound) {
        [self.navigationController popViewControllerAnimated:YES];
        UIViewController* previousController =
        [self.navigationController.viewControllers lastObject];
        if (previousController && [previousController isKindOfClass:[TPSitePageViewController class]]) {
          TPSitePageViewController* pageVC = (TPSitePageViewController*)previousController;
          [pageVC loadPage];
        }
        return NO;
      }
    }
  }
  return YES;
}

@end
