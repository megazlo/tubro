//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPSitePageAppearance.h"

#import <ChameleonFramework/Chameleon.h>

#import "TPSiteProtocol.h"

@interface TPSitePageAppearance ()

@property (nonatomic, assign, readwrite) UIStatusBarStyle preferedStatusBarStyle;
@property (nonatomic, copy, readwrite) UIColor* navigationBarBackgroundColor;
@property (nonatomic, copy, readwrite) UIColor* navigationBarTintColor;
@property (nonatomic, copy, readwrite) UIColor* backgroundColor;
@property (nonatomic, copy, readwrite) UIColor* activityIndicatorColor;
@property (nonatomic, copy, readwrite) UIColor* linkColor;
@property (nonatomic, copy, readwrite) UIColor* postAuthorColor;
@property (nonatomic, copy, readwrite) UIColor* separatorColor;

@end

@implementation TPSitePageAppearance

+ (instancetype)sitePageAppearanceForSite:(id<TPSiteProtocol>)site
                                 pageType:(NSString*)pageType {
  NSAssert(site, @"");
  TPSitePageAppearance* appearance = [[TPSitePageAppearance alloc] init];
  UIColor* primaryColor = [site primaryColorForPageType:pageType];
  UIColor* backgroundColor = [UIColor flatWhiteColor];
//  NSArray* colorScheme = [NSArray arrayOfColorsWithColorScheme:ColorSchemeTriadic
//                                                           for:primaryColor
//                                                    flatScheme:YES];

  appearance.navigationBarBackgroundColor = primaryColor;
  appearance.navigationBarTintColor = [UIColor colorWithContrastingBlackOrWhiteColorOn:primaryColor
                                                                                isFlat:YES];
  appearance.preferedStatusBarStyle =
      [ChameleonStatusBar statusBarStyleForColor:appearance.navigationBarBackgroundColor];
  appearance.backgroundColor = backgroundColor;
  appearance.activityIndicatorColor = [UIColor colorWithContrastingBlackOrWhiteColorOn:backgroundColor
                                                                                isFlat:YES];
  appearance.linkColor = [UIColor flatBlueColorDark];
  appearance.postAuthorColor = [UIColor flatRedColorDark];
  appearance.separatorColor = [UIColor flatWhiteColorDark];
  return appearance;
}

@end
