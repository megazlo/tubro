//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@protocol TPSiteProtocol;

@interface TPSitePageAppearance : NSObject

+ (instancetype)sitePageAppearanceForSite:(id<TPSiteProtocol>)site
                                 pageType:(NSString*)pageType;

@property (nonatomic, assign, readonly) UIStatusBarStyle preferedStatusBarStyle;
@property (nonatomic, copy, readonly) UIColor* navigationBarBackgroundColor;
@property (nonatomic, copy, readonly) UIColor* navigationBarTintColor;
@property (nonatomic, copy, readonly) UIColor* backgroundColor;
@property (nonatomic, copy, readonly) UIColor* activityIndicatorColor;
@property (nonatomic, copy, readonly) UIColor* linkColor;
@property (nonatomic, copy, readonly) UIColor* postAuthorColor;
@property (nonatomic, copy, readonly) UIColor* separatorColor;

@end
