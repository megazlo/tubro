//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPAppearance.h"

#import <ChameleonFramework/Chameleon.h>

#import "TPSitePageAppearance.h"

@interface TPAppearance ()

@end

@implementation TPAppearance

+ (instancetype)sharedInstance {
  static TPAppearance* sharedInstance = nil;
  if (!sharedInstance) {
    sharedInstance = [[TPAppearance alloc] init];
  }
  return sharedInstance;
}

- (TPSitePageAppearance*)appearanceForSite:(id<TPSiteProtocol>)site
                                  pageType:(NSString*)pageType {
  return [TPSitePageAppearance sitePageAppearanceForSite:site pageType:pageType];
}

@end
