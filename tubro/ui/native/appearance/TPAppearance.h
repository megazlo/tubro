//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@protocol TPSiteProtocol;
@class TPSitePageAppearance;

@interface TPAppearance : NSObject

+ (instancetype)sharedInstance;

- (TPSitePageAppearance*)appearanceForSite:(id<TPSiteProtocol>)site
                                  pageType:(NSString*)pageType;

@end
