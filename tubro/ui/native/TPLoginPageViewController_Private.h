//  Created by Evgeniy Krasichkov on 14.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPLoginPageViewController.h"

#import "NSObject+Threads.h"
#import "TPLoginPageProtocol.h"
#import "TPSiteProtocol.h"

@interface TPLoginForm : NSObject <FXForm>

@property (nonatomic, copy) NSString* username;
@property (nonatomic, copy) NSString* password;

@property (nonatomic, copy) UIImage* captchaImage;
@property (nonatomic, copy) NSString* captcha;

@end

@interface TPLoginPageViewController () <FXFormControllerDelegate>

@property (nonatomic, strong) TPLoginForm* form;
@property (nonatomic, strong) IBOutlet UITableView* formTableView;
@property (nonatomic, strong) FXFormController* formController;

@property (nonatomic, weak) UIBarButtonItem* doneButton;

- (void)onDoneClicked:(UIButton *)sender;

- (void)loginCompleteWithSuccess:(BOOL)success;

@end
