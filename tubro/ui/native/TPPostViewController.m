//  Created by Evgeniy Krasichkov on 14.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPPostViewController.h"

#import <MWPhotoBrowser/MWPhotoBrowser.h>

#import "NSObject+Threads.h"
#import "TPCommentProtocol.h"
#import "TPCommentTableViewCell.h"
#import "TPImageProtocol.h"
#import "TPPostProtocol.h"
#import "TPRoutes.h"
#import "TPSiteProtocol.h"
#import "TPPostTableViewCell.h"

static CGFloat const kCollapsedPostHeightToContainerWidth = 1.0;
static CGFloat const kCollapsedCommentMinHeight = 40;
static CGFloat const kCollapsedCommentMaxHeightToContainerWidth = 1.0;  // 160;
static CGFloat const kCollapsedContentMaxHeightToContainerHeight = 0.60;

typedef enum TPInterestCommentType : NSInteger {
    TPInterestCommentTypeNew = 0,
    TPInterestCommentTypeAuthor,
    TPInterestCommentTypeMine,
    TPInterestCommentTypeLast = TPInterestCommentTypeMine,
} TPInterestCommentType;

@interface TPPostViewController () <UITableViewDataSource,
                                    UITableViewDelegate,
                                    TPUpdatableObjectDelegate,
                                    TPContentTableViewCellDelegate,
                                    MWPhotoBrowserDelegate>

@property (nonatomic, readonly) NSArray* comments;

@property (nonatomic, assign) BOOL postExpanded;
@property (nonatomic, strong) NSMutableArray* commentsExpanded;

@property (nonatomic, assign) TPInterestCommentType interestCommentType;
@property (nonatomic, copy) NSArray* currentInterestCommentNumberByType;

@property (nonatomic, assign) BOOL interestCommentsDataValid;
@property (nonatomic, assign) NSInteger interestCommentsCount;
@property (nonatomic, copy) NSArray* interestCommentIndexes;
@property (nonatomic, assign) NSInteger currentInterestCommentNumber;
@property (nonatomic, assign) NSInteger desiredInterestCommentNumber;

@property (nonatomic, strong) NSArray* photos;
@property (nonatomic, strong) NSIndexPath* scrollToIndexPath;

@end

@implementation TPPostViewController

@synthesize page = _page;

- (void)dealloc {
}

- (void)viewDidLoad {
  [super viewDidLoad];

  NSMutableArray* currentInterestCommentNumberByType = [@[] mutableCopy];
  for (NSInteger type = 0; type <= TPInterestCommentTypeMine; ++type) {
    [currentInterestCommentNumberByType addObject:@0];
  }
  self.currentInterestCommentNumberByType = currentInterestCommentNumberByType;
  self.desiredInterestCommentNumber = -1;

  [self.tableView registerNib:[self postCellNib]
       forCellReuseIdentifier:@"Post"];
  [self.tableView registerNib:[self commentCellNib]
       forCellReuseIdentifier:@"Comment"];

  if (self.post) {
    [self setContent:self.post
         atIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [self.tableView reloadData];
  }
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
  [self updateInterestCommentsNavigationControls];
  [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
}

- (UINib*)postCellNib {
  return [UINib nibWithNibName:@"TPPostTableViewCell" bundle:nil];
}

- (UINib*)commentCellNib {
  return [UINib nibWithNibName:@"TPCommentTableViewCell" bundle:nil];
}

- (NSArray*)comments {
  return [self contentArrayForSection:1];
}

- (NSMutableArray *)commentsExpanded {
  if (!_commentsExpanded) {
    _commentsExpanded = [@[] mutableCopy];
  }
  return _commentsExpanded;
}

- (BOOL)isCommentExpanded:(NSInteger)index {
  while (self.commentsExpanded.count <= index) {
    [self.commentsExpanded addObject:[NSNumber numberWithBool:NO]];
  }
  return [self.commentsExpanded[index] boolValue];
}

- (void)setCommentAtIndex:(NSInteger)index expanded:(BOOL)expanded {
  if ([self isCommentExpanded:index] != expanded) {
    [self.commentsExpanded replaceObjectAtIndex:index
                                     withObject:[NSNumber numberWithBool:expanded]];
  }
}

- (TPParsedPage*)page {
  if (!_page) {
    _page = [self.site commentsPageWithUrl:self.url];
  }
  return _page;
}

- (void)pageDidLoad {
  [self resetInterestCommentsNavigationControls];
}

- (void)pageDidRetrievePartialData:(TPParsedPage *)page {
  [self resetInterestCommentsNavigationControls];
  [self tryToScrollToDesiredInterestComment];
}

- (void)resetInterestCommentsNavigationControls {
  self.interestCommentsDataValid = NO;
  [self updateInterestCommentsNavigationControls];
  [self tryToScrollToDesiredInterestComment];
}

- (void)tryToScrollToDesiredInterestComment {
  if (self.desiredInterestCommentNumber >= 0) {
    NSInteger prevValue = self.currentInterestCommentNumber;
    self.currentInterestCommentNumber = self.desiredInterestCommentNumber;
    if ([self currentInterestCommentIndexPath]) {
      [self scrollToCurrentInterestComment];
      self.desiredInterestCommentNumber = -1;
    } else {
      self.currentInterestCommentNumber = prevValue;
    }
  }
}

- (void)updateInterestCommentsNavigationControls {
  if (self.isViewLoaded && self.view.window) {
    NSInteger currentComment = self.currentInterestCommentNumber + 1;
    if (!self.interestCommentsCount) {
      currentComment = 0;
    }
    if (self.currentInterestCommentNumber == 0) {
      NSArray* visibleCells = [self indexPathsForVisibleCommentRows];
      if (!self.interestCommentIndexes.count ||
          [[self.interestCommentIndexes firstObject] integerValue] >
          ((NSIndexPath*)[visibleCells lastObject]).row) {
        currentComment = 0;
      }
    }
    self.interestCommentsNavigationStatus.title =
        [NSString stringWithFormat:@"(%d/%d)",
            (int)currentComment,
            (int)self.interestCommentsCount];

    NSArray* visibleCells = [self indexPathsForVisibleCommentRows];
    if (visibleCells.count) {
      if (self.currentInterestCommentNumber != 0 ||
          [[self.interestCommentIndexes firstObject] integerValue] <
          ((NSIndexPath*)[visibleCells firstObject]).row) {
        self.prevInterestCommentButton.enabled = YES;
      } else {
        self.prevInterestCommentButton.enabled = NO;
      }
      if (self.currentInterestCommentNumber != self.interestCommentsCount - 1 ||
          ![self.interestCommentIndexes lastObject] ||
          [[self.interestCommentIndexes lastObject] integerValue] >
          ((NSIndexPath*)[visibleCells lastObject]).row) {
        self.nextInterestCommentButton.enabled = YES;
      } else {
        self.nextInterestCommentButton.enabled = NO;
      }
    } else {
      self.prevInterestCommentButton.enabled = NO;
      self.nextInterestCommentButton.enabled = NO;
    }
    if (!self.interestCommentsCount) {
      self.prevInterestCommentButton.enabled = NO;
      self.nextInterestCommentButton.enabled = NO;
    }
    switch (self.interestCommentType) {
      case TPInterestCommentTypeNew:
        [self.interestCommentTypeButton setTitle:@"New:"];
        break;
      case TPInterestCommentTypeAuthor:
        [self.interestCommentTypeButton setTitle:@"Author:"];
        break;
      case TPInterestCommentTypeMine:
        [self.interestCommentTypeButton setTitle:@"Mine:"];
        break;
    }
  }
}

- (NSInteger)interestCommentsCount {
  if (!self.interestCommentsDataValid)
    [self updateInterestCommentsData];
  return _interestCommentsCount;
}

- (NSArray *)interestCommentIndexes {
  if (!self.interestCommentsDataValid)
    [self updateInterestCommentsData];
  return _interestCommentIndexes;
}

- (void)onInterestCommentTypeButtonTapped:(id)sender {
  NSMutableArray* currentInterestCommentNumberByType =
      [self.currentInterestCommentNumberByType mutableCopy];
  [currentInterestCommentNumberByType setObject:[NSNumber numberWithInteger:self.currentInterestCommentNumber]
                             atIndexedSubscript:self.interestCommentType];
  self.currentInterestCommentNumberByType = currentInterestCommentNumberByType;

  if (self.interestCommentType == TPInterestCommentTypeLast) {
    self.interestCommentType = 0;
  } else {
    self.interestCommentType++;
  }
  self.interestCommentsDataValid = NO;

  self.currentInterestCommentNumber =
      [self.currentInterestCommentNumberByType[self.interestCommentType] integerValue];
  self.desiredInterestCommentNumber = -1;
  [self scrollToCurrentInterestComment];

  [self resetInterestCommentsNavigationControls];
}

- (void)onNextInterestCommentButtonTapped:(id)sender {
  NSInteger previousValue = self.currentInterestCommentNumber;
  if (self.currentInterestCommentNumber == 0) {
    NSArray* visibleCells = [self indexPathsForVisibleCommentRows];
    if (self.interestCommentIndexes.count &&
        [[self.interestCommentIndexes firstObject] integerValue] <=
        ((NSIndexPath*)[visibleCells lastObject]).row) {
      ++self.currentInterestCommentNumber;
    }
  } else if (self.currentInterestCommentNumber < self.interestCommentsCount - 1) {
    ++self.currentInterestCommentNumber;
  }
  if (![self currentInterestCommentIndexPath]) {
    self.currentInterestCommentNumber = previousValue;
    [self displayCommendHasntLoadedNotification];
    self.desiredInterestCommentNumber = self.currentInterestCommentNumber;
  }
  [self scrollToCurrentInterestComment];
}

- (void)onPrevInterestCommentButtonTapped:(id)sender {
  NSInteger previousValue = self.currentInterestCommentNumber;
  if (self.currentInterestCommentNumber == self.interestCommentsCount - 1) {
    NSArray* visibleCells = [self indexPathsForVisibleCommentRows];
    if ([[self.interestCommentIndexes lastObject] integerValue] >=
        ((NSIndexPath*)[visibleCells firstObject]).row) {
      --self.currentInterestCommentNumber;
    }
  } else if (self.currentInterestCommentNumber > 0) {
    --self.currentInterestCommentNumber;
  }
  if (![self currentInterestCommentIndexPath]) {
    self.currentInterestCommentNumber = previousValue;
    [self displayCommendHasntLoadedNotification];
    self.desiredInterestCommentNumber = self.currentInterestCommentNumber;
  }
  [self scrollToCurrentInterestComment];
}

- (IBAction)onGoToLastCommentButtonTapped:(id)sender {
  NSInteger numberOfComments = [[self contentArrayForSection:1] count];
  if (numberOfComments) {
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:numberOfComments - 1
                                                inSection:1];
    [self scrollToCommentWithIndexPath:indexPath];
    [self updateInterestCommentsNavigationControls];
  }
}

- (void)displayCommendHasntLoadedNotification {
  NSString* title = @"This comment hasn't loaded yet";
  NSString* message = @"Please wait for a while";
  [self displayNotificationWithTitle:title message:message];
}

- (void)scrollToCurrentInterestComment {
  [self scrollToCommentWithIndexPath:[self currentInterestCommentIndexPath]];
}

- (void)scrollToCommentWithIndexPath:(NSIndexPath*)indexPath {
  if (indexPath) {
    [self.tableView scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionMiddle
                                  animated:YES];
    self.scrollToIndexPath = indexPath;
    NSArray* visibleCells = [self indexPathsForVisibleCommentRows];
    for (NSIndexPath* visibleIndexPath in visibleCells) {
      if ([visibleIndexPath isEqual:indexPath]) {
        [self scrollViewDidEndScrollingAnimation:self.tableView];
      }
    }
  }
}

- (NSIndexPath*)currentInterestCommentIndexPath {
  if (self.currentInterestCommentNumber < self.interestCommentIndexes.count) {
    NSInteger commentIndex =
        [self.interestCommentIndexes[self.currentInterestCommentNumber] integerValue];
    NSIndexPath* indexPath =
        [NSIndexPath indexPathForRow:commentIndex
                           inSection:1];
    return indexPath;
  }
  return nil;
}

- (void)updateInterestCommentsData {
  NSInteger count = 0;
  NSMutableArray* indexes = [@[] mutableCopy];
  NSInteger index = 0;
  for (id<TPCommentProtocol> comment in self.comments) {
    BOOL isCommentInteresting = NO;
    switch (self.interestCommentType) {
      case TPInterestCommentTypeNew:
        isCommentInteresting = comment.unread;
        break;
      case TPInterestCommentTypeAuthor:
        isCommentInteresting = [comment.userId isEqualToString:self.post.userId];
        break;
      case TPInterestCommentTypeMine:
        isCommentInteresting = [comment.userId isEqualToString:@"mega210"];
        break;
    }
    if (isCommentInteresting) {
      ++count;
      [indexes addObject:[NSNumber numberWithInteger:index]];
    }
    ++index;
  }
  self.interestCommentIndexes = indexes;
  if (self.interestCommentType == TPInterestCommentTypeNew) {
    self.interestCommentsCount = MAX(count, self.post.newCommentsCount);
  } else {
    self.interestCommentsCount = count;
  }
  self.interestCommentsDataValid = YES;
}

- (void)updateCurrentInterestCommentIndex {
  NSArray* visibleCells = [self indexPathsForVisibleCommentRows];
  NSInteger currentInterestCommentIndex = -1;
  if (self.currentInterestCommentNumber < self.interestCommentIndexes.count) {
    currentInterestCommentIndex =
        [self.interestCommentIndexes[self.currentInterestCommentNumber] integerValue];
  }
  for (NSIndexPath* indexPath in visibleCells) {
    if (indexPath.row == currentInterestCommentIndex) {
      return;
    }
  }
  NSInteger minVisibleIndex = ((NSIndexPath*)[visibleCells firstObject]).row;
  NSInteger maxVisibleIndex = ((NSIndexPath*)[visibleCells lastObject]).row;
  if (currentInterestCommentIndex < minVisibleIndex) {
    NSInteger interestCommentNumber = 0;
    for (NSNumber* number in self.interestCommentIndexes) {
      if ([number integerValue] >= minVisibleIndex) {
        if ([number integerValue] > maxVisibleIndex) {
        }
        break;
      }
      ++interestCommentNumber;
    }
    self.currentInterestCommentNumber = interestCommentNumber;
  } else if (currentInterestCommentIndex > maxVisibleIndex) {
    NSInteger interestCommentNumber = 0;
    for (NSNumber* number in self.interestCommentIndexes) {
      if ([number integerValue] > maxVisibleIndex) {
        if (interestCommentNumber)
          --interestCommentNumber;
        break;
      }
      ++interestCommentNumber;
    }
    if ([self.interestCommentIndexes[interestCommentNumber] integerValue] <
        minVisibleIndex) {
    } else {
      self.currentInterestCommentNumber = interestCommentNumber;
    }
  }
  [self updateInterestCommentsNavigationControls];
}

- (NSArray*)indexPathsForVisibleCommentRows {
  NSArray* visibleRows = [self.tableView indexPathsForVisibleRows];
  visibleRows = [visibleRows sortedArrayUsingSelector:@selector(compare:)];
  if (visibleRows.count && ((NSIndexPath*)visibleRows.firstObject).section == 0) {
    NSMutableArray* mutableVisibleRows = [visibleRows mutableCopy];
    [mutableVisibleRows removeObjectAtIndex:0];
    return [mutableVisibleRows copy];
  }
  return visibleRows;
}

- (NSInteger)currentInterestCommentNumber {
  if (self.interestCommentsCount) {
    if (_currentInterestCommentNumber >= self.interestCommentsCount) {
      _currentInterestCommentNumber = self.interestCommentsCount - 1;
    }
  } else {
    _currentInterestCommentNumber = 0;
  }
  return _currentInterestCommentNumber;
}

#pragma mark - TPUpdatableObjectDelegate

- (void)objectDidUpdate:(id<TPUpdatableObjectProtocol>)object {
  UITableViewCell* cell = (UITableViewCell*)object;
  NSAssert(object && [object isKindOfClass:[UITableViewCell class]], @"");
  NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
  if (indexPath) {
    [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationFade];
  }
}

#pragma mark - TPSitePageViewController

- (NSString*)titleFromPage:(TPParsedPage*)page {
  id<TPBlogProtocol> blog = (id<TPBlogProtocol>)page;
  return blog.title;
}

- (NSInteger)numberOfContentSections {
  return 2;
}

- (NSArray*)contentArrayFromPage:(TPParsedPage *)page forSection:(NSInteger)section {
  id<TPCommentsProtocol> comments = (id<TPCommentsProtocol>)page;
  if (section == 0) {
    return @[comments.post];
  }
  return comments.comments;
}

- (BOOL)newContentReplacesOld {
  return NO;
}

- (BOOL)needInsertContent:(id)content
                withIndex:(NSInteger)contentIndex
           inContentArray:(NSArray*)contentArray
                  section:(NSInteger)section {
  if (section == 0) {
    if (content && (!self.post || ![self.post isEqual:content])) {
      self.post = content;
      [self resetInterestCommentsNavigationControls];
      return YES;
    }
    return NO;
  }
  return contentIndex >= contentArray.count;
}

- (void)updateCellAtIndexPath:(NSIndexPath*)indexPath
                  withContent:(id)content {
  TPContentTableViewCell* cell =
      (TPContentTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
  if (cell) {
    cell.content = content;
  }
}

- (NSInteger)rowWhereInsertContent:(id)content
                         withIndex:(NSInteger)contentIndex
                    inContentArray:(NSArray *)contentArray
                           section:(NSInteger)section
             replaceCurrentContent:(BOOL*)replaceCurrentContent {
  if (section == 0) {
    *replaceCurrentContent = YES;
    return 0;
  }
  return contentArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
                cellForContent:(id)content
                   atIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.section == 0) {
    TPPostTableViewCell* cell =
        [tableView dequeueReusableCellWithIdentifier:@"Post"
                                        forIndexPath:indexPath];
    NSAssert([cell isKindOfClass:[TPPostTableViewCell class]], @"");
    cell.appearance = self.appearance;
    cell.delegate = self;
    cell.cellDelegate = self;
    cell.site = self.site;
    cell.height = [self tableView:self.tableView heightForRowAtIndexPath:indexPath];
    cell.expanded = self.postExpanded;
    cell.highlightAuthor = YES;
    cell.content = content;
    return cell;
  }
  TPCommentTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"Comment"
                                                                 forIndexPath:indexPath];
  NSAssert([cell isKindOfClass:[TPCommentTableViewCell class]], @"");
  cell.appearance = self.appearance;
  cell.delegate = self;
  cell.cellDelegate = self;
  cell.site = self.site;
  cell.height = [self tableView:self.tableView heightForRowAtIndexPath:indexPath];
  cell.expanded = [self isCommentExpanded:indexPath.row];
  cell.highlightAuthor = [self.post.userId isEqualToString:[content userId]];
  cell.content = content;
  return cell;
}

- (NSString*)sitePageType {
  return kSitePageTypeComments;
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  NSAssert(tableView, @"");
  CGFloat containerWidth = tableView.bounds.size.width;
  CGFloat containerHeight = tableView.bounds.size.height;
  NSAssert(containerWidth > 0, @"");
  if (indexPath.section == 0) {
    CGFloat postCollapsedHeight =
        MIN(kCollapsedPostHeightToContainerWidth * containerWidth,
            kCollapsedContentMaxHeightToContainerHeight * containerHeight);
    if (self.postExpanded) {
      CGFloat contentHeight =
          [TPPostTableViewCell heightToFitContent:self.post
                                         expanded:self.postExpanded
                                         forWidth:self.tableView.bounds.size.width];
      return MAX(contentHeight, postCollapsedHeight);
    }
    return postCollapsedHeight;
  }
  if (indexPath.row < self.comments.count) {
    id<TPCommentProtocol> comment = self.comments[indexPath.row];
    BOOL expanded = [self isCommentExpanded:indexPath.row];
    CGFloat contentHeight =
        [TPCommentTableViewCell heightToFitContent:comment
                                          expanded:expanded
                                          forWidth:self.tableView.bounds.size.width];
    if (expanded) {
      return MAX(contentHeight, kCollapsedCommentMinHeight);
    }
    CGFloat commentCollapsedHeight =
        MIN(kCollapsedCommentMaxHeightToContainerWidth * containerWidth,
            kCollapsedContentMaxHeightToContainerHeight * containerHeight);
    return MAX(MIN(contentHeight, commentCollapsedHeight),
               kCollapsedCommentMinHeight);
  }
  return 80;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
  NSIndexPath* indexPath = self.scrollToIndexPath;
  if (indexPath) {
    [self updateInterestCommentsNavigationControls];
    self.scrollToIndexPath = nil;
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[TPContentTableViewCell class]]) {
      TPContentTableViewCell* contentCell = (TPContentTableViewCell*)cell;
      [contentCell highlightTemporarily];
    }
    [self updateCurrentInterestCommentIndex];
  }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
  [self updateCurrentInterestCommentIndex];
}

#pragma mark TPContentTableViewCellDelegate

- (void)expandCell:(TPContentTableViewCell *)cell {
  [self resizeCell:cell expand:YES];
}

- (void)collapseCell:(TPContentTableViewCell *)cell {
  [self resizeCell:cell expand:NO];
}

- (void)resizeCell:(TPContentTableViewCell *)cell expand:(BOOL)expand {
  NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
  if (indexPath) {
    if (indexPath.section == 0) {
      self.postExpanded = expand;
    } else {
      if ([self isCommentExpanded:indexPath.row] != expand) {
        [self setCommentAtIndex:indexPath.row expanded:expand];
      }
    }
    [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationFade];
  }
}

- (void)showMediaForCell:(TPContentTableViewCell *)cell {
  NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
  if (indexPath) {
    id<TPImageProtocol> selectedImage = nil;
    if (indexPath.section == 0) {
      if (self.post.images.count) {
        selectedImage = [self.post.images objectAtIndex:0];
      }
    } else {
      id<TPCommentProtocol> comment = self.comments[indexPath.row];
      if (comment.images.count) {
        selectedImage = [comment.images objectAtIndex:0];
      }
    }

    if (selectedImage) {
      NSInteger startImage = 0;
      NSMutableArray* photos = [@[] mutableCopy];
      for (id<TPImageProtocol> image in self.post.images) {
        if (image == selectedImage) {
          startImage = photos.count;
        }
        MWPhoto* mwPhoto =
            [MWPhoto photoWithURL:[NSURL URLWithString:image.url]];
        NSString* caption = self.post.body.string;
        caption = [caption substringToIndex:MIN(64, caption.length)];
        mwPhoto.caption = caption;
        [photos addObject:mwPhoto];
      }

      for (id<TPCommentProtocol> comment in self.comments) {
        for (id<TPImageProtocol> image in comment.images) {
          if (image == selectedImage) {
            startImage = photos.count;
          }
          MWPhoto* mwPhoto =
              [MWPhoto photoWithURL:[NSURL URLWithString:image.url]];
          NSString* caption = comment.body.string;
          caption = [caption substringToIndex:MIN(64, caption.length)];
          mwPhoto.caption = caption;
          [photos addObject:mwPhoto];
        }
      }

      MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];

      browser.displayActionButton = YES;
          // Show action button to allow sharing, copying, etc (defaults to YES)
      browser.displayNavArrows = NO;
          // Whether to display left and right nav arrows on toolbar (defaults to NO)
      browser.displaySelectionButtons = NO;
          // Whether selection buttons are shown on each image (defaults to NO)
      browser.zoomPhotosToFill = YES;
          // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
      browser.alwaysShowControls = NO;
          // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
      browser.enableGrid = YES;
          // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
      browser.startOnGrid = NO;
          // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)

      self.photos = [photos copy];
      [browser setCurrentPhotoIndex:startImage];
      [self.navigationController pushViewController:browser animated:YES];
    }
  }
}

- (void)openUrl:(NSURL *)url forCell:(TPContentTableViewCell *)cell {
  [[TPRoutes sharedInstance] openUrl:url withProperties:nil preserveHistory:YES];
}

#pragma delegate MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser*)photoBrowser {
  return self.photos.count;
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser*)photoBrowser
                photoAtIndex:(NSUInteger)index {
  if (index < self.photos.count)
    return [self.photos objectAtIndex:index];
  return nil;
}

@end
