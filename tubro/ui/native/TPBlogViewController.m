//  Created by Evgeniy Krasichkov on 14.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPBlogViewController.h"

#import <AMSlideMenu/UIViewController+AMSlideMenu.h>

#import "NSObject+Threads.h"
#import "TPBlogProtocol.h"
#import "TPL9mSite.h"
#import "TPPostTableViewCell.h"
#import "TPPostProtocol.h"
#import "TPPostViewController.h"
#import "TPRoutes.h"
#import "TPSiteProtocol.h"
#import "TPSiteFactory.h"
#include "TPSlideMenuLeftTableViewController.h"

NSInteger const kPostCellHeight = 80;

@interface TPBlogViewController () <UITableViewDataSource,
                                    UITableViewDelegate,
                                    TPContentTableViewCellDelegate,
                                    TPUpdatableObjectDelegate>

@property (nonatomic, assign) NSInteger numberOfReloads;

@end

@implementation TPBlogViewController

@synthesize page = _page;

- (void)viewDidLoad {
  [super viewDidLoad];

  UINib* postCell = [UINib nibWithNibName:@"TPPostTableViewCell"
                                   bundle:nil];
  [self.tableView registerNib:postCell forCellReuseIdentifier:@"Post"];
}

- (TPParsedPage*)page {
  if (!_page) {
    _page = [self.site blogPageWithUrl:self.url];
  }
  return _page;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
  [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
  [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows]
                        withRowAnimation:UITableViewRowAnimationNone];
}

- (void)viewWillDisappear:(BOOL)animated {
  self.numberOfReloads = 0;
  [super viewWillDisappear:animated];
}

#pragma mark - TPSitePageViewController

- (void)pageDidLoad {
  [super pageDidLoad];

  if ([self.url rangeOfString:@"kotiki"].location != NSNotFound) {
    self.numberOfReloads++;
    TPSiteFactory* siteFactory = [TPSiteFactory sharedInstance];
    if (self.numberOfReloads > 10 && siteFactory.sites.count < 2) {
      TPL9mSite* site = [[TPL9mSite alloc] initWithDomain:@"leprosorium.ru"];
      siteFactory.sites = [siteFactory.sites arrayByAddingObject:site];
      [siteFactory registerRoutingSchemes];
      [self displayNotificationWithTitle:@"New achievement unlocked" message:@"ПЫЩЬ!"];
      TPSlideMenuLeftTableViewController* leftMenu =
          (TPSlideMenuLeftTableViewController*)self.mainSlideMenu.leftMenu;
      [leftMenu reloadSites];
    }
  }
}

- (NSString*)titleFromPage:(TPParsedPage*)page {
  id<TPBlogProtocol> blog = (id<TPBlogProtocol>)page;
  return blog.title;
}

- (NSInteger)numberOfContentSections {
  return 1;
}

- (id)contentArrayFromPage:(TPParsedPage *)page forSection:(NSInteger)section {
  id<TPBlogProtocol> blog = (id<TPBlogProtocol>)page;
  NSAssert(section == 0, @"");
  return blog.posts;
}

#pragma mark - TPUpdatableObjectDelegate

- (void)objectDidUpdate:(id<TPUpdatableObjectProtocol>)object {
  UITableViewCell* cell = (UITableViewCell*)object;
  NSAssert(object && [object isKindOfClass:[UITableViewCell class]], @"");
  NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
  if (indexPath) {
    [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationFade];
  }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell*)tableView:(UITableView*)tableView
               cellForContent:(id)content
                  atIndexPath:(NSIndexPath*)indexPath {
  NSAssert(indexPath.section == 0, @"");
  TPPostTableViewCell* cell =
      [tableView dequeueReusableCellWithIdentifier:@"Post"
                                      forIndexPath:indexPath];
  NSAssert([cell isKindOfClass:[TPPostTableViewCell class]], @"");
  cell.appearance = self.appearance;
  cell.delegate = self;
  cell.cellDelegate = self;
  cell.previewMode = YES;
  cell.site = self.site;
  cell.height = kPostCellHeight;
  cell.content = content;
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return kPostCellHeight;
}

#pragma mark TPContentTableViewCellDelegate

- (void)expandCell:(TPContentTableViewCell *)cell {
}

- (void)collapseCell:(TPContentTableViewCell *)cell {
}

- (void)resizeCell:(TPContentTableViewCell *)cell expand:(BOOL)expand {
}

- (void)showMediaForCell:(TPContentTableViewCell *)cell {
}

- (void)openUrl:(NSURL *)url forCell:(TPContentTableViewCell *)cell {
  [[TPRoutes sharedInstance] openUrl:url withProperties:nil preserveHistory:YES];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  id<TPPostProtocol> post = [self contentAtIndexPath:indexPath];
  NSAssert(post, @"");
  [[TPRoutes sharedInstance] openUrl:[NSURL URLWithString:post.url]
                      withProperties:@{ @"post" : post }
                     preserveHistory:YES];
}

@end
