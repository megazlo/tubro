//  Created by Evgeniy Krasichkov on 14.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@protocol TPSiteProtocol;

@interface TPWebPageViewController : UIViewController

@property (nonatomic, copy) NSString* url;
@property (nonatomic, strong) id<TPSiteProtocol> site;
@property (nonatomic, assign) BOOL ignoreRouting;

@property IBOutlet UIWebView* webView;

@end
