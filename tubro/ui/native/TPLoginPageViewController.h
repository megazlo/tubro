//  Created by Evgeniy Krasichkov on 14.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <FXForms/FXForms.h>

#import "TPLoginPageProtocol.h"
#import "TPSitePageViewController.h"

@interface TPLoginPageViewController : UIViewController

@property (nonatomic, copy) NSString* url;
@property (nonatomic, strong) id<TPSiteProtocol> site;
@property (nonatomic, strong) TPParsedPage<TPLoginPageProtocol>* page;
@property (nonatomic, weak) TPParsedPage* originPage;

@property (nonatomic, weak) IBOutlet UIView* progressView;

@end
