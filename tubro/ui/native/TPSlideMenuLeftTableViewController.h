//  Created by Evgeniy Krasichkov on 09.08.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "AMSlideMenuLeftTableViewController.h"

@interface TPSlideMenuLeftTableViewController : AMSlideMenuLeftTableViewController

- (void)reloadSites;

- (NSString *)segueIdentifierForIndexPath:(NSIndexPath *)indexPath;

- (void)selectRowAtIndexPath:(NSIndexPath *)indexPath;

@end
