//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPLoginPageViewController_Private.h"

SPEC_BEGIN(TPLoginPageViewControllerSpec)

describe(@"TPLoginPageViewController", ^{
  __block TPLoginPageViewController* controller = nil;

  beforeEach(^{
    controller =
        [[TPLoginPageViewController alloc] init];
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[controller shouldNot] beNil];
    });
  });

  context(@"when @viewDidLoad was called", ^{
    __block id site = nil;

    beforeEach(^{
      site = [KWMock nullMockForProtocol:@protocol(TPSiteProtocol)];
      controller.site = site;
    });

    it(@"should disable done button", ^{
      [controller viewDidLoad];
      [[theValue(controller.doneButton.isEnabled) should] beNo];
    });
    it(@"should show progress view", ^{
      [controller viewDidLoad];
      [[theValue(controller.progressView.hidden) should] beNo];
    });
    it(@"should get login page object from site", ^{
      [[site should] receive:@selector(loginPageWithSource:)];
      [controller viewDidLoad];
    });

    context(@"origin page was set", ^{
      __block TPParsedPage* originPage = nil;

      beforeEach(^{
        originPage = [TPParsedPage nullMock];
        controller.originPage = originPage;
      });
    });

    context(@"when page was set", ^{
      __block id page = nil;
      __block UIView* progressView = nil;

      beforeEach(^{
        page = [KWMock nullMockForProtocol:@protocol(TPRemoteDataSourceProtocol)];
        [controller viewDidLoad];
        progressView = [[UIView alloc] init];
        controller.progressView = progressView;
      });

      context(@"when page status is OK", ^{
        beforeEach(^{
          [page stub:@selector(status)
              andReturn:theValue(TPRemoteDataSourceStatusOK)];
        });

        it(@"should enable done button", ^{
          controller.page = page;
          [[theValue(controller.doneButton.isEnabled) should] beYes];
        });
        it(@"should hide progress view", ^{
          controller.page = page;
          [[theValue(controller.progressView.hidden) should] beYes];
        });
      });
      
      context(@"when page status is not OK", ^{
        beforeEach(^{
          [page stub:@selector(status)
              andReturn:theValue(TPRemoteDataSourceStatusLoadingError)];
        });

        it(@"should disable done button", ^{
          controller.page = page;
          [[theValue(controller.doneButton.isEnabled) should] beNo];
        });
        it(@"should hide progress view", ^{
          controller.page = page;
          [[theValue(controller.progressView.hidden) should] beYes];
        });
      });
      
      context(@"when done button clicked", ^{
        NSString* const kUsername = @"user";
        NSString* const kPassword = @"pass";
        NSString* const kCaptcha = @"captcha";

        __block TPLoginForm* form = nil;

        beforeEach(^{
          [page stub:@selector(status)
              andReturn:theValue(TPRemoteDataSourceStatusOK)];
          controller.page = page;

          form = [TPLoginForm nullMock];
          controller.form = form;

          [form stub:@selector(username) andReturn:kUsername];
          [form stub:@selector(password) andReturn:kPassword];
          [form stub:@selector(captcha) andReturn:kCaptcha];
        });

        it(@"should send login information to server", ^{
          SEL loginSelector =
              @selector(loginWithUsername:password:captcha:completionBlock:);
          [[page should] receive:loginSelector
                   withArguments:kUsername, kPassword, kCaptcha, any()];
          [controller onDoneClicked:nil];
        });
        it(@"should disable done button", ^{
          [controller onDoneClicked:nil];
          [[theValue(controller.doneButton.isEnabled) should] beNo];
        });
        it(@"should show progress view", ^{
          [controller onDoneClicked:nil];
          [[theValue(controller.progressView.hidden) should] beNo];
        });

        context(@"when login succeeded", ^{
          __block UINavigationController* navigationController = nil;

          beforeEach(^{
            navigationController = [UINavigationController nullMock];
            [controller stub:@selector(navigationController)
                   andReturn:navigationController];
          });

          it(@"should pop view controller", ^{
            [[controller.navigationController should] receive:@selector(popViewControllerAnimated:)];
            [controller loginCompleteWithSuccess:YES];
          });
        });

        context(@"when login failed", ^{
          it(@"should enable done button", ^{
            [controller loginCompleteWithSuccess:NO];
            [[theValue(controller.doneButton.isEnabled) should] beYes];
          });
          it(@"should hide progress view", ^{
            [controller loginCompleteWithSuccess:NO];
            [[theValue(controller.progressView.hidden) should] beYes];
          });
        });
      });
    });
  });
});

SPEC_END