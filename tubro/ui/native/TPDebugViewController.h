//  Created by Evgeniy Krasichkov on 09.08.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@interface TPDebugViewController : UIViewController

- (instancetype)initWithParentView:(UIView*)parentView;

@end
