//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPBlogViewController.h"

#import "TPBlogProtocol.h"
#import "TPSiteProtocol.h"

SPEC_BEGIN(TPBlogViewControllerSpec)

describe(@"TPBlogViewController", ^{
  __block TPBlogViewController* controller = nil;
  __block id page = nil;

  beforeEach(^{
    controller =
        [[TPBlogViewController alloc] init];
    page = [KWMock nullMockForProtocol:@protocol(TPRemoteDataSourceProtocol)];
  });

  context(@"when new object was created", ^{
    it(@"should exists", ^{
      [[controller shouldNot] beNil];
    });
  });

  context(@"when view did load", ^{
    __block id site = nil;

    beforeEach(^{
      site = [KWMock nullMockForProtocol:@protocol(TPSiteProtocol)];
      controller.site = site;
    });

  });
});

SPEC_END