//  Created by Evgeniy Krasichkov on 04.01.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPFadeTransition.h"

@implementation TPFadeTransition

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
  return 0.5;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
  UITableViewController* toViewController =
      (UITableViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
  UIView *containerView = [transitionContext containerView];

  [containerView addSubview:toViewController.view]; 
  toViewController.view.alpha = 0;

  NSTimeInterval animationDuration = [self transitionDuration:transitionContext];
  [UIView animateWithDuration:animationDuration animations:^{
    toViewController.view.alpha = 1;
  } completion:^(BOOL finished) {
      [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
  }];
}

@end
