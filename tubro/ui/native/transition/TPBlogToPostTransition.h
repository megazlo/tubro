//  Created by Evgeniy Krasichkov on 04.01.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@interface TPBlogToPostTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end
