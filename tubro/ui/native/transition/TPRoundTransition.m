//  Created by Evgeniy Krasichkov on 04.01.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPRoundTransition.h"

@interface TPRoundTransition ()

@property (nonatomic, strong) UITableViewController* fromViewController;
@property (nonatomic, strong) id<UIViewControllerContextTransitioning> transitionContext;

@end

@implementation TPRoundTransition

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
  return 0.5;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
  self.transitionContext = transitionContext;

  UITableViewController* toViewController =
      (UITableViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
  UITableViewController* fromViewController =
      (UITableViewController*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
  self.fromViewController = fromViewController;
  UIView *containerView = [transitionContext containerView];

  [containerView insertSubview:toViewController.view belowSubview:fromViewController.view];

  CGFloat initRoundRadius =
      sqrt(pow(CGRectGetWidth(fromViewController.view.frame) * 0.5, 2) +
           pow(CGRectGetHeight(fromViewController.view.frame) * 0.5, 2));
  CGRect initRect =
      CGRectOffset(CGRectInset(CGRectZero, -initRoundRadius, -initRoundRadius),
                   CGRectGetMidX(fromViewController.view.frame),
                   CGRectGetMidY(fromViewController.view.frame));
  UIBezierPath* circleMaskPathInit = [UIBezierPath bezierPathWithOvalInRect:initRect];
  CGRect finalRect = CGRectMake(10, -50, 1, 1);
  UIBezierPath* circleMaskPathFinal = [UIBezierPath bezierPathWithOvalInRect:finalRect];

  CAShapeLayer* maskLayer = [CAShapeLayer layer];
  maskLayer.path = circleMaskPathInit.CGPath;
  fromViewController.view.layer.mask = maskLayer;

  CABasicAnimation* maskAnimation = [CABasicAnimation animation];
  maskAnimation.fillMode = kCAFillModeForwards;
  maskAnimation.removedOnCompletion = NO;
  maskAnimation.fromValue = (__bridge id)(circleMaskPathInit.CGPath);
  maskAnimation.toValue = (__bridge id)(circleMaskPathFinal.CGPath);
  maskAnimation.duration = [self transitionDuration:transitionContext];
  maskAnimation.delegate = self;
  [maskLayer addAnimation:maskAnimation forKey:@"path"];

//  NSTimeInterval animationDuration = [self transitionDuration:transitionContext];
//  [UIView animateWithDuration:animationDuration animations:^{
//  } completion:^(BOOL finished) {
//      [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
//  }];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
  self.fromViewController.view.layer.mask = nil;
  [self.transitionContext completeTransition:![self.transitionContext transitionWasCancelled]];
  [self.fromViewController removeFromParentViewController];
}

@end
