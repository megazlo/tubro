//  Created by Evgeniy Krasichkov on 04.01.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPBlogToPostTransition.h"

@implementation TPBlogToPostTransition

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
  return 0.5;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
  UITableViewController* toViewController =
      (UITableViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
  UITableViewController* fromViewController =
      (UITableViewController*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
  UIView *containerView = [transitionContext containerView];

  UIView* clipView = [[UIView alloc] initWithFrame:CGRectZero];
  clipView.clipsToBounds = YES;
  clipView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
  [containerView addSubview:clipView];
  [clipView addSubview:toViewController.view];

  NSIndexPath* selectedRow = [fromViewController.tableView indexPathForSelectedRow];
  UITableViewCell* sourceCell = [fromViewController.tableView cellForRowAtIndexPath:selectedRow];
  UIView* sourceCellSuperview = sourceCell.superview;
  CGRect sourceFrame = [containerView convertRect:sourceCell.frame fromView:sourceCell.superview];
  [containerView addSubview:sourceCell];
  sourceCell.frame = sourceFrame;

  NSIndexPath* postRow = [NSIndexPath indexPathForRow:0 inSection:0];
  UITableViewCell* destCell = [toViewController.tableView cellForRowAtIndexPath:postRow];
  CGRect destFrame = [containerView convertRect:destCell.frame fromView:destCell.superview];

  clipView.frame = CGRectMake(0, CGRectGetMaxY(sourceFrame),
                              CGRectGetWidth(containerView.frame),
                              0);
  toViewController.view.frame =
      CGRectOffset(containerView.bounds,
                   0, -CGRectGetMaxY(destFrame));

  NSTimeInterval animationDuration = [self transitionDuration:transitionContext];
  [UIView animateWithDuration:animationDuration animations:^{
      sourceCell.frame = destFrame;
      CGRect tableFame = toViewController.tableView.frame;
      UIEdgeInsets tableInsets = toViewController.tableView.contentInset;
      UIEdgeInsetsInsetRect(tableFame, tableInsets);
      clipView.frame = CGRectMake(0, CGRectGetMaxY(destFrame),
                                  CGRectGetWidth(containerView.frame),
                                  CGRectGetHeight(containerView.frame) - CGRectGetMaxY(destFrame));
  } completion:^(BOOL finished) {
      [sourceCellSuperview addSubview:sourceCell];
      [containerView addSubview:toViewController.view];
      toViewController.view.frame = containerView.bounds;
      [clipView removeFromSuperview];
      [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
  }];
}

@end
