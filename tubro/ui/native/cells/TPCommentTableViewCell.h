//  Created by Evgeniy Krasichkov on 06.07.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPContentTableViewCell.H"

@protocol TPCommentProtocol;

@interface TPCommentTableViewCell : TPContentTableViewCell

@end
