//  Created by Evgeniy Krasichkov on 06.07.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <TTTAttributedLabel/TTTAttributedLabel.h>

#import "TPSitePageAppearance.h"
#import "TPSiteProtocol.h"
#import "TPUpdatableObjectProtocol.h"
#import "TPUserGeneratedContentProtocol.h"

@class TPContentTableViewCell;
@class YLImageView;

@protocol TPContentTableViewCellDelegate <NSObject>

- (void)expandCell:(TPContentTableViewCell*)cell;
- (void)collapseCell:(TPContentTableViewCell*)cell;
- (void)showMediaForCell:(TPContentTableViewCell*)cell;
- (void)openUrl:(NSURL*)url forCell:(TPContentTableViewCell*)cell;

@end

@interface TPContentTableViewCell : UITableViewCell
    <TPUpdatableObjectProtocol>

@property (nonatomic, weak) id<TPContentTableViewCellDelegate> cellDelegate;
@property (nonatomic, strong) id<TPUserGeneratedContentProtocol> content;
@property (nonatomic, strong) id<TPSiteProtocol> site;
@property (nonatomic, assign) BOOL expanded;
@property (nonatomic, assign) BOOL previewMode;
@property (nonatomic, assign) BOOL highlightAuthor;
@property (nonatomic, strong) TPSitePageAppearance* appearance;

@property IBOutlet TTTAttributedLabel* body;
@property IBOutlet TTTAttributedLabel* footer;
@property IBOutlet TTTAttributedLabel* rating;
@property IBOutlet YLImageView* background;
@property IBOutlet UIView* dimView;

@property (nonatomic, assign) NSInteger height;

+ (CGFloat)heightToFitContent:(id<TPUserGeneratedContentProtocol>)content
                     expanded:(BOOL)expanded
                     forWidth:(CGFloat)width;

- (void)invalidate;
- (void)setDimColor:(UIColor*)color;
- (void)highlightTemporarily;

- (NSArray*)footerComponents;

@end
