//  Created by Evgeniy Krasichkov on 06.07.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPUpdatableObjectProtocol.h"

@interface TPProgressTableViewCell : UITableViewCell

@property IBOutlet UIActivityIndicatorView* activityIndicator;

@end
