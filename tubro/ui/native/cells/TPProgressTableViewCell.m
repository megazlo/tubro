//  Created by Evgeniy Krasichkov on 06.07.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPProgressTableViewCell.h"

@interface TPProgressTableViewCell ()

@end

@implementation TPProgressTableViewCell

- (void)prepareForReuse {
  [super prepareForReuse];
  [self.activityIndicator startAnimating];
}

@end
