//  Created by Evgeniy Krasichkov on 06.07.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPPostTableViewCell.h"

#import "NSAttributedString+Utils.h"
#import "TPImage.h"
#import "TPPostProtocol.h"

@interface TPPostTableViewCell ()

@end

@implementation TPPostTableViewCell

@dynamic content;

- (NSArray*)footerComponents {
  NSMutableArray* components = [[super footerComponents] mutableCopy];
  if (self.content.blogId) {
    NSString* strippedBlogId = [self.site strippedBlogId:self.content.blogId];
    NSString* blogUrl = [self.site urlToBlog:strippedBlogId];
    NSAttributedString* blog =
        [NSAttributedString tp_attributedStringWithString:strippedBlogId
                                                      url:blogUrl];
    [components insertObject:blog atIndex:1];
  }
  NSString* comments =
      [NSString stringWithFormat:@"%lu/%lu",
          (unsigned long)self.content.commentsCount,
          (unsigned long)self.content.newCommentsCount];
  NSAttributedString* commentsAS =
      [[NSAttributedString alloc] initWithString:comments];
  [components addObject:commentsAS];

  return [components copy];
}

@end
