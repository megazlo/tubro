//  Created by Evgeniy Krasichkov on 06.07.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPContentTableViewCell.h"

#import "NSAttributedString+Utils.h"
#import "ONOXMLElement+Util.h"
#import "TPImage.h"
#import "TPPostProtocol.h"
#import "UIImageView+Util.h"

static const CGFloat kContentHorizontalMargin = 10.0;
static const CGFloat kContentVerticalMargin = 5.0;
static const CGFloat kContentWithImageMinHeight = 80;
static const CGFloat kFooterHeight = 17.0;

@interface TPContentTableViewCell () <TTTAttributedLabelDelegate>

@end

@implementation TPContentTableViewCell

@synthesize delegate = _delegate;

+ (CGFloat)heightToFitContent:(id<TPUserGeneratedContentProtocol>)content
                     expanded:(BOOL)expanded
                     forWidth:(CGFloat)width {
  CGSize maximumLabelSize = CGSizeMake(width -
                                       [self contentIndent:content
                                                  expanded:expanded] -
                                       kContentHorizontalMargin * 2.0,
                                       FLT_MAX);
  CGSize expectedLabelSize =
      [TTTAttributedLabel sizeThatFitsAttributedString:content.body
                                       withConstraints:maximumLabelSize
                                limitedToNumberOfLines:0];
  CGFloat height = expectedLabelSize.height + kContentVerticalMargin * 2.0 + kFooterHeight;
  if (content.images.count)
    height = MAX(height, kContentWithImageMinHeight);
  return height;
}

+ (CGFloat)contentIndent:(id<TPUserGeneratedContentProtocol>)content
                expanded:(BOOL)expanded {
  return 0;
}

- (void)dealloc {
  [self.background tp_setImageWithUrl:nil];
}

- (void)prepareForReuse {
  self.content = nil;
  [self.background tp_setImageWithUrl:nil];
}

- (void)setContent:(id<TPUserGeneratedContentProtocol>)content {
  _content = content;
  [self invalidate];
}

- (void)setDimColor:(UIColor*)color {
  self.dimView.backgroundColor = color;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
  if (self.expanded) {
    [self showMedia];
    //[self collapse];
  } else {
    [self expand];
  }
  [super touchesEnded:touches withEvent:event];
}

- (void)showMedia {
  [self.cellDelegate showMediaForCell:self];
}

- (void)expand {
  if (!self.expanded) {
    CGFloat desiredHeight =
        [[self class] heightToFitContent:self.content
                                expanded:NO
                                forWidth:self.frame.size.width];
    if (self.frame.size.height < desiredHeight) {
      [self.cellDelegate expandCell:self];
    } else {
      [self showMedia];
    }
  }
}

- (void)collapse {
  [self.cellDelegate collapseCell:self];
}

- (void)layoutSubviews {
  CGFloat bodyBottomMargin = kContentVerticalMargin + kFooterHeight;

  self.background.frame = self.bounds;
  self.dimView.frame = self.bounds;

  CGFloat indent = [self contentIndent];
  CGRect bodyFrame =
      CGRectMake(self.bounds.origin.x + kContentHorizontalMargin + indent,
                 self.bounds.origin.y + kContentVerticalMargin,
                 self.bounds.size.width - kContentHorizontalMargin * 2.0 - indent,
                 self.bounds.size.height - kContentVerticalMargin - bodyBottomMargin);
  self.body.delegate = self;
  self.body.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
  self.body.lineBreakMode = NSLineBreakByTruncatingTail;
  self.body.frame = bodyFrame;

  CGRect footerFrame =
      CGRectMake(self.bounds.origin.x + kContentHorizontalMargin + indent,
                 self.bounds.origin.y + self.bounds.size.height - kContentVerticalMargin - kFooterHeight,
                 self.bounds.size.width - kContentHorizontalMargin * 2.0 - indent,
                 kFooterHeight);
  self.footer.delegate = self;
  self.footer.verticalAlignment = TTTAttributedLabelVerticalAlignmentBottom;
  self.footer.frame = footerFrame;

  self.rating.delegate = self;
  self.rating.textAlignment = NSTextAlignmentRight;
  self.rating.verticalAlignment = TTTAttributedLabelVerticalAlignmentBottom;
  self.rating.frame = footerFrame;

  [super layoutSubviews];
}

- (void)invalidate {
  [self invalidateAppearance];
  [self invalidateBody];
  [self invalidateFooter];
  [self invalidateRating];
  [self invalidateImage];
  [self setNeedsLayout];
}

- (void)invalidateAppearance {
  self.dimView.backgroundColor = self.appearance.backgroundColor;
  self.contentView.backgroundColor = self.appearance.backgroundColor;
}

- (void)invalidateBody {
  if (self.previewMode) {
    self.body.text = [self.content.body tp_attributedStringByRemovingAllLinks];
  } else {
    self.body.text = self.content.body;
  }
}

- (void)invalidateFooter {
  NSMutableAttributedString* footer = [[NSMutableAttributedString alloc] init];
  if (self.content) {
    NSArray* footerComponents = [self footerComponents];
    NSUInteger componentNumber = 0;
    for (NSAttributedString* component in footerComponents) {
      [footer appendAttributedString:component];
      if (componentNumber < footerComponents.count - 1) {
        [footer appendAttributedString:[[NSAttributedString alloc] initWithString:@" | "]];
      }
      ++componentNumber;
    }
  }
  NSDictionary* attributes = @{
      NSFontAttributeName :
          [UIFont fontWithName:@"HelveticaNeue" size:12.0],
  };
  [footer addAttributes:attributes range:NSMakeRange(0, footer.length)];
  NSMutableDictionary* linkAttributes = [attributes mutableCopy];
  [linkAttributes setObject:@1 forKey:NSUnderlineStyleAttributeName];
  [self.footer setLinkAttributes:[linkAttributes copy]];
  self.footer.text = footer;
}

- (void)invalidateRating {
  NSAttributedString* rating = [NSAttributedString tp_attributedStringWithString:
      [NSString stringWithFormat:@"%@%d",
                                 self.content.rating > 0 ? @"+" : @"",
                                 (int)self.content.rating]];
  self.rating.text = rating;
}

- (void)invalidateImage {
  if ([self.content.images count]) {
    TPImage* image = self.content.images[0];
    NSAssert(image, @"");
    if (image) {
      NSAssert(self.height, @"");
      [self.background tp_setImageWithUrl:image.url
                         placeholderImage:self.background.image];
    }
  }
}

- (NSArray*)footerComponents {
  NSString* userUrl = [self.site urlToUser:self.content.userId];
  NSAttributedString* user =
      [NSAttributedString tp_attributedStringWithString:self.content.userId
                                                    url:userUrl];
  if (self.highlightAuthor) {
    NSMutableAttributedString* userMutable = [user mutableCopy];
    [userMutable addAttribute:NSForegroundColorAttributeName
                        value:self.appearance.postAuthorColor
                        range:NSMakeRange(0, userMutable.length)];
    user = [userMutable copy];
  }
  
  NSString* dateString =
      [NSDateFormatter localizedStringFromDate:self.content.date
                                     dateStyle:NSDateFormatterShortStyle
                                     timeStyle:NSDateFormatterShortStyle];
  NSAttributedString* date =
      [NSAttributedString tp_attributedStringWithString:dateString];
  return @[user, date];
}

- (CGFloat)contentIndent {
  return [[self class] contentIndent:self.content expanded:self.expanded];
}

- (void)highlightTemporarily {
  UIColor* previousColor = self.dimView.backgroundColor;
  self.dimView.backgroundColor = [UIColor colorWithRed:1
                                                 green:1
                                                  blue:64.0/256.0 alpha:1];
  [UIView animateWithDuration:1.0 animations:^{
      self.dimView.backgroundColor = previousColor;
  }];
}

#pragma comment TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL*)url {
  [self.cellDelegate openUrl:url forCell:self];
}

@end
