//  Created by Evgeniy Krasichkov on 06.07.14.
//  Copyright (c) 2014 Evgeniy Krasichkov. All rights reserved.

#import "TPCommentTableViewCell.h"

#import "TPImage.h"
#import "TPCommentProtocol.h"

@interface TPCommentTableViewCell ()

@property (nonatomic, strong) id<TPCommentProtocol> content;

@end

@implementation TPCommentTableViewCell

@dynamic content;

+ (CGFloat)contentIndent:(id<TPUserGeneratedContentProtocol>)content
                expanded:(BOOL)expanded {
  if (expanded)
    return 0;
  id<TPCommentProtocol> comment = (id<TPCommentProtocol>)content;
  return comment.indent * 7.5;
}

- (void)invalidate {
  [super invalidate];
  UIColor* dimColor = (self.content.unread ?
                       [UIColor colorWithRed:1 green:1 blue:224.0/256.0 alpha:1] :
                       self.appearance.backgroundColor);
  [self setDimColor:dimColor];
}

@end
