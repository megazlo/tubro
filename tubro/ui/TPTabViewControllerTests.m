//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPTabViewController_Private.h"

#import "TPContentViewControllerFactory.h"
#import "TPTab.h"
#import "TPTabPresenterDelegate.h"

SPEC_BEGIN(TPTabViewControllerSpec)

describe(@"TPTabViewController", ^{
});

SPEC_END