//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#include "TPContentView.h"
#include "TPTabPresenter.h"

@class TPContentViewControllerFactory;

@interface TPTabViewController : UIViewController<TPTabPresenter, TPContentView>

@property(nonatomic, strong) TPContentViewControllerFactory* factory;

+ (instancetype)controller;

@end
