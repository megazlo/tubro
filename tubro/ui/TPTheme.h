//  Created by Evgeniy Krasichkov on 11.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@interface TPTheme : NSObject

+ (instancetype)sharedInstance;

@property(nonatomic, readonly) UIColor* backgroundColor;
@property(nonatomic, readonly) UIColor* omniboxQueryColor;
@property(nonatomic, readonly) UIColor* omniboxTitleColor;

@end
