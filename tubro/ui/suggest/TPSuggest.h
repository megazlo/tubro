//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#include "TPSuggestPresenterDelegate.h"
#include "TPUserQueryReceiver.h"

@protocol TPSuggestDelegate;
@protocol TPSuggestPresenter;

@interface TPSuggest : NSObject<TPSuggestPresenterDelegate, TPUserQueryReceiver>

@property(nonatomic, weak) id<TPSuggestDelegate> delegate;
@property(nonatomic, weak) id<TPSuggestPresenter> presenter;

@end
