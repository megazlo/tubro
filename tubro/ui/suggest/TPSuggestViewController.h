//  Created by Evgeniy Krasichkov on 05.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#include "TPSuggestPresenter.h"

@protocol TPSuggestPresenterDelegate;

@interface TPSuggestViewController : UIViewController<TPSuggestPresenter>

@property(nonatomic, weak) id<TPSuggestPresenterDelegate> delegate;

+ (instancetype)controller;

@end
