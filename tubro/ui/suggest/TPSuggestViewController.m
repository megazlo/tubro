//  Created by Evgeniy Krasichkov on 05.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPSuggestViewController.h"

@implementation TPSuggestViewController

+ (instancetype)controller {
  TPSuggestViewController* vc =
      [[TPSuggestViewController alloc] initWithNibName:nil bundle:nil];
  return vc;
}

- (void)loadView {
  UIImage* image = [UIImage imageNamed:@"suggest-background"];
  self.view = [[UIView alloc] initWithFrame:CGRectZero];
  self.view.backgroundColor = [UIColor colorWithPatternImage:image];
  self.view.opaque = NO;
}

@end
