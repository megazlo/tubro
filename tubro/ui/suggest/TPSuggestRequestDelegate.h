//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@class TPSuggestRequest;

@protocol TPSuggestRequestDelegate <NSObject>

- (void)request:(TPSuggestRequest*)request
    didReturnSuggests:(NSArray*)suggests;

@end
