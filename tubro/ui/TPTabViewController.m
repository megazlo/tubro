//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPTabViewController_Private.h"
#import "TPTabPresenterDelegate.h"

@implementation TPTabViewController

@synthesize delegate;

+ (instancetype)controller {
  TPTabViewController* vc =
      [[TPTabViewController alloc] initWithNibName:nil bundle:nil];
  vc.factory = [TPContentViewControllerFactory sharedInstance];
  return vc;
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  self.contentVC.frame = self.view.bounds;
}

#pragma mark TPTabPresenter

- (NSString*)title {
  return self.contentVC.title;
}

- (void)stopLoading {
  [self.contentVC stopLoading];
}

- (void)reload {
  [self.contentVC reload];
}

- (void)startLoadingRequest:(NSURLRequest*)request
                 withReason:(TPNavigationReason)reason {
  [self.contentVC yb_removeFromParentViewController];
  self.contentVC = [self.factory contentViewControllerForRequest:request];
  self.contentVC.delegate = self;
  [self yb_addViewControlledBy:self.contentVC
               asSubviewToView:self.view
                     withFrame:self.view.bounds];
  [self.contentVC startLoadingRequest:request withNavigationReason:reason];
}

#pragma mark TPContentPresenterDelegate

- (void)contentPresenter:(id<TPContentPresenter>)contentPresenter
          didUpdateTitle:(NSString*)title {
  [self.delegate tabPresenter:self didUpdateTitle:title];
}

- (BOOL)contentPresenter:(id<TPContentPresenter>)contentPresenter
    shouldStartNavigationWithRequest:(NSURLRequest*)request{
  return [self.delegate tabPresenter:self
              shouldStartNavigationWithRequest:request];
}

- (void)contentPresenter:(id<TPContentPresenter>)contentPresenter
    didCommitNavigationWithRequest:(NSURLRequest*)request {
  [self.delegate tabPresenter:self didCommitNavigationWithRequest:request];
}

- (void)contentPresenterDidFinishNavigation:
    (id<TPContentPresenter>)contentPresenter {
  [self.delegate tabPresenterDidFinishNavigation:self];
}

#pragma mark TPContentView

- (void)setFrame:(CGRect)frame {
  self.view.frame = frame;
}

- (CGRect)frame {
  return self.view.frame;
}

- (void)setContentInset:(UIEdgeInsets)contentInset {
  self.contentVC.contentInset = contentInset;
}

- (UIEdgeInsets)contentInset {
  return self.contentVC.contentInset;
}

@end
