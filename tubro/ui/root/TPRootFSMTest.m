//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPRootFSM.h"

SPEC_BEGIN(TPRootFSMSpec)

describe(@"TPRootFSM", ^{
  __block TPRootFSM* fsm = nil;
  __block id delegate = nil;

  beforeEach(^{
    fsm = [[TPRootFSM alloc] init];
    delegate = [KWMock nullMockForProtocol:@protocol(TPRootFSMDelegate)];
    fsm.delegate = delegate;
  });

  it(@"should notify state change to delegate", ^{
    TPRootState const kState = TPRootStateCarousel;
    [[delegate should] receive:@selector(rootFSM:didChangeStateFrom:to:)
                 withArguments:fsm, theValue(fsm.state), theValue(kState)];
    [fsm setState:kState];
  });
});

SPEC_END