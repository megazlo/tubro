//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPRootViewController.h"

#import <Kiwi/Kiwi.h>

#import "TPComponentsFSMController.h"
#import "TPComponentsLayout.h"
#import "TPTabViewController.h"
#import "TPOmniboxViewController.h"
#import "TPRootFSM.h"
#import "UIViewController+YBContainmentAdditions.h"

SPEC_BEGIN(TPRootViewControllerSpec)

describe(@"TPRootViewController", ^{
  __block TPRootViewController* vc = nil;

  beforeEach(^{
    vc = [[TPRootViewController alloc] initWithNibName:nil bundle:nil];
  });

  context(@"when fsm was set", ^{
    __block TPRootFSM* fsm = nil;

    beforeEach(^{
      fsm = [TPRootFSM nullMock];
    });

    it(@"should set self as delegate", ^{
      [[fsm should] receive:@selector(setDelegate:) withArguments:vc];

      vc.fsm = fsm;

      [[vc.fsm should] equal:fsm];
    });
  });

  context(@"when OmniboxVC was set", ^{
    __block TPOmniboxViewController* omniboxVC = nil;

    beforeEach(^{
      omniboxVC = [TPOmniboxViewController nullMock];
    });

    it(@"should add VC as child VC", ^{
      SEL sel = @selector(yb_addViewControlledBy:asSubviewToView:withFrame:);
      [[vc should] receive:sel withArguments:omniboxVC, vc.view, any()];
      vc.omniboxVC = omniboxVC;
      [[vc.omniboxVC should] equal:omniboxVC];
    });
  });

  describe(@"TPRootFSMState forwarding", ^{
    __block TPRootFSM* rootFSM = nil;
    __block TPComponentsFSMController* componentsFSM = nil;
    __block TPComponentsLayout* componentsLayout = nil;

    beforeEach(^{
      rootFSM = [TPRootFSM nullMock];
      componentsFSM = [TPComponentsFSMController nullMock];
      componentsLayout = [TPComponentsFSMController nullMock];
      vc.componentsFSM = componentsFSM;
      vc.componentsLayout = componentsLayout;
    });

    specify(^{
      TPRootState const kFromState = TPRootStateCarousel;
      TPRootState const kToState = TPRootStateWebView;
      [[componentsFSM should] receive:@selector(rootFSM:didChangeStateFrom:to:)
                        withArguments:rootFSM, theValue(kFromState), theValue(kToState)];
      [[componentsLayout should] receive:@selector(rootFSM:didChangeStateFrom:to:)
                           withArguments:rootFSM, theValue(kFromState), theValue(kToState)];
      [vc rootFSM:rootFSM didChangeStateFrom:kFromState to:kToState];
    });
  });
});

SPEC_END