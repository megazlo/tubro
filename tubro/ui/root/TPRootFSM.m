//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPRootFSM.h"

@implementation TPRootFSM

- (void)setState:(TPRootState)state {
  if (_state != state) {
    TPRootState oldState = _state;
    _state = state;
    [self.delegate rootFSM:self didChangeStateFrom:oldState to:state];
  }
}

@end
