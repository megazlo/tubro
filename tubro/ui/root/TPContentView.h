//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@protocol TPContentView <NSObject>

@property(nonatomic) CGRect frame;
@property(nonatomic) UIEdgeInsets contentInset;

@end