//  Created by Evgeniy Krasichkov on 10.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPKeyboardAppearanceObserver.h"

@interface NSNotification(TPAnimationDuration)

- (NSTimeInterval)tp_animationDuration;

@end

@implementation NSNotification(TPAnimationDuration)

- (NSTimeInterval)tp_animationDuration {
  NSDictionary* info = [self userInfo];
  NSValue* value = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
  NSTimeInterval duration = 0;
  [value getValue:&duration];
  return duration;
}

@end

@implementation TPKeyboardAppearanceObserver

- (instancetype)init {
  if ((self = [super init])) {
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(keyboardWillShow:)
               name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(keyboardDidShow:)
               name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(keyboardWillChangeFrame:)
               name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(keyboardDidChangeFrame:)
               name:UIKeyboardDidChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(keyboardWillHide:)
               name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(keyboardDidHide:)
               name:UIKeyboardDidHideNotification object:nil];
  }
  return self;
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillShowNotification
              object:nil];
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardDidShowNotification
              object:nil];
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillChangeFrameNotification
              object:nil];
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardDidChangeFrameNotification
              object:nil];
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillHideNotification
              object:nil];
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardDidShowNotification
              object:nil];
}

#pragma mark - keyboard events

- (void)keyboardWillShow:(NSNotification*)notification {
  [self.delegate keyboardAppearanceObserverKeyboardWillShow:self];
}

- (void)keyboardDidShow:(NSNotification*)notification {
  [self.delegate keyboardAppearanceObserverKeyboardDidShow:self];
}

- (void)keyboardWillChangeFrame:(NSNotification*)notification {
  CGRect keyboardFrame =
      [[[notification userInfo]
            objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
  [self.delegate keyboardAppearanceObserver:self
                    keyboardWillChangeFrame:keyboardFrame
                          animationDuration:[notification tp_animationDuration]];
}

- (void)keyboardDidChangeFrame:(NSNotification*)notification {
  [self.delegate keyboardAppearanceObserverKeyboardDidChangeFrame:self];
}

-(void)keyboardWillHide:(NSNotification*)notification {
  [self.delegate keyboardAppearanceObserverKeyboardWillHide:self];
}

- (void)keyboardDidHide:(NSNotification*)notification {
  [self.delegate keyboardAppearanceObserverKeyboardDidHide:self];
}

@end
