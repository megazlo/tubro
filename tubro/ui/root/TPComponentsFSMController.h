//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import "TPOmniboxFSM.h"
#import "TPRootFSM.h"

@interface TPComponentsFSMController : NSObject<TPRootFSMDelegate>

@property (nonatomic, weak) TPOmniboxFSM* omniboxFSM;

@end
