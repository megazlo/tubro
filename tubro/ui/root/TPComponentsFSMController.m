//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPComponentsFSMController.h"

@implementation TPComponentsFSMController

#pragma mark TPRootFSMDelegate

- (void)rootFSM:(TPRootFSM*)rootFSM
    didChangeStateFrom:(TPRootState)fromState
                    to:(TPRootState)toState {
  switch (toState) {
    case TPRootStateNewBorn:
      break;
    case TPRootStateWebView:
      [self.omniboxFSM setState:TPOmniboxStateWebControls];
      break;
    case TPRootStateNativeView:
      break;
    case TPRootStateEditQuery:
      [self.omniboxFSM setState:TPOmniboxStateEditQuery];
      break;
    case TPRootStateCarousel:
      break;
    case TPRootStateImageGallery:
      break;
  }
}

@end
