//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPComponentsLayout.h"

SPEC_BEGIN(TPComponentsLayoutSpec)

describe(@"TPComponentsLayout", ^{
  __block TPComponentsLayout* layout = nil;

  beforeEach(^{
    layout = [[TPComponentsLayout alloc] init];
  });
});

SPEC_END