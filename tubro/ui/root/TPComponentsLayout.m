//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPComponentsLayout.h"

#import "TPContentView.h"
#import "TPCommonMacros.h"

static CGFloat const kTPComponentsLayoutOmniboxHeight = 45.0;

@interface TPComponentsLayout ()

@property(nonatomic, readonly) CGRect contentFrame;

@property(nonatomic, readonly) CGRect statusBarFrame;
@property(nonatomic, readonly) CGRect omniboxFrame;

@end

@implementation TPComponentsLayout

- (CGRect)statusBarFrame {
  CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
  CGRect statusBarWindowRect =
      [self.containerView.window convertRect:statusBarFrame fromWindow: nil];
  CGRect statusBarViewRect =
      [self.containerView convertRect:statusBarWindowRect fromView: nil];
  return statusBarViewRect;
}

- (CGFloat)keyboardBottomInset {
  if (!CGRectGetHeight(self.keyboardFrame)) {
    return 0;
  }
  return MAX(CGRectGetMaxY(self.containerView.bounds) -
             CGRectGetMinY(self.keyboardFrame),
             0);
}

- (CGRect)omniboxFrame {
  CGRect rootBounds = self.containerView.bounds;
  CGRect omniboxFrame =
      CGRectMake(0,
                 rootBounds.size.height - self.keyboardBottomInset -
                     kTPComponentsLayoutOmniboxHeight,
                 rootBounds.size.width, kTPComponentsLayoutOmniboxHeight);
  return omniboxFrame;
}

- (CGRect)contentFrame {
  CGFloat const kStatusBarHeight = self.statusBarFrame.size.height;
  CGRect rootBounds = self.containerView.bounds;
  CGFloat contentHeight =
      rootBounds.size.height -
          kTPComponentsLayoutOmniboxHeight - kStatusBarHeight;
  CGRect contentFrame =
      CGRectMake(0,
                 kStatusBarHeight,
                 rootBounds.size.width,
                 contentHeight);
  return contentFrame;
}

- (void)layoutSubviews {
  self.contentView.frame = self.contentFrame;

  [self layoutOmnibox];
  [self layoutSuggest];
  [self layoutContentInsetsAccordingToKeyboard];
}

- (void)layoutOmnibox {
  self.omniboxView.frame = self.omniboxFrame;
  [self.omniboxView setNeedsLayout];
  [self.omniboxView layoutIfNeeded];
}

- (void)layoutSuggest {
  CGRect suggestViewFrame =
      CGRectMake(0,
                 CGRectGetHeight(self.statusBarFrame),
                 CGRectGetWidth(self.containerView.bounds),
                 CGRectGetHeight(self.containerView.bounds) -
                    CGRectGetHeight(self.omniboxFrame) -
                    CGRectGetHeight(self.statusBarFrame) -
                    [self keyboardBottomInset]);
  self.suggestView.frame = suggestViewFrame;
  if (self.rootState == TPRootStateEditQuery) {
    self.suggestView.alpha = 1.0;
  } else {
    self.suggestView.alpha = 0;
  }
}

- (void)layoutContentInsetsAccordingToKeyboard {
  UIEdgeInsets contentInset = UIEdgeInsetsZero;
  contentInset.bottom = self.keyboardBottomInset;
  self.contentView.contentInset = contentInset;
}

#pragma mark TPRootFSMDelegate

- (void)rootFSM:(TPRootFSM*)rootFSM
    didChangeStateFrom:(TPRootState)fromState
                    to:(TPRootState)toState {
  self.rootState = toState;
}

@end
