//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPRootFSM.h"

@protocol TPContentView;

@interface TPComponentsLayout : NSObject<TPRootFSMDelegate>

@property(nonatomic, assign) TPRootState rootState;
@property (nonatomic, weak) UIView* containerView;
@property (nonatomic, assign) CGRect keyboardFrame;

@property (nonatomic, strong) UIView* omniboxView;
@property (nonatomic, strong) id<TPContentView> contentView;
@property (nonatomic, strong) UIView* suggestView;

- (void)layoutSubviews;

@end
