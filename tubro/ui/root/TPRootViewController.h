//  Created by Evgeniy Krasichkov on 21.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPRootFSM.h"

@class TPComponentsFSMController;
@class TPComponentsKeyboardLayout;
@class TPComponentsLayout;
@class TPKeyboardAppearanceObserver;
@class TPOmnibox;
@class TPOmniboxViewController;
@class TPSuggest;
@class TPSuggestViewController;
@class TPQueryController;
@class TPRootController;
@class TPTabManager;
@class TPTabManagerViewController;

@protocol TPRequestFactory;

@interface TPRootViewController : UIViewController<TPRootFSMDelegate>

@property(nonatomic, strong) TPRootFSM* fsm;
@property(nonatomic, strong) TPRootController* rootController;

@property(nonatomic, strong) TPKeyboardAppearanceObserver*
    keyboardAppearanceObserver;

@property(nonatomic, strong) TPTabManager* tabManager;
@property(nonatomic, strong) TPTabManagerViewController* tabManagerVC;

@property(nonatomic, strong) TPOmnibox* omnibox;
@property(nonatomic, strong) TPOmniboxViewController* omniboxVC;

@property(nonatomic, strong) TPSuggest* suggest;
@property(nonatomic, strong) TPSuggestViewController* suggestVC;

@property(nonatomic, strong) TPQueryController* queryController;
@property(nonatomic, strong) id<TPRequestFactory> requestFactory;

@property(nonatomic, strong) TPComponentsFSMController* componentsFSM;
@property(nonatomic, strong) TPComponentsLayout* componentsLayout;
@property(nonatomic, strong)
    TPComponentsKeyboardLayout* componentsKeyboardLayout;

+ (instancetype)controller;

@end
