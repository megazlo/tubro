//  Created by Evgeniy Krasichkov on 21.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPRootViewController.h"

#import "TPComplexRequestFactory.h"
#import "TPComponentsFSMController.h"
#import "TPComponentsLayout.h"
#import "TPContentViewControllerFactory.h"
#import "TPKeyboardAppearanceObserver.h"
#import "TPOmnibox.h"
#import "TPOmniboxViewController.h"
#import "TPSuggest.h"
#import "TPSuggestViewController.h"
#import "TPQueryController.h"
#import "TPRequestFactory.h"
#import "TPRootController.h"
#import "TPTabManager.h"
#import "TPTabManagerPresenterDelegate.h"
#import "TPTabManagerViewController.h"
#import "TPTab.h"
#import "TPTheme.h"
#import "TPWebRequestFactory.h"
#import "TPYandexRequestFactory.h"
#import "UIViewController+YBContainmentAdditions.h"

@interface TPRootViewController()
    <TPKeyboardAppearanceObserverDelegate, TPTabManagerPresenterDelegate>

@end

@implementation TPRootViewController

+ (instancetype)controller {
  TPRootViewController* vc =
      [[TPRootViewController alloc] initWithNibName:nil bundle:nil];

  TPComplexRequestFactory* requestFactory =
      [[TPComplexRequestFactory alloc] init];
  requestFactory.factories = @[
      [[TPWebRequestFactory alloc] init],
      [[TPYandexRequestFactory alloc] init]];
  vc.requestFactory = requestFactory;

  vc.tabManagerVC = [[TPTabManagerViewController alloc] init];
  vc.tabManagerVC.delegate = vc;
  vc.tabManager = [[TPTabManager alloc] init];
  vc.tabManager.tabManagerPresenter = vc.tabManagerVC;

  vc.queryController = [[TPQueryController alloc] init];
  vc.queryController.requestFactory = vc.requestFactory;
  vc.queryController.requestLoader = vc.tabManager;

  vc.suggest = [[TPSuggest alloc] init];
  vc.suggestVC = [TPSuggestViewController controller];
  vc.suggestVC.delegate = vc.suggest;
  vc.suggest.presenter = vc.suggestVC;

  vc.fsm = [[TPRootFSM alloc] init];
  vc.rootController = [[TPRootController alloc] init];
  vc.rootController.fsm = vc.fsm;
  vc.rootController.suggestQueryReceiver = vc.suggest;
  vc.rootController.navigationQueryReceiver = vc.queryController;
  vc.rootController.navigationController = vc.tabManager;

  vc.keyboardAppearanceObserver = [[TPKeyboardAppearanceObserver alloc] init];
  vc.keyboardAppearanceObserver.delegate = vc;

  vc.omnibox = [[TPOmnibox alloc] init];
  vc.omnibox.delegate = vc.rootController;
  vc.omniboxVC = [TPOmniboxViewController controller];
  vc.omniboxVC.delegate = vc.omnibox;
  vc.omnibox.presenter = vc.omniboxVC;

  vc.tabManager.statusDelegate = vc.omnibox;

  TPComponentsLayout* componentsLayout = [[TPComponentsLayout alloc] init];
  componentsLayout.containerView = vc.view;
  componentsLayout.contentView = vc.tabManagerVC;
  componentsLayout.omniboxView = vc.omniboxVC.view;
  componentsLayout.suggestView = vc.suggestVC.view;
  vc.componentsLayout = componentsLayout;

  vc.componentsFSM = [[TPComponentsFSMController alloc] init];
  vc.componentsFSM.omniboxFSM = vc.omniboxVC.fsm;

  return vc;
}

- (void)setFsm:(TPRootFSM*)fsm {
  if (_fsm != fsm) {
    _fsm = fsm;
    fsm.delegate = self;
  }
}

- (void)setTabManagerVC:(TPTabManagerViewController*)tabManagerVC {
  if (_tabManagerVC != tabManagerVC) {
    _tabManagerVC = tabManagerVC;
    [self yb_addViewControlledBy:tabManagerVC
                 asSubviewToView:self.view
                       withFrame:CGRectZero];
  }
}

- (void)setOmniboxVC:(TPOmniboxViewController*)omniboxVC {
  if (_omniboxVC != omniboxVC) {
    _omniboxVC = omniboxVC;
    [self yb_addViewControlledBy:omniboxVC
                 asSubviewToView:self.view
                       withFrame:CGRectZero];
  }
}

- (void)setSuggestVC:(TPSuggestViewController*)suggestVC {
  if (_suggestVC != suggestVC) {
    _suggestVC = suggestVC;
    [self yb_addViewControlledBy:suggestVC
                 asSubviewToView:self.view
                       withFrame:CGRectZero];
  }
}

- (void)loadView {
  self.view = [[UIView alloc] initWithFrame:CGRectZero];
  self.view.backgroundColor = [[TPTheme sharedInstance] backgroundColor];
  self.view.autoresizingMask =
      UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
}

- (void)viewDidLoad {
}

- (void)viewWillAppear:(BOOL)animated {
  [self.view setNeedsLayout];
}

- (void)viewWillDisappear:(BOOL)animated {
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

- (void)layoutWithAnimation {
  [UIView animateWithDuration:0.25 animations:^{
      [self.view setNeedsLayout];
      [self.view layoutIfNeeded];
  }];
}

#pragma mark layout

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];

  [self.componentsLayout layoutSubviews];
}

#pragma mark TPRootFSMDelegate

- (void)rootFSM:(TPRootFSM*)rootFSM
    didChangeStateFrom:(TPRootState)fromState
                    to:(TPRootState)toState {
  [self.componentsFSM rootFSM:rootFSM
           didChangeStateFrom:fromState
                           to:toState];
  [self.componentsLayout rootFSM:rootFSM
              didChangeStateFrom:fromState
                              to:toState];
  [self layoutWithAnimation];
}

#pragma mark TPKeyboardAppearanceObserverDelegate

- (void)keyboardAppearanceObserverKeyboardWillShow:
    (TPKeyboardAppearanceObserver*)observer {
}

- (void)keyboardAppearanceObserverKeyboardDidShow:
    (TPKeyboardAppearanceObserver*)observer {
}

- (void)keyboardAppearanceObserver:(TPKeyboardAppearanceObserver*)observer
           keyboardWillChangeFrame:(CGRect)frame
                 animationDuration:(NSTimeInterval)animationDuration {
  self.componentsLayout.keyboardFrame = frame;
  [UIView animateWithDuration:animationDuration animations:^{
    [self.componentsLayout layoutSubviews];
  }];
}

- (void)keyboardAppearanceObserverKeyboardDidChangeFrame:
    (TPKeyboardAppearanceObserver*)observer {
}

- (void)keyboardAppearanceObserverKeyboardWillHide:
    (TPKeyboardAppearanceObserver*)observer {
}

- (void)keyboardAppearanceObserverKeyboardDidHide:
    (TPKeyboardAppearanceObserver*)observer {
}

@end
