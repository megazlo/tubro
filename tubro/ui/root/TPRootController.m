//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPRootController.h"

#import "TPRootFSM.h"
#import "TPUserQueryReceiver.h"
#import "TPUserNavigationController.h"

@implementation TPRootController

#pragma mark TPOmniboxDelegate

- (void)omniboxDidRequestEditQuery:(TPOmnibox*)omnibox {
  self.fsm.state = TPRootStateEditQuery;
}

- (void)omniboxDidFinishEditQuery:(TPOmnibox*)omnibox {
  self.fsm.state = TPRootStateWebView;
}

- (void)omnibox:(TPOmnibox*)omnibox didChangeQuery:(NSString*)query {
  [self.suggestQueryReceiver userDidEnterQuery:query];
}

- (void)omnibox:(TPOmnibox*)omnibox didReturnQuery:(NSString*)query {
  [self.navigationQueryReceiver userDidEnterQuery:query];
}

- (void)omniboxDidRequestGoBackward:(TPOmnibox*)omnibox {
  [self.navigationController goBackward];
}

- (void)omniboxDidRequestGoForward:(TPOmnibox*)omnibox {
  [self.navigationController goForward];
}

- (void)omniboxDidRequestCancelEditQuery:(TPOmnibox *)omnibox {
  if (self.fsm.state == TPRootStateEditQuery) {
    self.fsm.state = TPRootStateWebView;
  }
}

- (void)omniboxDidRequestCancelLoading:(TPOmnibox *)omnibox {
  [self.navigationController stopLoading];
}

- (void)omniboxDidRequestReload:(TPOmnibox *)omnibox {
  [self.navigationController reload];
}

@end
