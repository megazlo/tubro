//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@class TPRootFSM;

typedef NS_ENUM(NSInteger, TPRootState) {
  TPRootStateNewBorn = 0,
  TPRootStateWebView,
  TPRootStateNativeView,
  TPRootStateEditQuery,
  TPRootStateCarousel,
  TPRootStateImageGallery,
};

@protocol TPRootFSMDelegate <NSObject>

- (void)rootFSM:(TPRootFSM*)rootFSM
    didChangeStateFrom:(TPRootState)fromState
                    to:(TPRootState)toState;

@end

@interface TPRootFSM : NSObject

@property(nonatomic, weak) id<TPRootFSMDelegate> delegate;

@property(nonatomic, assign) TPRootState state;

@end
