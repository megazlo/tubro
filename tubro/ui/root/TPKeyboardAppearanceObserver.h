//  Created by Evgeniy Krasichkov on 10.05.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@class TPKeyboardAppearanceObserver;

@protocol TPKeyboardAppearanceObserverDelegate <NSObject>

- (void)keyboardAppearanceObserverKeyboardWillShow:
    (TPKeyboardAppearanceObserver*)observer;
- (void)keyboardAppearanceObserverKeyboardDidShow:
    (TPKeyboardAppearanceObserver*)observer;
- (void)keyboardAppearanceObserver:(TPKeyboardAppearanceObserver*)observer
           keyboardWillChangeFrame:(CGRect)frame
                 animationDuration:(NSTimeInterval)animationDuration;
- (void)keyboardAppearanceObserverKeyboardDidChangeFrame:
    (TPKeyboardAppearanceObserver*)observer;
- (void)keyboardAppearanceObserverKeyboardWillHide:
    (TPKeyboardAppearanceObserver*)observer;
- (void)keyboardAppearanceObserverKeyboardDidHide:
    (TPKeyboardAppearanceObserver*)observer;

@end

@interface TPKeyboardAppearanceObserver : NSObject

@property(nonatomic, weak) id<TPKeyboardAppearanceObserverDelegate> delegate;

@end
