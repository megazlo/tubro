//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmnibox.h"

@class TPRootFSM;

@protocol TPUserNavigationController;
@protocol TPUserQueryReceiver;

@interface TPRootController : NSObject<TPOmniboxDelegate>

@property(nonatomic, weak) TPRootFSM* fsm;
@property(nonatomic, weak) id<TPUserQueryReceiver> suggestQueryReceiver;
@property(nonatomic, weak) id<TPUserQueryReceiver> navigationQueryReceiver;
@property(nonatomic, weak) id<TPUserNavigationController> navigationController;

@end
