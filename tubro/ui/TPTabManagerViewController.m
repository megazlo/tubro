//  Created by Evgeniy Krasichkov on 27.06.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPTabManagerViewController.h"

#import "TPContentViewControllerFactory.h"
#import "TPTab.h"
#import "TPTabManagerPresenterDelegate.h"
#import "TPTabPresenterDelegate.h"
#import "TPTabViewController.h"
#import "UIViewController+YBContainmentAdditions.h"

@interface TPTabManagerViewController()

@property(nonatomic, strong) TPTabViewController* activeTab;

@end

@implementation TPTabManagerViewController

@synthesize delegate;

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  self.activeTab.frame = self.view.bounds;
}

#pragma mark TPTabManagerPresenter

- (void)tabManager:(TPTabManager*)tabManager tabDidActivate:(TPTab*)tab {
  self.activeTab = [TPTabViewController controller];
  [self yb_addViewControlledBy:self.activeTab
               asSubviewToView:self.view
                     withFrame:self.view.bounds];
  tab.presenter = self.activeTab;
  self.activeTab.delegate = tab;
}

#pragma mark TPContentView

- (void)setFrame:(CGRect)frame {
  self.view.frame = frame;
}

- (CGRect)frame {
  return self.view.frame;
}

- (void)setContentInset:(UIEdgeInsets)contentInset {
  self.activeTab.contentInset = contentInset;
}

- (UIEdgeInsets)contentInset {
  return self.activeTab.contentInset;
}

@end
