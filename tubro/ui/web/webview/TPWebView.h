//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPNavigationReason.h"

@protocol TPWebViewDelegate;
@protocol TPWebViewImpl;

@interface TPWebView : NSObject

@property(nonatomic, weak) id<TPWebViewDelegate> delegate;

@property(nonatomic, readonly) NSString* title;

@property(nonatomic, readonly) UIView* view;
@property(nonatomic, readonly) UIScrollView* scrollView;
@property(nonatomic, assign, readonly) BOOL loading;
@property(nonatomic, assign, readonly) double loadingProgress;

@property(nonatomic, strong) id<TPWebViewImpl> impl;

+ (instancetype)webView;

- (void)loadRequest:(NSURLRequest*)request
    withNavigationReason:(TPNavigationReason)reason;
- (void)stopLoading;
- (void)reload;

@end
