//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

@class TPWebView;

@protocol TPWebViewDelegate<NSObject>

- (void)webView:(TPWebView*)webView didChangeTitle:(NSString*)title;

- (BOOL)webView:(TPWebView*)webView
    shouldStartNavigationWithRequest:(NSURLRequest*)request;
- (void)webView:(TPWebView*)webView
    didCommitNavigationWithRequest:(NSURLRequest*)request;
- (void)webViewDidFinishNavigation:(TPWebView*)webView;

@end
