// Copyright (c) 2015 Yandex LLC. All rights reserved.
// Author: Roman Ermolov <rermolov@yandex-team.ru>

#import <WebKit/WebKit.h>

@interface WKNavigation (YBAdditions)

@property(nonatomic, strong, readonly) NSURLRequest* yb_request;

@end
