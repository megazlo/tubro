// Copyright (c) 2015 Yandex LLC. All rights reserved.
// Author: Roman Ermolov <rermolov@yandex-team.ru>

#import "WKNavigation+YBAdditions.h"

#import "NSObject+YBIBRO.h"

@implementation WKNavigation (YBAdditions)

/**
 Called to get current request.
 @note Selector: \p _request
 */
static NSString* const YBRequestActionIdentifier =
    @"=Q3clVXclJ3X";

- (NSURLRequest*)yb_request {
  return [self yb_performActionWithIdentifier:YBRequestActionIdentifier];
}

@end
