//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPWKWebViewImpl.h"

#import <WebKit/WebKit.h>

#import "TPCommonMacros.h"
#import "TPKVOBlockObserver.h"
#import "TPWebViewImplDelegate.h"
#import "WKNavigation+YBAdditions.h"

@interface TPWKWebViewImpl()<WKNavigationDelegate>

@property (nonatomic, strong) TPKVOBlockObserver* titleObserver;

@end

@implementation TPWKWebViewImpl

@synthesize delegate;

- (WKWebView*)webView {
  if (!_webView) {
    _webView = [[WKWebView alloc] initWithFrame:CGRectZero];
    _webView.navigationDelegate = self;
    [self startObservingTitleOfWebView:_webView];
  }
  return _webView;
}

- (void)startObservingTitleOfWebView:(WKWebView*)webView {
  __weak __typeof(self) weakSelf = self;
  TPKVOBlockObserverHandler valueChangeHandler = ^(NSString* title) {
    if (title.length == 0) {
      return;
    }
    __typeof(self) strongSelf = weakSelf;
    [strongSelf.delegate webViewImpl:self didChangeTitle:title];
  };
  self.titleObserver =
      [[TPKVOBlockObserver alloc] initWithObject:webView
                                         keyPath:TP_KEY_PATH(webView, title)
                              valueChangeHandler:valueChangeHandler];
}

- (NSString*)title {
  return self.webView.title;
}

- (UIView*)view {
  return self.webView;
}

- (UIScrollView*)scrollView {
  return self.webView.scrollView;
}

- (BOOL)tryGoBackwardWithRequest:(NSURLRequest*)request {
  WKBackForwardList* backForwardList = self.webView.backForwardList;
  WKBackForwardListItem* backItem = backForwardList.backItem;
  if ([backItem.URL isEqual:request.URL]) {
    [self.webView goBack];
    return YES;
  }
  return NO;
}

- (BOOL)tryGoForwardWithRequest:(NSURLRequest*)request {
  WKBackForwardList* backForwardList = self.webView.backForwardList;
  WKBackForwardListItem* forwardItem = backForwardList.forwardItem;
  if ([forwardItem.URL isEqual:request.URL]) {
    [self.webView goForward];
    return YES;
  }
  return NO;
}

- (void)loadRequest:(NSURLRequest*)request
    withNavigationReason:(TPNavigationReason)reason {
  if (reason == TPNavigationReasonGoBackward &&
      [self tryGoBackwardWithRequest:request]) {
    return;
  }
  if (reason == TPNavigationReasonGoForward &&
      [self tryGoForwardWithRequest:request]) {
    return;
  }

  [self.webView loadRequest:request];
}

- (void)stopLoading {
  [self.webView stopLoading];
}

- (void)reload {
  [self.webView reload];
}

#pragma mark WKNavigationDelegate

- (void)webView:(WKWebView *)webView
    decidePolicyForNavigationAction:(WKNavigationAction*)navigationAction
                    decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
  if (!navigationAction.targetFrame) {
    [webView loadRequest:navigationAction.request];
    decisionHandler(WKNavigationActionPolicyCancel);
    [self.webView loadRequest:navigationAction.request];
    return;
  }

  BOOL shouldStart =
      [self.delegate webViewImpl:self
          shouldStartNavigationWithRequest:navigationAction.request];
  WKNavigationActionPolicy decision =
      shouldStart ? WKNavigationActionPolicyAllow :
                    WKNavigationActionPolicyCancel;
  decisionHandler(decision);
}

- (void)webView:(WKWebView *)webView
    decidePolicyForNavigationResponse:(WKNavigationResponse*)navigationResponse
                      decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
  decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)webView:(WKWebView*)webView
    didCommitNavigation:(WKNavigation*)navigation {
  NSURLRequest* request = [navigation yb_request];
  if (request) {
    [self.delegate webViewImpl:self didCommitNavigationWithRequest:request];
  }
}

- (void)webView:(WKWebView*)webView
    didStartProvisionalNavigation:(WKNavigation*)navigation {
  NSURLRequest* request = [navigation yb_request];
  if (request) {
  }
}

- (void)webView:(WKWebView*)webView
    didReceiveServerRedirectForProvisionalNavigation:(WKNavigation*)navigation {
  NSURLRequest* request = [navigation yb_request];
  if (request) {
  }
}

- (void)webView:(WKWebView*)webView
    didFinishNavigation:(WKNavigation*)navigation {
  [self.delegate webViewImplDidFinishNavigation:self];
}

- (void)webView:(WKWebView*)webView
    didFailNavigation:(WKNavigation*)navigation
            withError:(NSError*)error {
}

-(void)webView:(WKWebView*)webView
    didFailProvisionalNavigation:(WKNavigation*)navigation
                       withError:(NSError*)error {
}

@end
