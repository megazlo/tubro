//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPWebViewImpl.h"

@class WKWebView;

@interface TPWKWebViewImpl : NSObject<TPWebViewImpl>

@property (nonatomic, strong) WKWebView* webView;

@end
