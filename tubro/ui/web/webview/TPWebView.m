//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPWebView.h"

#import "TPWebViewDelegate.h"
#import "TPWebViewImplDelegate.h"
#import "TPWebViewImplFactory.h"

@interface TPWebView()<TPWebViewImplDelegate>

@end

@implementation TPWebView

+ (instancetype)webView {
  TPWebView* webView = [[TPWebView alloc] init];
  return webView;
}

- (id<TPWebViewImpl>)impl {
  if (!_impl) {
    _impl = [TPWebViewImplFactory webViewImpl];
    _impl.delegate = self;
  }
  return _impl;
}

- (NSString*)title {
  return self.impl.title;
}

- (UIView*)view {
  return self.impl.view;
}

- (UIScrollView*)scrollView {
  return self.impl.scrollView;
}

- (void)loadRequest:(NSURLRequest*)request
    withNavigationReason:(TPNavigationReason)reason {
  [self.impl loadRequest:request withNavigationReason:reason];
}

- (void)stopLoading {
  [self.impl stopLoading];
}

- (void)reload {
  [self.impl reload];
}

#pragma mark TPWebViewImplDelegate

- (void)webViewImpl:(id<TPWebViewImpl>)webViewImpl
     didChangeTitle:(NSString*)title {
  [self.delegate webView:self didChangeTitle:title];
}

- (BOOL)webViewImpl:(id<TPWebViewImpl>)webViewImpl
    shouldStartNavigationWithRequest:(NSURLRequest*)request {
  return [self.delegate webView:self shouldStartNavigationWithRequest:request];
}

- (void)webViewImpl:(id<TPWebViewImpl>)webViewImpl
    didCommitNavigationWithRequest:(NSURLRequest*)request {
  [self.delegate webView:self didCommitNavigationWithRequest:request];
}

- (void)webViewImplDidFinishNavigation:(id<TPWebViewImpl>)webViewImpl {
  [self.delegate webViewDidFinishNavigation:self];
}

@end
