//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPWebViewImplFactory.h"

#import "TPUIWebViewImpl.h"
#import "TPWKWebViewImpl.h"
#import "UIDevice+YBAdditions.h"

@implementation TPWebViewImplFactory

+ (id<TPWebViewImpl>)webViewImpl {
  if ([UIDevice yb_isOSVersionIsEqualOrHigherThan8]) {
    return [[TPWKWebViewImpl alloc] init];
  }
  return [[TPUIWebViewImpl alloc] init];
}

@end
