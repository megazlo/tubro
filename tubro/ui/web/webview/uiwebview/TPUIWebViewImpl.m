//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPUIWebViewImpl.h"

@interface TPUIWebViewImpl ()

@property (nonatomic, retain) UIWebView* webView;

@end

@implementation TPUIWebViewImpl

@synthesize delegate;

- (UIWebView*)webView {
  if (!_webView) {
    _webView = [[UIWebView alloc] initWithFrame:CGRectZero];
  }
  return _webView;
}

- (UIView*)view {
  return self.webView;
}

- (UIScrollView*)scrollView {
  return self.webView.scrollView;
}

- (NSString*)title {
  return nil;
}

- (void)loadRequest:(NSURLRequest*)request
    withNavigationReason:(TPNavigationReason)reason {
  [self.webView loadRequest:request];
}

- (void)stopLoading {
  [self.webView stopLoading];
}

- (void)reload {
  [self.webView reload];
}

@end
