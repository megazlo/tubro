//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPWebViewImpl.h"

@interface TPWebViewImplFactory : NSObject

+ (id<TPWebViewImpl>)webViewImpl;

@end
