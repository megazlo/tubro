//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@protocol TPWebViewImpl;

@protocol TPWebViewImplDelegate<NSObject>

- (void)webViewImpl:(id<TPWebViewImpl>)webViewImpl
     didChangeTitle:(NSString*)title;

- (BOOL)webViewImpl:(id<TPWebViewImpl>)webViewImpl
    shouldStartNavigationWithRequest:(NSURLRequest*)request;
- (void)webViewImpl:(id<TPWebViewImpl>)webViewImpl
    didCommitNavigationWithRequest:(NSURLRequest*)request;
- (void)webViewImplDidFinishNavigation:(id<TPWebViewImpl>)webViewImpl;

@end
