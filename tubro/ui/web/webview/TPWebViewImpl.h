//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import "TPNavigationReason.h"

@protocol TPWebViewImplDelegate;

@protocol TPWebViewImpl<NSObject>

@property(nonatomic, readonly) NSString* title;

@property(nonatomic, readonly) UIView* view;
@property(nonatomic, readonly) UIScrollView* scrollView;

@property(nonatomic, weak) id<TPWebViewImplDelegate> delegate;

- (void)loadRequest:(NSURLRequest*)request
    withNavigationReason:(TPNavigationReason)reason;
- (void)stopLoading;
- (void)reload;

@end
