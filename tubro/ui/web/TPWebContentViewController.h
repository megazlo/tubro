//  Created by Evgeniy Krasichkov on 17.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPContentPresenter.h"

@class TPWebView;

@interface TPWebContentViewController : UIViewController<TPContentPresenter>

@property(nonatomic, strong) TPWebView* webView;

+ (instancetype)viewController;

@end
