//  Created by Evgeniy Krasichkov on 17.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPWebContentViewController.h"

#import "TPContentPresenterDelegate.h"
#import "TPWebView.h"
#import "TPWebViewDelegate.h"

@interface TPWebContentViewController()<TPWebViewDelegate>

@end

@implementation TPWebContentViewController

@synthesize delegate;

+ (instancetype)viewController {
  TPWebContentViewController* controller =
      [[TPWebContentViewController alloc] init];
  return controller;
}

- (void)loadView {
  self.view = self.webView.view;
}

- (TPWebView*)webView {
  if (!_webView) {
    _webView = [TPWebView webView];
    _webView.delegate = self;
  }
  return _webView;
}

- (void)viewDidLoad {
  [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

#pragma mark TPContentPresenter

- (NSString*)title {
  return self.webView.title;
}

- (void)startLoadingRequest:(NSURLRequest*)request
       withNavigationReason:(TPNavigationReason)reason {
  [self.webView loadRequest:request withNavigationReason:reason];
}

- (void)stopLoading {
  [self.webView stopLoading];
}

- (void)reload {
  [self.webView reload];
}

- (BOOL)isLoading {
  return NO;
}

- (BOOL)isLoaded {
  return NO;
}

#pragma mark TPContentView

- (void)setFrame:(CGRect)frame {
  self.webView.view.frame = frame;
}

- (CGRect)frame {
  return self.webView.view.frame;
}

- (void)setContentInset:(UIEdgeInsets)contentInset {
  self.webView.scrollView.contentInset = contentInset;
  self.webView.scrollView.scrollIndicatorInsets = contentInset;
}

- (UIEdgeInsets)contentInset {
  return self.webView.scrollView.contentInset;
}

#pragma mark TPWebViewDelegate

- (void)webView:(TPWebView*)webView didChangeTitle:(NSString*)title {
  [self.delegate contentPresenter:self didUpdateTitle:title];
}

- (BOOL)webView:(TPWebView*)webView
    shouldStartNavigationWithRequest:(NSURLRequest*)request {
  return [self.delegate contentPresenter:self
        shouldStartNavigationWithRequest:request];
}

- (void)webView:(TPWebView*)webView
    didCommitNavigationWithRequest:(NSURLRequest*)request {
  [self.delegate contentPresenter:self didCommitNavigationWithRequest:request];
}

- (void)webViewDidFinishNavigation:(TPWebView*)webView {
  [self.delegate contentPresenterDidFinishNavigation:self];
}

@end
