//  Created by Evgeniy Krasichkov on 27.06.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPContentView.h"
#import "TPTabManagerPresenter.h"

@interface TPTabManagerViewController
    : UIViewController<TPContentView, TPTabManagerPresenter>

@end
