//  Created by Evgeniy Krasichkov on 23.02.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPTabViewController.h"

#import "TPContentPresenter.h"
#import "TPContentPresenterDelegate.h"
#import "TPContentViewControllerFactory.h"
#import "TPTab.h"
#import "UIViewController+YBContainmentAdditions.h"

@interface TPTabViewController () <TPContentPresenterDelegate>

@property(nonatomic, strong) UIViewController<TPContentPresenter>* contentVC;

@end
