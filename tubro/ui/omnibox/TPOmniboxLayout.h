//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPOmniboxFSM.h"
#import "TPViewWithDesiredBounds.h"

@interface TPOmniboxLayout : NSObject

@property (nonatomic, weak) TPOmniboxFSM* fsm;

@property (nonatomic, weak) UIView* containerView;

@property (nonatomic, strong) UIView* locationView;
@property (nonatomic, strong) UIView<TPViewWithDesiredBounds>* navigationView;
@property (nonatomic, strong) UIView* cancelView;
@property (nonatomic, strong) UIView* loadingControlView;

- (void)invalidate;

@end
