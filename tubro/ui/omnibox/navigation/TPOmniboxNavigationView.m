//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxNavigationView.h"

CGFloat const kTPOmniboxNavigationViewButtonWidth = 30.0;

@implementation TPOmniboxNavigationView

+ (instancetype)view {
  TPOmniboxNavigationView* view = [[self alloc] init];
  return view;
}

- (instancetype)init {
  if ((self = [super init])) {
    _backButtonVisible = YES;
    _forwardButtonVisible = YES;
  }
  return self;
}

- (UIButton*)backButton {
  if (!_backButton) {
    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* image = [UIImage imageNamed:@"omnibox-navigation-button-back"];
    [_backButton setImage:image forState:UIControlStateNormal];
    [_backButton addTarget:self
                    action:@selector(onBackButtonTap)
          forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_backButton];
  }
  return _backButton;
}

- (UIButton*)forwardButton {
  if (!_forwardButton) {
    _forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* image = [UIImage imageNamed:@"omnibox-navigation-button-forward"];
    [_forwardButton setImage:image forState:UIControlStateNormal];
    [_forwardButton addTarget:self
                       action:@selector(onForwardButtonTap)
             forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_forwardButton];
  }
  return _forwardButton;
}

- (IBAction)onBackButtonTap {
  [self.delegate navigationViewBackButtonDidTap:self];
}

- (IBAction)onForwardButtonTap {
  [self.delegate navigationViewForwardButtonDidTap:self];
}

- (void)layoutSubviews {
  [super layoutSubviews];

  CGRect frame = self.bounds;
  self.backButton.frame =
      CGRectMake(frame.origin.x,
                 frame.origin.y,
                 kTPOmniboxNavigationViewButtonWidth,
                 frame.size.height);
  self.backButton.alpha = self.backButtonVisible ? 1.0 : 0;
  self.forwardButton.frame =
      CGRectMake(frame.origin.x + (self.backButtonVisible ?
                    kTPOmniboxNavigationViewButtonWidth :
                    0),
                 frame.origin.y,
                 kTPOmniboxNavigationViewButtonWidth,
                 frame.size.height);
  self.forwardButton.alpha = self.forwardButtonVisible ? 1.0 : 0;
}

- (CGRect)desiredBounds {
  CGFloat width = 0;
  if (self.forwardButtonVisible) width += kTPOmniboxNavigationViewButtonWidth;
  if (self.backButtonVisible) width += kTPOmniboxNavigationViewButtonWidth;
  CGRect bounds = CGRectMake(0, 0, width, 0);
  return bounds;
}

@end
