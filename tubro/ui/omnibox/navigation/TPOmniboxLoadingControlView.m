//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxLoadingControlView.h"

@implementation TPOmniboxLoadingControlView

+ (instancetype)view {
  TPOmniboxLoadingControlView* view = [[self alloc] init];
  return view;
}

- (UIButton*)cancelButton {
  if (!_cancelButton) {
    _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* image = [UIImage imageNamed:@"omnibox-button-cancel"];
    [_cancelButton setImage:image forState:UIControlStateNormal];
    [_cancelButton addTarget:self
                      action:@selector(onCancelButtonTap)
          forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_cancelButton];
  }
  return _cancelButton;
}

- (UIButton*)reloadButton {
  if (!_reloadButton) {
    _reloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* image = [UIImage imageNamed:@"omnibox-navigation-button-reload"];
    [_reloadButton setImage:image forState:UIControlStateNormal];
    [_reloadButton addTarget:self
                      action:@selector(onReloadButtonTap)
          forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_reloadButton];
  }
  return _reloadButton;
}

- (IBAction)onCancelButtonTap {
  [self.delegate loadingControlViewCancelButtonDidTap:self];
}

- (IBAction)onReloadButtonTap {
  [self.delegate loadingControlViewReloadButtonDidTap:self];
}

- (void)layoutSubviews {
  [super layoutSubviews];

  CGRect frame = self.bounds;
  self.cancelButton.frame = frame;
  self.cancelButton.alpha = self.loading ? 1.0 : 0;
  self.reloadButton.frame = frame;
  self.reloadButton.alpha = self.loading ? 0 : 1.0;
}

@end
