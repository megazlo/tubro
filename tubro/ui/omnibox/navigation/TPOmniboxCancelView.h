//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#include <UIKit/UIKit.h>

@class TPOmniboxCancelView;

@protocol TPOmniboxCancelViewDelegate <NSObject>

- (void)cancelViewCancelButtonDidTap:(TPOmniboxCancelView*)cancelView;

@end

@interface TPOmniboxCancelView : UIView

@property(nonatomic, weak) id<TPOmniboxCancelViewDelegate> delegate;

@property(nonatomic, strong) UIButton* cancelButton;

+ (instancetype)view;

@end
