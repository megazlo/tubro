//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPViewWithDesiredBounds.h"

@class TPOmniboxNavigationView;

@protocol TPOmniboxNavigationViewDelegate <NSObject>

- (void)navigationViewBackButtonDidTap:(TPOmniboxNavigationView*)navigationView;
- (void)navigationViewForwardButtonDidTap:
    (TPOmniboxNavigationView*)navigationView;

@end

@interface TPOmniboxNavigationView : UIView<TPViewWithDesiredBounds>

@property(nonatomic, weak) id<TPOmniboxNavigationViewDelegate> delegate;

@property(nonatomic, strong) UIButton* backButton;
@property(nonatomic, strong) UIButton* forwardButton;

@property(nonatomic, assign) BOOL backButtonVisible;
@property(nonatomic, assign) BOOL forwardButtonVisible;

+ (instancetype)view;

@end
