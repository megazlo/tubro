//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxCancelView.h"

@implementation TPOmniboxCancelView

+ (instancetype)view {
  TPOmniboxCancelView* view = [[self alloc] init];
  return view;
}

- (UIButton*)cancelButton {
  if (!_cancelButton) {
    _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* image = [UIImage imageNamed:@"omnibox-button-cancel"];
    [_cancelButton setImage:image forState:UIControlStateNormal];
    [_cancelButton addTarget:self
                      action:@selector(onCancelButtonTap)
          forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_cancelButton];
  }
  return _cancelButton;
}

- (IBAction)onCancelButtonTap {
  [self.delegate cancelViewCancelButtonDidTap:self];
}

- (void)layoutSubviews {
  [super layoutSubviews];

  CGRect frame = self.bounds;
  self.cancelButton.frame = frame;
}

@end
