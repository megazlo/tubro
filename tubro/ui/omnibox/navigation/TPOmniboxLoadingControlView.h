//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#include <UIKit/UIKit.h>

@class TPOmniboxLoadingControlView;

@protocol TPOmniboxLoadingControlViewDelegate <NSObject>

- (void)loadingControlViewCancelButtonDidTap:(TPOmniboxLoadingControlView*)view;
- (void)loadingControlViewReloadButtonDidTap:(TPOmniboxLoadingControlView*)view;

@end

@interface TPOmniboxLoadingControlView : UIView

@property(nonatomic, weak) id<TPOmniboxLoadingControlViewDelegate> delegate;

@property(nonatomic, assign) BOOL loading;

@property(nonatomic, strong) UIButton* cancelButton;
@property(nonatomic, strong) UIButton* reloadButton;

+ (instancetype)view;

@end
