//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxLocationFSM.h"

@implementation TPOmniboxLocationFSM

- (void)setState:(TPOmniboxLocationState)state {
  if (_state != state) {
    TPOmniboxLocationState oldState = _state;
    _state = state;
    [self.delegate omniboxLocationFSM:self
                   didChangeStateFrom:oldState
                                   to:state];
  }
}

@end
