//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxTitleView.h"

#import "TPTheme.h"

#include <TTTAttributedLabel/TTTAttributedLabel.h>

@implementation TPOmniboxTitleView

+ (instancetype)view {
  TPOmniboxTitleView* view = [[self alloc] init];
  return view;
}

- (TTTAttributedLabel*)titleLabel {
  if (!_titleLabel) {
    TTTAttributedLabel* titleLabel =
        [[TTTAttributedLabel alloc] initWithFrame:CGRectZero];
    titleLabel.textColor = [TPTheme sharedInstance].omniboxTitleColor;
    _titleLabel = titleLabel;

    UITapGestureRecognizer* tapRecognizer =
      [[UITapGestureRecognizer alloc] initWithTarget:self 
                                              action:@selector(titleLabelDidTap:)];
    [_titleLabel addGestureRecognizer:tapRecognizer];

    [self addSubview:_titleLabel];
  }
  return _titleLabel;
}

- (void)titleLabelDidTap:(UITapGestureRecognizer*)recognizer {
  [self.delegate omniboxTitleViewDidReceiveTap:self];
}

- (void)layoutSubviews {
  [super layoutSubviews];

  self.titleLabel.frame = CGRectInset(self.bounds, 8, 2);
}

- (void)setTitle:(NSString*)title {
  if (_title != title) {
    _title = [title copy];
    [self.titleLabel setText:title];
  }
}

@end
