//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#include <TTTAttributedLabel/TTTAttributedLabel.h>

@class TPOmniboxEditQueryView;
@class TPOmniboxLocationFSM;
@class TPOmniboxLocationView;
@class TPOmniboxTitleView;

@protocol TPOmniboxLocationViewDelegate <NSObject>

- (void)omniboxLocationViewDidReceiveTitleTap:
    (TPOmniboxLocationView*)locationView;

- (void)omniboxLocationViewDidFinishEditQuery:
    (TPOmniboxLocationView*)locationView;
- (void)omniboxLocationView:(TPOmniboxLocationView*)locationView
             didChangeQuery:(NSString*)query;
- (void)omniboxLocationView:(TPOmniboxLocationView*)locationView
             didReturnQuery:(NSString*)query;

@end

@interface TPOmniboxLocationView : UIView

@property(nonatomic, weak) id<TPOmniboxLocationViewDelegate> delegate;

@property(nonatomic, copy) NSString* title;
@property(nonatomic, copy) NSURLRequest* request;

@property(nonatomic, strong) TPOmniboxLocationFSM* fsm;
@property(nonatomic, strong) TPOmniboxEditQueryView* editQueryView;
@property(nonatomic, strong) TPOmniboxTitleView* titleView;

+ (instancetype)view;

@end
