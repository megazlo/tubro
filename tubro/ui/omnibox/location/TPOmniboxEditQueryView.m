//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxEditQueryView.h"

#import "TPTheme.h"

@interface TPOmniboxEditQueryView () <UITextFieldDelegate>

@property(nonatomic, strong) UIView* backgroundLeft;
@property(nonatomic, strong) UIView* backgroundCenter;
@property(nonatomic, strong) UIView* backgroundRight;

@end

@implementation TPOmniboxEditQueryView

- (instancetype)initWithFrame:(CGRect)frame {
  if ((self = [super initWithFrame:frame])) {
    {
      UIImage* image = [UIImage imageNamed:@"omnibox-query-background-left"];
      UIView* view = [[UIView alloc] initWithFrame:CGRectZero];
      view.backgroundColor = [UIColor colorWithPatternImage:image];
      view.frame = CGRectMake(0, 0, image.size.width, image.size.height);
      [self addSubview:view];
      _backgroundLeft = view;
    }
    {
      UIImage* image = [UIImage imageNamed:@"omnibox-query-background-center"];
      UIView* view = [[UIView alloc] initWithFrame:CGRectZero];
      view.backgroundColor = [UIColor colorWithPatternImage:image];
      view.frame = CGRectMake(0, 0, image.size.width, image.size.height);
      [self addSubview:view];
      _backgroundCenter = view;
    }
    {
      UIImage* image = [UIImage imageNamed:@"omnibox-query-background-right"];
      UIView* view = [[UIView alloc] initWithFrame:CGRectZero];
      view.backgroundColor = [UIColor colorWithPatternImage:image];
      view.frame = CGRectMake(0, 0, image.size.width, image.size.height);
      [self addSubview:view];
      _backgroundRight = view;
    }
    {
      UITextField* textField = [[UITextField alloc] initWithFrame:CGRectZero];
      textField.delegate = self;
      textField.keyboardType = UIKeyboardTypeWebSearch;
      textField.autocorrectionType = UITextAutocorrectionTypeNo;
      textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
      textField.clearButtonMode = UITextFieldViewModeWhileEditing;
      textField.textColor = [TPTheme sharedInstance].omniboxQueryColor;
      [self addSubview:textField];
      _queryTextField = textField;
    }
  }
  return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];

  CGRect frame = self.bounds;
  CGFloat yShift = (frame.size.height -
                       self.backgroundLeft.frame.size.height) * 0.5;
  {
    self.backgroundLeft.frame =
        CGRectMake(frame.origin.x,
                   frame.origin.y + yShift,
                   self.backgroundLeft.frame.size.width,
                   self.backgroundLeft.frame.size.height);
  }
  {
    self.backgroundCenter.frame =
        CGRectMake(CGRectGetMaxX(self.backgroundLeft.frame),
                   frame.origin.y + yShift,
                   frame.size.width - self.backgroundLeft.frame.size.width -
                      self.backgroundRight.frame.size.width,
                   self.backgroundCenter.frame.size.height);
  }
  {
    self.backgroundRight.frame =
        CGRectMake(CGRectGetMaxX(self.backgroundCenter.frame),
                   frame.origin.y + yShift,
                   self.backgroundRight.frame.size.width,
                   self.backgroundRight.frame.size.height);
  }
  self.queryTextField.frame = CGRectInset(self.bounds, 8, 2);
}

- (BOOL)becomeFirstResponder {
  self.queryTextField.text = [self.request.URL absoluteString];
  [self.queryTextField selectAll:self];
  return [self.queryTextField becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
  return [self.queryTextField resignFirstResponder];
}

- (void)returnQuery {
  [self.delegate omniboxEditQueryView:self
                   didReturnWithQuery:self.queryTextField.text];
}

- (void)didFinishEdit {
  [self.delegate omniboxEditQueryViewDidFinishEdit:self];
}

#pragma UITextFieldDelegate

- (BOOL)textField:(UITextField*)textField
    shouldChangeCharactersInRange:(NSRange)range
                replacementString:(NSString *)string {
  NSString* queryString =
      [textField.text stringByReplacingCharactersInRange:range
                                              withString:string];
  [self.delegate omniboxEditQueryView:self didChangeQuery:queryString];
  return YES;
}

- (void)textFieldDidEndEditing:(UITextField*)textField {
  [self didFinishEdit];
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
  [self didFinishEdit];
  [self returnQuery];
  return YES;
}

@end
