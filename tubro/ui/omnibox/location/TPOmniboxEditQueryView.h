//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#include <UIKit/UIKit.h>

@class TPOmniboxEditQueryView;

@protocol TPOmniboxEditQueryViewDelegate <NSObject>

- (void)omniboxEditQueryView:(TPOmniboxEditQueryView*)editQueryView
          didReturnWithQuery:(NSString*)query;
- (void)omniboxEditQueryView:(TPOmniboxEditQueryView*)editQueryView
              didChangeQuery:(NSString*)query;
- (void)omniboxEditQueryViewDidFinishEdit:
    (TPOmniboxEditQueryView*)editQueryView;

@end

@interface TPOmniboxEditQueryView : UIView

@property(nonatomic, weak) id<TPOmniboxEditQueryViewDelegate> delegate;

@property(nonatomic, copy) NSURLRequest* request;

@property(nonatomic, strong) UITextField* queryTextField;

@end
