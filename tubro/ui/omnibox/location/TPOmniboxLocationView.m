//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxLocationView.h"

#import "TPOmniboxEditQueryView.h"
#import "TPOmniboxLocationFSM.h"
#import "TPOmniboxTitleView.h"

@interface TPOmniboxLocationView () <
    TPOmniboxEditQueryViewDelegate,
    TPOmniboxTitleViewDelegate,
    TPOmniboxLocationFSMDelegate>

@end

@implementation TPOmniboxLocationView

+ (instancetype)view {
  TPOmniboxLocationView* view = [[self alloc] init];
  view.fsm = [[TPOmniboxLocationFSM alloc] init];
  view.fsm.delegate = view;
  return view;
}

- (TPOmniboxTitleView*)titleView {
  if (!_titleView) {
    _titleView = [TPOmniboxTitleView view];
    _titleView.delegate = self;
    _titleView.alpha = 0;
    [self addSubview:_titleView];
  }
  return _titleView;
}

- (TPOmniboxEditQueryView*)editQueryView {
  if (!_editQueryView) {
    _editQueryView = [[TPOmniboxEditQueryView alloc] init];
    _editQueryView.delegate = self;
    _editQueryView.alpha = 0;
    [self addSubview:_editQueryView];
  }
  return _editQueryView;
}

- (void)layoutSubviews {
  [super layoutSubviews];

  switch (self.fsm.state) {
    case TPOmniboxLocationStateNewBorn:
      break;
    case TPOmniboxLocationStateTitle:
      self.editQueryView.alpha = 0;
      self.titleView.alpha = 1;
      break;
    case TPOmniboxLocationStateEditQuery:
      self.editQueryView.alpha = 1;
      self.titleView.alpha = 0;
      break;
  }

  self.titleView.frame = CGRectInset(self.bounds, 8, 5);
  self.editQueryView.frame = CGRectInset(self.bounds, 8, 5);
}

- (void)setTitle:(NSString*)title {
  self.titleView.title = title;
}

- (NSString*)title {
  return self.titleView.title;
}

- (void)setRequest:(NSURLRequest*)request {
  self.titleView.request = request;
  self.editQueryView.request = request;
}

- (NSURLRequest*)request{
  return self.titleView.request;
}

#pragma mark TPOmniboxTitleViewDelegate

- (void)omniboxTitleViewDidReceiveTap:(TPOmniboxTitleView*)titleView {
  [self.delegate omniboxLocationViewDidReceiveTitleTap:self];
}

#pragma mark TPOmniboxEditQueryViewDelegate

- (void)omniboxEditQueryView:(TPOmniboxEditQueryView*)editQueryView
              didChangeQuery:(NSString*)query {
  [self.delegate omniboxLocationView:self didChangeQuery:query];
}

- (void)omniboxEditQueryView:(TPOmniboxEditQueryView*)editQueryView
          didReturnWithQuery:(NSString*)query {
  [self.delegate omniboxLocationView:self didReturnQuery:query];
}

- (void)omniboxEditQueryViewDidFinishEdit:
    (TPOmniboxEditQueryView*)editQueryView {
  [self.delegate omniboxLocationViewDidFinishEditQuery:self];
}

#pragma mark TPOmniboxLocationFSMDelegate

- (void)omniboxLocationFSM:(TPOmniboxLocationFSM*)omniboxLocationFSM
        didChangeStateFrom:(TPOmniboxLocationState)fromState
                        to:(TPOmniboxLocationState)toState {
  if (toState == TPOmniboxLocationStateEditQuery) {
    [self.editQueryView becomeFirstResponder];
  } else {
    [self.editQueryView resignFirstResponder];
  }
}

@end
