//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPOmniboxTitleView.h"

SPEC_BEGIN(TPOmniboxTitleViewSpec)

describe(@"TPOmniboxTitleView", ^{
  __block TPOmniboxTitleView* view = nil;

  beforeEach(^{
    view = [[TPOmniboxTitleView alloc] init];
  });
});

SPEC_END