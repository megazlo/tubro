//  Created by Evgeniy Krasichkov on 06.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#include <UIKit/UIKit.h>

@class TPOmniboxTitleView;
@class TTTAttributedLabel;

@protocol TPOmniboxTitleViewDelegate <NSObject>

- (void)omniboxTitleViewDidReceiveTap:(TPOmniboxTitleView*)titleView;

@end

@interface TPOmniboxTitleView : UIView

@property(nonatomic, weak) id<TPOmniboxTitleViewDelegate> delegate;

@property(nonatomic, copy) NSString* title;
@property(nonatomic, copy) NSURLRequest* request;

@property(nonatomic, strong) TTTAttributedLabel* titleLabel;

+ (instancetype)view;

@end
