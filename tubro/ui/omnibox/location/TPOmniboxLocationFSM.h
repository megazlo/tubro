//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@class TPOmniboxLocationFSM;

typedef NS_ENUM(NSInteger, TPOmniboxLocationState) {
  TPOmniboxLocationStateNewBorn = 0,
  TPOmniboxLocationStateTitle,
  TPOmniboxLocationStateEditQuery,
};

@protocol TPOmniboxLocationFSMDelegate <NSObject>

- (void)omniboxLocationFSM:(TPOmniboxLocationFSM*)omniboxLocationFSM
        didChangeStateFrom:(TPOmniboxLocationState)fromState
                        to:(TPOmniboxLocationState)toState;

@end

@interface TPOmniboxLocationFSM : NSObject

@property(nonatomic, weak) id<TPOmniboxLocationFSMDelegate> delegate;

@property(nonatomic, assign) TPOmniboxLocationState state;

@end
