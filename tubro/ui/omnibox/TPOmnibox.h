//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import "TPOmniboxPresenter.h"
#import "TPTabManagerStatusDelegate.h"

@class TPOmnibox;

@protocol TPOmniboxDelegate <NSObject>

- (void)omniboxDidRequestEditQuery:(TPOmnibox*)omnibox;
- (void)omniboxDidFinishEditQuery:(TPOmnibox*)omnibox;
- (void)omnibox:(TPOmnibox*)omnibox didChangeQuery:(NSString*)query;
- (void)omnibox:(TPOmnibox*)omnibox didReturnQuery:(NSString*)query;
- (void)omniboxDidRequestGoForward:(TPOmnibox*)omnibox;
- (void)omniboxDidRequestGoBackward:(TPOmnibox*)omnibox;
- (void)omniboxDidRequestCancelEditQuery:(TPOmnibox*)omnibox;
- (void)omniboxDidRequestCancelLoading:(TPOmnibox*)omnibox;
- (void)omniboxDidRequestReload:(TPOmnibox*)omnibox;

@end

@interface TPOmnibox : NSObject<TPOmniboxPresenterDelegate,
                                TPTabManagerStatusDelegate>

@property(nonatomic, weak) id<TPOmniboxDelegate> delegate;
@property(nonatomic, weak) id<TPOmniboxPresenter> presenter;

@end
