//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@class TPOmnibox;

@protocol TPOmniboxPresenter;
@protocol TPUserNavigationController;

@protocol TPOmniboxPresenterDelegate <NSObject>

- (void)omniboxPresenterDidRequestEditQuery:
    (id<TPOmniboxPresenter>)omniboxPresenter;
- (void)omniboxPresenterDidFinishEditQuery:
    (id<TPOmniboxPresenter>)omniboxPresenter;
- (void)omniboxPresenter:(id<TPOmniboxPresenter>)omniboxPresenter
          didChangeQuery:(NSString*)query;
- (void)omniboxPresenter:(id<TPOmniboxPresenter>)omniboxPresenter
          didReturnQuery:(NSString*)query;

- (void)omniboxPresenterBackButtonDidTap:
    (id<TPOmniboxPresenter>)omniboxPresenter;
- (void)omniboxPresenterForwardButtonDidTap:
    (id<TPOmniboxPresenter>)omniboxPresenter;
- (void)omniboxPresenterCancelEditQueryButtonDidTap:
    (id<TPOmniboxPresenter>)omniboxPresenter;
- (void)omniboxPresenterCancelLoadingButtonDidTap:
    (id<TPOmniboxPresenter>)omniboxPresenter;
- (void)omniboxPresenterReloadButtonDidTap:
    (id<TPOmniboxPresenter>)omniboxPresenter;

@end

@protocol TPOmniboxPresenter <NSObject>

- (void)omnibox:(TPOmnibox*)omnibox didUpdateTitle:(NSString*)title;
- (void)omnibox:(TPOmnibox*)omnibox
    didUpdateNavigationController:(id<TPUserNavigationController>)controller;

- (void)omnibox:(TPOmnibox*)omnibox
    didStartLoadingRequest:(NSURLRequest*)request;
- (void)omniboxDidFinishLoadingRequest:(TPOmnibox*)omnibox;

@end

