//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

#import "TPOmniboxLocationFSM.h"
#import "TPOmniboxFSM.h"

@interface TPOmniboxComponentsFSMController : NSObject<TPOmniboxFSMDelegate>

@property (nonatomic, weak) TPOmniboxLocationFSM* omniboxLocationFSM;

@end
