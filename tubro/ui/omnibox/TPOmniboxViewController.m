//  Created by Evgeniy Krasichkov on 05.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxViewController.h"

#import "TPOmniboxCancelView.h"
#import "TPOmniboxComponentsFSMController.h"
#import "TPOmniboxLayout.h"
#import "TPOmniboxLocationFSM.h"
#import "TPOmniboxLocationView.h"
#import "TPOmniboxNavigationView.h"
#import "TPOmniboxLoadingControlView.h"
#import "TPUserNavigationController.h"
#import "TPTheme.h"

@interface TPOmniboxViewController ()<
    TPOmniboxFSMDelegate,
    TPOmniboxCancelViewDelegate,
    TPOmniboxLoadingControlViewDelegate,
    TPOmniboxLocationViewDelegate,
    TPOmniboxNavigationViewDelegate>

@end

@implementation TPOmniboxViewController

+ (instancetype)controller {
  TPOmniboxViewController* vc =
      [[TPOmniboxViewController alloc] initWithNibName:nil bundle:nil];
  vc.locationView = [TPOmniboxLocationView view];
  vc.locationView.delegate = vc;
  vc.navigationView = [TPOmniboxNavigationView view];
  vc.navigationView.delegate = vc;
  vc.cancelView = [TPOmniboxCancelView view];
  vc.cancelView.delegate = vc;
  vc.loadingControlView = [TPOmniboxLoadingControlView view];
  vc.loadingControlView.delegate = vc;

  vc.fsm = [[TPOmniboxFSM alloc] init];
  vc.fsm.delegate = vc;

  vc.layout = [[TPOmniboxLayout alloc] init];
  vc.layout.fsm = vc.fsm;
  vc.layout.containerView = vc.view;
  vc.layout.locationView = vc.locationView;
  vc.layout.navigationView = vc.navigationView;
  vc.layout.cancelView = vc.cancelView;
  vc.layout.loadingControlView = vc.loadingControlView;

  vc.componentsFSMController = [[TPOmniboxComponentsFSMController alloc] init];
  vc.componentsFSMController.omniboxLocationFSM = vc.locationView.fsm;
  return vc;
}

- (void)loadView {
  self.view = [[UIView alloc] initWithFrame:CGRectZero];
  self.view.backgroundColor = [[TPTheme sharedInstance] backgroundColor];
}

- (void)viewDidLoad {
  [self.view addSubview:self.locationView];
  [self.view addSubview:self.navigationView];
  [self.view addSubview:self.cancelView];
  [self.view addSubview:self.loadingControlView];
}

- (void)setLoading:(BOOL)isLoading {
  self.loadingControlView.loading = isLoading;
  [self.navigationView setNeedsLayout];
  [self layoutWithAnimation];
}

#pragma mark layout

- (void)layoutWithAnimation {
  [UIView animateWithDuration:0.25 animations:^{
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
  }];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];

  [self.layout invalidate];
}

#pragma mark TPOmniboxPresenter

- (void)omnibox:(TPOmnibox*)omnibox didUpdateTitle:(NSString*)title {
  self.locationView.title = title;
}

- (void)omnibox:(TPOmnibox *)omnibox
    didUpdateNavigationController:(id<TPUserNavigationController>)controller {
  self.navigationView.forwardButtonVisible = [controller canGoForward];
  self.navigationView.backButtonVisible = [controller canGoBackward];
  [self.navigationView setNeedsLayout];
  [self layoutWithAnimation];
}

- (void)omnibox:(TPOmnibox*)omnibox
    didStartLoadingRequest:(NSURLRequest*)request {
  self.locationView.request = request;
  [self setLoading:YES];
}

- (void)omniboxDidFinishLoadingRequest:(TPOmnibox*)omnibox {
  [self setLoading:NO];
}

#pragma mark TPOmniboxFSMDelegate

- (void)omniboxFSM:(TPOmniboxFSM*)omniboxFSM
    didChangeStateFrom:(TPOmniboxState)fromState
                    to:(TPOmniboxState)toState {
  [self.componentsFSMController omniboxFSM:omniboxFSM
                        didChangeStateFrom:fromState
                                        to:toState];
}

#pragma TPOmniboxLocationViewDelegate

- (void)omniboxLocationViewDidReceiveTitleTap:
    (TPOmniboxLocationView*)locationView {
  [self.delegate omniboxPresenterDidRequestEditQuery:self];
}

- (void)omniboxLocationViewDidFinishEditQuery:
    (TPOmniboxLocationView*)locationView {
  [self.delegate omniboxPresenterDidFinishEditQuery:self];
}

- (void)omniboxLocationView:(TPOmniboxLocationView*)locationView
             didChangeQuery:(NSString*)query {
  [self.delegate omniboxPresenter:self didChangeQuery:query];
}

- (void)omniboxLocationView:(TPOmniboxLocationView*)locationView
             didReturnQuery:(NSString*)query {
  [self.delegate omniboxPresenter:self didReturnQuery:query];
}

#pragma mark TPOmniboxNavigationViewDelegate

- (void)navigationViewBackButtonDidTap:
    (TPOmniboxNavigationView*)navigationView {
  [self.delegate omniboxPresenterBackButtonDidTap:self];
}

- (void)navigationViewForwardButtonDidTap:
    (TPOmniboxNavigationView*)navigationView {
  [self.delegate omniboxPresenterForwardButtonDidTap:self];
}

#pragma mark TPOmniboxCancelViewDelegate

- (void)cancelViewCancelButtonDidTap:(TPOmniboxCancelView*)cancelView {
  [self.delegate omniboxPresenterCancelEditQueryButtonDidTap:self];
}

#pragma mark TPOmniboxLoadingControlViewDelegate

- (void)loadingControlViewCancelButtonDidTap:(TPOmniboxLoadingControlView*)view {
  [self.delegate omniboxPresenterCancelLoadingButtonDidTap:self];
}

- (void)loadingControlViewReloadButtonDidTap:(TPOmniboxLoadingControlView*)view {
  [self.delegate omniboxPresenterReloadButtonDidTap:self];
}

@end
