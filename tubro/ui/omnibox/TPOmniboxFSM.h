//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <Foundation/Foundation.h>

@class TPOmniboxFSM;

typedef NS_ENUM(NSInteger, TPOmniboxState) {
  TPOmniboxStateNewBorn = 0,
  TPOmniboxStateWebControls,
  TPOmniboxStateNativeControls,
  TPOmniboxStateEditQuery,
};

@protocol TPOmniboxFSMDelegate <NSObject>

- (void)omniboxFSM:(TPOmniboxFSM*)omniboxFSM
    didChangeStateFrom:(TPOmniboxState)fromState
                    to:(TPOmniboxState)toState;

@end

@interface TPOmniboxFSM : NSObject

@property(nonatomic, weak) id<TPOmniboxFSMDelegate> delegate;

@property(nonatomic, assign) TPOmniboxState state;

@end
