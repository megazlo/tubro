//  Created by Evgeniy Krasichkov on 05.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import <UIKit/UIKit.h>

#import "TPOmniboxFSM.h"
#import "TPOmniboxPresenter.h"

@class TPOmniboxCancelView;
@class TPOmniboxComponentsFSMController;
@class TPOmniboxLayout;
@class TPOmniboxLocationView;
@class TPOmniboxNavigationView;
@class TPOmniboxLoadingControlView;

@interface TPOmniboxViewController : UIViewController<TPOmniboxPresenter>

@property(nonatomic, weak) id<TPOmniboxPresenterDelegate> delegate;

@property(nonatomic, strong) TPOmniboxFSM* fsm;
@property(nonatomic, strong) TPOmniboxLayout* layout;
@property(nonatomic, strong)
    TPOmniboxComponentsFSMController* componentsFSMController;

@property(nonatomic, strong) TPOmniboxLocationView* locationView;
@property(nonatomic, strong) TPOmniboxNavigationView* navigationView;
@property(nonatomic, strong) TPOmniboxCancelView* cancelView;
@property(nonatomic, strong) TPOmniboxLoadingControlView* loadingControlView;

+ (instancetype)controller;

@end
