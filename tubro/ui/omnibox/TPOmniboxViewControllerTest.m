//  Created by Evgeniy Krasichkov on 01.05.14.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "Kiwi.h"

#import "TPOmniboxViewController.h"

SPEC_BEGIN(TPOmniboxViewControllerSpec)

describe(@"TPOmniboxViewController", ^{
  __block TPOmniboxViewController* vc = nil;

  beforeEach(^{
    vc = [[TPOmniboxViewController alloc] init];
  });
});

SPEC_END