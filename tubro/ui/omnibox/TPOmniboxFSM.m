//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxFSM.h"

@implementation TPOmniboxFSM

- (void)setState:(TPOmniboxState)state {
  if (_state != state) {
    TPOmniboxState oldState = _state;
    _state = state;
    [self.delegate omniboxFSM:self didChangeStateFrom:oldState to:state];
  }
}

@end
