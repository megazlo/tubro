//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmnibox.h"

@implementation TPOmnibox

#pragma mark TPOmniboxPresenterDelegate

- (void)omniboxPresenterDidRequestEditQuery:
    (id<TPOmniboxPresenter>)omniboxPresenter {
  [self.delegate omniboxDidRequestEditQuery:self];
}

- (void)omniboxPresenterDidFinishEditQuery:
    (id<TPOmniboxPresenter>)omniboxPresenter {
  [self.delegate omniboxDidFinishEditQuery:self];
}

- (void)omniboxPresenter:(id<TPOmniboxPresenter>)omniboxPresenter
          didChangeQuery:(NSString*)query {
  [self.delegate omnibox:self didChangeQuery:query];
}

- (void)omniboxPresenter:(id<TPOmniboxPresenter>)omniboxPresenter
          didReturnQuery:(NSString*)query {
  [self.delegate omnibox:self didReturnQuery:query];
}

- (void)omniboxPresenterBackButtonDidTap:
    (id<TPOmniboxPresenter>)omniboxPresenter {
  [self.delegate omniboxDidRequestGoBackward:self];
}

- (void)omniboxPresenterForwardButtonDidTap:
    (id<TPOmniboxPresenter>)omniboxPresenter {
  [self.delegate omniboxDidRequestGoForward:self];
}

- (void)omniboxPresenterCancelEditQueryButtonDidTap:
    (id<TPOmniboxPresenter>)omniboxPresenter {
  [self.delegate omniboxDidRequestCancelEditQuery:self];
}

- (void)omniboxPresenterCancelLoadingButtonDidTap:
    (id<TPOmniboxPresenter>)omniboxPresenter {
  [self.delegate omniboxDidRequestCancelLoading:self];
}

- (void)omniboxPresenterReloadButtonDidTap:
    (id<TPOmniboxPresenter>)omniboxPresenter {
  [self.delegate omniboxDidRequestReload:self];
}

#pragma mark TPTabManagerStatusDelegate

- (void)tabManager:(TPTabManager*)tabManager
    didUpdateTitle:(NSString*)title {
  [self.presenter omnibox:self didUpdateTitle:title];
}

- (void)tabManager:(TPTabManager*)tabManager
    didUpdateNavigationController:(id<TPUserNavigationController>)controller {
  [self.presenter omnibox:self didUpdateNavigationController:controller];
}

- (void)tabManager:(TPTabManager*)tabManager
    didStartLoadingRequest:(NSURLRequest*)request {
  [self.presenter omnibox:self didStartLoadingRequest:request];
}

- (void)tabManagerDidFinishLoadingRequest:(TPTabManager*)tabManager {
  [self.presenter omniboxDidFinishLoadingRequest:self];
}

@end
