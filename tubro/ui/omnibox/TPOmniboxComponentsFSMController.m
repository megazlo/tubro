//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxComponentsFSMController.h"

@implementation TPOmniboxComponentsFSMController

#pragma mark TPOmniboxFSMDelegate

- (void)omniboxFSM:(TPOmniboxFSM*)omniboxFSM
    didChangeStateFrom:(TPOmniboxState)fromState
                    to:(TPOmniboxState)toState {
  switch (toState) {
    case TPOmniboxStateNewBorn:
      break;
    case TPOmniboxStateWebControls:
      self.omniboxLocationFSM.state = TPOmniboxLocationStateTitle;
      break;
    case TPOmniboxStateNativeControls:
      break;
    case TPOmniboxStateEditQuery:
      self.omniboxLocationFSM.state = TPOmniboxLocationStateEditQuery;
      break;
  }
}
@end
