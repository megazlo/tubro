//  Created by Evgeniy Krasichkov on 28.03.15.
//  Copyright (c) 2015 Evgeniy Krasichkov. All rights reserved.

#import "TPOmniboxLayout.h"

@interface TPOmniboxLayout()

@end

@implementation TPOmniboxLayout

#pragma mark TPOmniboxFSMDelegate

- (void)invalidate {
  [self layoutSubviews];
}

- (CGFloat)navigationViewWidth {
  return self.navigationView.desiredBounds.size.width;
}

- (CGFloat)cancelViewWidth {
  return 40.0f;
}

- (CGFloat)loadingControlViewWidth {
  return 40.0f;
}

- (CGFloat)locationViewLeftMargin {
  CGFloat minX = 0;
  switch (self.fsm.state) {
    case TPOmniboxStateNewBorn:
    case TPOmniboxStateEditQuery:
      break;
    case TPOmniboxStateWebControls:
    case TPOmniboxStateNativeControls:
      minX = [self navigationViewWidth];
      break;
  }
  return minX;
}

- (CGFloat)locationViewRightMargin {
  CGFloat maxX = 0;
  switch (self.fsm.state) {
    case TPOmniboxStateNewBorn:
    case TPOmniboxStateNativeControls:
      break;
    case TPOmniboxStateWebControls:
      maxX = [self loadingControlViewWidth];
      break;
    case TPOmniboxStateEditQuery:
      maxX = [self cancelViewWidth];
      break;
  }
  return maxX;
}

- (CGFloat)navigationViewAlpha {
  CGFloat alpha = 0;
  switch (self.fsm.state) {
    case TPOmniboxStateNewBorn:
    case TPOmniboxStateEditQuery:
      break;
    case TPOmniboxStateWebControls:
    case TPOmniboxStateNativeControls:
      alpha = 1.0;
      break;
  }
  return alpha;
}

- (CGFloat)cancelViewAlpha {
  CGFloat alpha = 0;
  switch (self.fsm.state) {
    case TPOmniboxStateNewBorn:
    case TPOmniboxStateWebControls:
    case TPOmniboxStateNativeControls:
      break;
    case TPOmniboxStateEditQuery:
      alpha = 1.0;
      break;
  }
  return alpha;
}

- (CGFloat)loadingControlViewAlpha {
  CGFloat alpha = 0;
  switch (self.fsm.state) {
    case TPOmniboxStateNewBorn:
    case TPOmniboxStateNativeControls:
    case TPOmniboxStateEditQuery:
      break;
    case TPOmniboxStateWebControls:
      alpha = 1.0;
      break;
  }
  return alpha;
}

- (CGRect)navigationViewFrame {
  CGRect frame = self.containerView.bounds;
  CGFloat width = [self navigationViewWidth];
  frame = CGRectMake(frame.origin.x,
                     frame.origin.y + 0,
                     width,
                     frame.size.height);
  return frame;
}

- (CGRect)cancelViewFrame {
  CGRect frame = self.containerView.bounds;
  CGFloat width = [self cancelViewWidth];
  frame = CGRectMake(frame.origin.x + frame.size.width - width,
                     frame.origin.y + 0,
                     width,
                     frame.size.height);
  return frame;
}

- (CGRect)loadingControlViewFrame {
  CGRect frame = self.containerView.bounds;
  CGFloat width = [self loadingControlViewWidth];
  frame = CGRectMake(frame.origin.x + frame.size.width - width,
                     frame.origin.y + 0,
                     width,
                     frame.size.height);
  return frame;
}

- (CGRect)locationViewFrame {
  CGRect frame = self.containerView.bounds;
  CGFloat leftMargin = [self locationViewLeftMargin];
  CGFloat rightMargin = [self locationViewRightMargin];
  frame = CGRectMake(frame.origin.x + leftMargin,
                     frame.origin.y + 0,
                     frame.size.width - leftMargin - rightMargin,
                     frame.size.height);
  return frame;
}

- (void)layoutSubviews {
  self.navigationView.frame = [self navigationViewFrame];
  self.navigationView.alpha = [self navigationViewAlpha];
  [self.navigationView setNeedsLayout];
  self.cancelView.frame = [self cancelViewFrame];
  self.cancelView.alpha = [self cancelViewAlpha];
  [self.cancelView setNeedsLayout];
  self.loadingControlView.frame = [self loadingControlViewFrame];
  self.loadingControlView.alpha = [self loadingControlViewAlpha];
  [self.loadingControlView setNeedsLayout];
  self.locationView.frame = [self locationViewFrame];
  [self.locationView setNeedsLayout];
}

@end
